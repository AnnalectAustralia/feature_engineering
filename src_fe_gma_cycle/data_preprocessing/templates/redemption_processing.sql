{creation_insertion_statement}
with propensity_campaigns as (
-- Find all campaigns associated with a propensity offer
select * from (
    select
        campaign_sk
        ,campaign_bk
        ,campaign_title
        ,coalesce(try_cast(substring(campaign_title, 1, 4)), try_cast(substring(campaign_title, 5, 4))) as offer_id
    from dw_pre.d_campaign
)
where offer_id between 8000 and 8999
and campaign_sk != 9933 -- ignore the stupid $2 Chicken McPieces offer
)

-- Find all transactions in the offer cycle dates that are related to a propensity offer
,propensity_transactions as (
select
    offer_cycle
    ,mobile_customer_id
    ,store_sk
    ,transaction_date_sk
    ,transaction_id
    ,max(c.campaign_sk) as campaign_sk
	,max(c.offer_id) as offer_id
from dw_pre.f_stld_detail t

inner join {schema}.{offer_cycle_dates} o
on offer_cycle = {offer_cycle} -- For the selected offer cycle
and transaction_date_sk between cycle_start_date_sk and cycle_end_date_sk -- Transactions in the allowed dates
and mobile_customer_id not in ('', '0') -- Non GMA customer transacations

inner join propensity_campaigns c
on t.campaign_sk = c.campaign_sk 

group by 1,2,3,4,5
)

-- Find which offers were sent to customers in this offer cycle
,sent_offers as (
    select
        offer_cycle
        ,mobile_customer_id
        ,segment
        ,offer_id
    from {schema}.{source_table} -- the table where the offers are stored

    where offer_cycle = {offer_cycle} -- the offer cycle we care about
    and offer_id between 8000 and 8999 -- limit to the propensity offers
)

-- join up the propensity transactions and the sent offers
,combined as (
    select
        o.*
        ,case when coalesce(t.transaction_date_sk, -1) = -1 then 0 else 1 end redeemed -- has the offer been redeemed
        ,t.store_sk
        ,t.transaction_date_sk
        ,t.transaction_id
        ,t.campaign_sk
    from sent_offers o

    left join propensity_transactions t
    on o.mobile_customer_id = t.mobile_customer_id
    and o.offer_id = t.offer_id
)

-- We have some issues with multiple redemptions
-- So we just take the one with the maximum transaction id
select 
    offer_cycle
    ,c.mobile_customer_id
    ,segment
    ,c.offer_id
    ,redeemed
    ,store_sk
    ,transaction_date_sk
    ,transaction_id
    ,campaign_sk
    ,num_redemptions
    ,getdate() as audit_redemptions_datetime -- audit time for logging purposes
from combined c

inner join (
    select
        mobile_customer_id
        ,offer_id
        ,count(transaction_id) as num_redemptions
        ,max(transaction_id) as max_tid
    from combined
    group by 1,2
) t
on t.mobile_customer_id = c.mobile_customer_id
and t.offer_id = c.offer_id
and coalesce(transaction_id, -1) >= coalesce(max_tid, -1)