
{creation_insertion_statement}
with offer_cycles as (
-- Find the offer cycles in the past
    select
        offer_cycle
        ,cycle_start_date_sk
        ,cycle_end_date_sk
        ,to_date('{model_date_sk}', 'YYYYMMDD') - to_date(cycle_start_date_sk, 'YYYYMMDD') as days_since_offer_cycle_start
        ,to_date('{model_date_sk}', 'YYYYMMDD') - to_date(cycle_end_date_sk, 'YYYYMMDD') as days_since_offer_cycle_end
    from {schema}.offer_cycle_dates
    where cycle_end_date_sk < {model_date_sk} -- All completed offer cycles (do we want to include ongoing ones?)
)

select
    {offer_cycle} as offer_cycle
    ,offer_id
    ,count(1) as offer_sent
    ,coalesce(sum(redeemed), 0) as offer_redeemed
    ,coalesce(sum(redeemed), 0) * 1.0 / count(1) as offer_redemption_rate
from {schema}.{source_table} r

inner join offer_cycles o
on r.offer_cycle = o.offer_cycle
and mobile_customer_id not in ('0') -- defunct mobile customer id

group by 1,2