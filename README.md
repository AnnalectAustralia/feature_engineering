# Feature Engineering
This repo is about collating all of the processes together for the model training of the offer allocation engine.

There are a number of key actions that are included in this:

* Daily Aggregates

* Redemption Calculations

* Transaction Feature Calculation

* Redemption Feature Calculation

  * Customer Redemption Stats

  * Offer Redemption Stats

  * Customer Offer Redemption Stats

* Training Data Generation

* Infer Data Generation

## Summary

### Preprocessing

This encompasses two processes. The daily aggregate calculation and the redemption calculation.

#### Daily Aggregates
This process is all about making the transaction feature calculation more efficient. It reads the data from the `dw_pre` dataset and performs feature engineering and aggregation to condense the data down to one row per customer per day.

Some examples of the features would be:

* `tld_net_amt`

    The total spend for that customer in that day.

* `cheeseburger_inc`

    The number of transactions in that day that include a cheeseburger.

* `propensity`

    The number of transactions that include a propensity offer.

Completing these calculations and storing them prior to the transaction feature engineering allows the features to be calculated considerably more quickly.

#### Redemption Calculations
Similarly to the daily aggregates this process is about calculting if a given offer was redeemed or not.

To do this we look at which offers were sent to which users on which offer cycle. We can then find the date range for that offer cycle and check `dw_pre.f_stld_detail` to see if there is a transaction that matches the offer sent.

The output of this can then be used to calculate the offer and customer redemption rates as part of the redemption features.

### Features
Again there are two processes here but both entail calculating the features for a given date. One for transactional features and one for redemption features.

#### Transaction Features
Firstly, the transactional features aggregate the daily aggregates to work out a representation of the customer from a transactional stand point at the given model date.

The output is one row per customer.

#### Redemption Features
The aim here to find out how a customer has interacted with the propensity offers previously. We want to find out if they have redeemed before and how often. 

We break it down into three component parts:

* Customer Redemption Stats

  This looks at redemptions at a customer level. For example:

  How many offers has the user been sent and how many redeemed?

  When did they last redeem and offer?

* Offer Redemption Stats

  Here we look at the offer redemption stats as some offers will be more lucrative than others.

* Customer Offer Redemption Stats

  This looks at how a customer interacts with a specific offer. So how many times did customer X get offer Y and how many times did they redeem it?

All of these are then combined into a view `redemption_features`. The reasoning behind this is due to the size of customer offer redemption stats. As it contains all customers cross joined with all offers for all offer cycles.

Hence a view was more economical.

### Model Data Prep
These processes combine the features so that they can be used for the relvant step in modelling training/inference.

#### Training
Here we use a few different tables to work out what happened when an offer was sent to a given customer.

We have the transaction and redemption features which is what the model makes a prediction off of.

And we know from the redemptions preprocessing whether the offer was redeemed or not.

Then finally we look at the daily aggregates file for the dates of the specified offer cycle to work out how much the customer spent and their GP%.

When these are all combined together we can train a model off of the back of it.

Note: All excluded transactions are not included in the targets e.g. Crew Offers and Voted Offers

The label columns are:
* `target_redeemed`
* `target_transactions`
* `target_spend`
* `target_gp_perc`

#### Infer
This is relatively more simple. As all we need to do is combine the redemption and transaction features for a given offer cycle/model_date.

The output is excessively large as there is a row for every combination of user and offer.

Note: This is not used currently as it takes up too much space on Redshift.

## Setup
First you should set up a virtual environment on your machine, if one doesn't exist already. 

To do this on the RDP boot up the miniconda prompt and execute:
``` sh
conda create --name features python=3.7
activate features
```
Then you can install the required packages:
``` sh
pip install -r requirements.txt
```
Then you should be able to run the application:
``` sh
python main.py --help
```

## Run

There are four methods that can be run by the tool:
    - preprocessing
    - training
    - infer
    - features

Each completes a diffent stage of the pipeline.

```
usage: Feature Engineering [-h] -c CONFIG -r REDSHIFT [-o]
                           {preprocessing,training,infer,features} ...

Run various processes to create the features for the Offer Allocation Engine

positional arguments:
  {preprocessing,training,infer,features}

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIG, --config CONFIG
                        The path to the config file
  -r REDSHIFT, --redshift REDSHIFT
                        The path to the redshift credentials
  -o, --overwrite       Would you like to overwrite the existing values in
                        redshift?
```

There are three key parameters here that are used for all of the methods.

#### Config
This contains all of the table names and all of the paths for the SQL code templates.

These are important as they point the app to the correct tables to calculate the features in the correct way.

```yaml
schema: annalect
offer_cycle_dates:
  table_name: offer_cycle_dates
daily_aggregates:
  template_path: src\data_preprocessing\templates\daily_aggregates.sql
  table_name: daily_aggregates
redemptions:
  template_path: src\data_preprocessing\templates\redemption_processing.sql
  table_name: redemptions
  source_table: offers_sent_log
offer_cycles:
  table_name: offer_cycle_dates
transaction_features:
  template_path: src\feature_engineering\templates\transaction_features.sql
  table_name: transaction_features
  source_table: daily_aggregates
customer_redemption_stats:
  template_path: src\feature_engineering\templates\customer_redemption_stats.sql
  table_name: customer_redemption_stats
  source_table: redemptions
offer_redemption_stats:
  template_path: src\feature_engineering\templates\offer_redemption_stats.sql
  table_name: offer_redemption_stats
  source_table: redemptions
customer_offer_redemption_stats:
  template_path: src\feature_engineering\templates\customer_offer_redemption_stats.sql
  table_name: customer_offer_redemption_stats
  source_table: redemptions
redemption_features:
  template_path: src\feature_engineering\templates\redemption_features_view.sql
  view_name: redemption_features
model_features:
  infer_table_name: model_infer
  infer_template_path: src/feature_engineering/templates/model_infer.sql
  training_table_name: model_training
  training_template_path: src/feature_engineering/templates/model_training.sql
```

#### Redshift
These include the credentials to access redshift in YAML format.

For example:
``` yaml
host: us-east-cprod-mdap-au-redshift1.ciuu2sg591tg.us-east-1.redshift.amazonaws.com
dbname: dev
port: 5439
user: {user_name}
password: {password}
```

#### Overwrite
This one is relatively simple as it is just whether you would like to overwrite any existing values in redshift.

### Preprocessing
This will calculate both the daily aggregates and the redemption features.

```
usage: Feature Engineering preprocessing [-h] [-oc OFFER_CYCLE]

optional arguments:
  -h, --help            show this help message and exit
  -oc OFFER_CYCLE, --offer-cycle OFFER_CYCLE
                        The offer cycle in question. If not provided it will
                        run all preprocessing since the last update.
```
#### offer_cycle
This is optional as if not given it will calculate all updates since the last set of calculations.

If given it will find the respective dates in the `offer_cycle_dates` table and perform the operation specifically for that offer cycle.

### Features
This is where the features are calculated and it will calculate both the transactional and redemption features.

```
usage: Feature Engineering features [-h] [-oc OFFER_CYCLE]

optional arguments:
  -h, --help            show this help message and exit
  -oc OFFER_CYCLE, --offer-cycle OFFER_CYCLE
                        The offer cycle to create the features for
```

#### offer_cycle
This is not optional as it will need to be looked up in the `offer_cycle_dates` table to find the correct date to calculate the features before.

### Training
Here we combine it all together and calculate the training labels. It will do this in a loop from the `start_cycle` to the `end_cycle` inclusive.

```
usage: Feature Engineering training [-h] -sc START_CYCLE -ec END_CYCLE

optional arguments:
  -h, --help            show this help message and exit
  -sc START_CYCLE, --start-cycle START_CYCLE
                        The start cycle for the training data.
  -ec END_CYCLE, --end-cycle END_CYCLE
                        The end cycle for the training data
```

### Infer
Here we will create the infer dataset.

```
usage: Feature Engineering infer [-h] -oc OFFER_CYCLE

optional arguments:
  -h, --help            show this help message and exit
  -oc OFFER_CYCLE, --offer-cycle OFFER_CYCLE
                        The offer cycle to run the infer for
```

#### offer_cycle
The offer cycle to calculate the infer dataset for.

# Features


|column_name|data_type|group|description|
|---|---|---|---|
|audit_training_datetime|timestamp without time zone|Audit|When was this training run completed|
|offer_id|bigint|Spine|What is the offer id|
|offer_name|character varying|Spine|What is the name of the offer|
|offer_cycle|integer|Spine|What offer cycle does this relate to|
|audit_transaction_datetime|timestamp without time zone|Audit|When were the transaction features generated|
|mobile_customer_id|character varying|Spine|The customer id|
|days_since_first_transaction|integer|RFM|How long since the customers first transaction|
|days_since_last_transaction|integer|RFM|How long since the customer's last transaction|
|active_days|bigint|RFM|How many distinct days has the customer been active|
|num_transactions|bigint|RFM|How many transactions has the customer completed (In GMA)|
|tld_net_amt|numeric|RFM|Total spend of the customer|
|total_units|bigint|RFM|How many units has the customer ordered|
|tld_gp_spend|numeric|GP %|Total GP relevant spend (excluding non food and non product)|
|tld_fp_costs|numeric|GP %|Total GP relevant food and paper costs|
|exclude_prop|numeric|Admin|What proportion of transactions are excluded (crew offers etc)|
|active_days_prop|numeric|RFM|What proportion of days since the customers first transaction have then been active|
|transactions_per_day|numeric|RFM|How many transactions do they complete per day on average|
|avg_days_between_transactions|numeric|RFM|1/transactions_per_day|
|avg_cheque|numeric|RFM|Their total spend divided by their total number of transactions|
|avg_units_per_transaction|numeric|RFM|How much do they order on average|
|gp_perc|numeric|GP %|Their lifetime GP %|
|gp_perc_last_week|numeric|GP %|GP% of transactions in the last week|
|gp_perc_last_month|numeric|GP %|GP% of transactions in the last month|
|gp_perc_last_3_months|numeric|GP %|GP% of transactions in the three months|
|mini_cheque_band_prop|numeric|RFM|What proportion of transactions < $5|
|small_cheque_band_prop|numeric|RFM|Proportion of transactions >= $5 and < $10|
|medium_cheque_band_prop|numeric|RFM|Proportion of transactions >= $10 and < $20|
|large_cheque_band_prop|numeric|RFM|Proportion of transactions >= $20 and < $40|
|supersize_cheque_band_prop|numeric|RFM|Proportion of transactions > $40|
|days_since_propensity|integer|Propensity|Days since a propensity offer was redeemed according to the daily aggregates|
|propensity_prop|numeric|Propensity|Proportion of transactions that include a propensity offer|
|propensity_per_day|numeric|Propensity|Total number of propensity redemptions / days since first transaction|
|avg_days_between_propensity|numeric|Propensity|1/propensity_per_day|
|propensity_avg_cheque|numeric|Propensity|The customer's average cheque when redeeming a propensity offer|
|propensity_gp_perc|numeric|Propensity|The GP % of the customer when redeeming a propensity offer|
|propensity_last_week|bigint|Propensity|Number of propensity redemptions in the last 7 days|
|propensity_last_month|bigint|Propensity|Number of propensity redemptions in the last 30 days|
|propensity_last_3_months|bigint|Propensity|Number of propensity redemptions in the last 90 days|
|days_since_mass_offer|integer|Offers|Days since a mass offer (Offer with ID not between 8000 and 8999) was redeemed|
|mass_offer_prop|numeric|Offers|Proportion of transactions that involve a mass offer|
|mass_offer_per_day|numeric|Offers|Total mass offers redeemed / days since first transaction|
|avg_days_between_mass_offer|numeric|Offers|1/mass_offer_per_day|
|mass_offer_avg_cheque|numeric|Offers|The average cheque when a mass offer was redeemed|
|mass_offer_gp_perc|numeric|Offers|The GP % when a mass offer was redeemed|
|mass_offer_last_week|bigint|Offers|How many mass offers were redeemed in the last 7 days|
|mass_offer_last_month|bigint|Offers|How many mass offers were redeemed in the last 30 days|
|mass_offer_last_3_months|bigint|Offers|How many mass offers were redeemed in the last 90 days|
|days_since_discount|integer|Discount|The number of days since a discount was last used|
|discount_per_day|numeric|Discount|Number of discounted transactions / days since first transaction|
|avg_days_between_discount|numeric|Discount|1/discount_per_day|
|discount_prop|numeric|Discount|Proportion of transactions that include a discount|
|discount_price_prop|numeric|Discount|Proportion of transactions that use a price discount|
|discount_promo_prop|numeric|Discount|Proportion of transactions that use a promo discount|
|discount_last_week|bigint|Discount|Number of discounts used in the last 7 days|
|discount_last_month|bigint|Discount|Number of discounts used in the last 30 days|
|discount_last_3_months|bigint|Discount|Number of discounts used in the last 90 days|
|last_week_active_prop|numeric|RFM|Proportion of last 7 days that the customer was active|
|last_month_active_prop|numeric|RFM|Proportion of last 30 days that the customer was active|
|last_3_months_active_prop|numeric|RFM|Proportion of last 90 days that the customer was active|
|last_week_avg_cheque|numeric|RFM|Avg Cheque in the last 7 days|
|last_month_avg_cheque|numeric|RFM|Avg Cheque in the last 30 days|
|last_3_months_avg_cheque|numeric|RFM|Avg Cheque in the last 90 days|
|weekend_breakfast|numeric|Day & Time|Proportion of transactions that happen on a weekend and between 5am and 10:30am|
|weekend_lunch|numeric|Day & Time|Proportion of transactions that happen on a weekend and between 10:30am and 2pm|
|weekend_tea|numeric|Day & Time|Proportion of transactions that happen on a weekend and between 2pm and 5pm|
|weekend_dinner|numeric|Day & Time|Proportion of transactions that happen on a weekend and between 5pm and 9pm|
|weekend_late|numeric|Day & Time|Proportion of transactions that happen on a weekend and after 9pm and before 5am|
|workday_breakfast|numeric|Day & Time|Proportion of transactions that happen on a workday and between 5am and 10:30am|
|workday_lunch|numeric|Day & Time|Proportion of transactions that happen on a workday and between 10:30am and 2pm|
|workday_tea|numeric|Day & Time|Proportion of transactions that happen on a workday and between 2pm and 5pm|
|workday_dinner|numeric|Day & Time|Proportion of transactions that happen on a workday and between 5pm and 9pm|
|workday_late|numeric|Day & Time|Proportion of transactions that happen on a workday and after 9pm and before 5am|
|avg_cheque_workday|numeric|Day & Time|Average cheque for transactions on a workday|
|avg_units_workday|numeric|Day & Time|Average units per transaction on a workday|
|avg_cheque_weekend|numeric|Day & Time|Average cheque for transactions on a weekend|
|avg_units_weekend|numeric|Day & Time|Average units per transaction on a weekend|
|city|numeric|Location|Proportion of transactions that occur in a city store|
|suburban|numeric|Location|Proportion of transactions that occur in a suburban store|
|country|numeric|Location|Proportion of transactions that occur in a country store|
|store_free_standing|numeric|Store Type|Proportion of transactions in a free standing store|
|store_instore|numeric|Store Type|Proportion of transactions in an instore store|
|store_food_court|numeric|Store Type|Proportion of transactions in a food court store|
|store_servo|numeric|Store Type|Proportion of transactions in a petrol station store|
|store_other|numeric|Store Type|Proportion of transactions in a store type not above|
|online_proc|numeric|Order Type|Proportion of transactions ordered online|
|mobile_proc|numeric|Order Type|Proportion of transactions ordered on mobile|
|in_store_proc|numeric|Order Type|Proportion of transactions ordered in store|
|drive_thru_proc|numeric|Order Type|Proportion of transactions ordered with drive through|
|coffee_days_since|integer|Food|Days since a coffee was ordered|
|coffee_days_since_discount|integer|Food|Days since a coffee was ordered using a discount|
|coffee_inc_prop|numeric|Food|Proportion of transactions that include a coffee|
|coffee_units_prop|numeric|Food|Proportion of units that are coffee related|
|coffee_spend_prop|numeric|Food|Proportion of spend that is coffee related|
|coffee_discount_prop|numeric|Food|Proportion of coffee transactions that include a dicount|
|burger_days_since|integer|Food|Look at equivalent for coffee|
|burger_days_since_discount|integer|Food|Look at equivalent for coffee|
|burger_inc_prop|numeric|Food|Look at equivalent for coffee|
|burger_units_prop|numeric|Food|Look at equivalent for coffee|
|burger_spend_prop|numeric|Food|Look at equivalent for coffee|
|burger_discount_prop|numeric|Food|Look at equivalent for coffee|
|chicken_days_since|integer|Food|Look at equivalent for coffee|
|chicken_days_since_discount|integer|Food|Look at equivalent for coffee|
|chicken_inc_prop|numeric|Food|Look at equivalent for coffee|
|chicken_units_prop|numeric|Food|Look at equivalent for coffee|
|chicken_spend_prop|numeric|Food|Look at equivalent for coffee|
|chicken_discount_prop|numeric|Food|Look at equivalent for coffee|
|snack_days_since|integer|Food|Look at equivalent for coffee|
|snack_days_since_discount|integer|Food|Look at equivalent for coffee|
|snack_inc_prop|numeric|Food|Look at equivalent for coffee|
|snack_units_prop|numeric|Food|Look at equivalent for coffee|
|snack_spend_prop|numeric|Food|Look at equivalent for coffee|
|snack_discount_prop|numeric|Food|Look at equivalent for coffee|
|dessert_days_since|integer|Food|Look at equivalent for coffee|
|dessert_days_since_discount|integer|Food|Look at equivalent for coffee|
|dessert_inc_prop|numeric|Food|Look at equivalent for coffee|
|dessert_units_prop|numeric|Food|Look at equivalent for coffee|
|dessert_spend_prop|numeric|Food|Look at equivalent for coffee|
|dessert_discount_prop|numeric|Food|Look at equivalent for coffee|
|breakfast_days_since|integer|Food|Look at equivalent for coffee|
|breakfast_days_since_discount|integer|Food|Look at equivalent for coffee|
|breakfast_inc_prop|numeric|Food|Look at equivalent for coffee|
|breakfast_units_prop|numeric|Food|Look at equivalent for coffee|
|breakfast_spend_prop|numeric|Food|Look at equivalent for coffee|
|breakfast_discount_prop|numeric|Food|Look at equivalent for coffee|
|beef_days_since|integer|Food|Look at equivalent for coffee|
|beef_days_since_discount|integer|Food|Look at equivalent for coffee|
|beef_inc_prop|numeric|Food|Look at equivalent for coffee|
|beef_units_prop|numeric|Food|Look at equivalent for coffee|
|beef_spend_prop|numeric|Food|Look at equivalent for coffee|
|beef_discount_prop|numeric|Food|Look at equivalent for coffee|
|fish_days_since|integer|Food|Look at equivalent for coffee|
|fish_days_since_discount|integer|Food|Look at equivalent for coffee|
|fish_inc_prop|numeric|Food|Look at equivalent for coffee|
|fish_units_prop|numeric|Food|Look at equivalent for coffee|
|fish_spend_prop|numeric|Food|Look at equivalent for coffee|
|fish_discount_prop|numeric|Food|Look at equivalent for coffee|
|fries_days_since|integer|Food|Look at equivalent for coffee|
|fries_days_since_discount|integer|Food|Look at equivalent for coffee|
|fries_inc_prop|numeric|Food|Look at equivalent for coffee|
|fries_units_prop|numeric|Food|Look at equivalent for coffee|
|fries_spend_prop|numeric|Food|Look at equivalent for coffee|
|fries_discount_prop|numeric|Food|Look at equivalent for coffee|
|mcveggie_days_since|integer|Food|Look at equivalent for coffee|
|mcveggie_days_since_discount|integer|Food|Look at equivalent for coffee|
|mcveggie_inc_prop|numeric|Food|Look at equivalent for coffee|
|mcveggie_units_prop|numeric|Food|Look at equivalent for coffee|
|mcveggie_spend_prop|numeric|Food|Look at equivalent for coffee|
|mcveggie_discount_prop|numeric|Food|Look at equivalent for coffee|
|salad_days_since|integer|Food|Look at equivalent for coffee|
|salad_days_since_discount|integer|Food|Look at equivalent for coffee|
|salad_inc_prop|numeric|Food|Look at equivalent for coffee|
|salad_units_prop|numeric|Food|Look at equivalent for coffee|
|salad_spend_prop|numeric|Food|Look at equivalent for coffee|
|salad_discount_prop|numeric|Food|Look at equivalent for coffee|
|happy_meal_days_since|integer|Food|Look at equivalent for coffee|
|happy_meal_days_since_discount|integer|Food|Look at equivalent for coffee|
|happy_meal_inc_prop|numeric|Food|Look at equivalent for coffee|
|happy_meal_units_prop|numeric|Food|Look at equivalent for coffee|
|happy_meal_spend_prop|numeric|Food|Look at equivalent for coffee|
|happy_meal_discount_prop|numeric|Food|Look at equivalent for coffee|
|family_days_since|integer|Food|Look at equivalent for coffee|
|family_days_since_discount|integer|Food|Look at equivalent for coffee|
|family_inc_prop|numeric|Food|Look at equivalent for coffee|
|family_units_prop|numeric|Food|Look at equivalent for coffee|
|family_spend_prop|numeric|Food|Look at equivalent for coffee|
|family_discount_prop|numeric|Food|Look at equivalent for coffee|
|vm_days_since|integer|Food|Look at equivalent for coffee|
|vm_days_since_discount|integer|Food|Look at equivalent for coffee|
|vm_inc_prop|numeric|Food|Look at equivalent for coffee|
|vm_units_prop|numeric|Food|Look at equivalent for coffee|
|vm_spend_prop|numeric|Food|Look at equivalent for coffee|
|vm_discount_prop|numeric|Food|Look at equivalent for coffee|
|shake_days_since|integer|Food|Look at equivalent for coffee|
|shake_days_since_discount|integer|Food|Look at equivalent for coffee|
|shake_inc_prop|numeric|Food|Look at equivalent for coffee|
|shake_units_prop|numeric|Food|Look at equivalent for coffee|
|shake_spend_prop|numeric|Food|Look at equivalent for coffee|
|shake_discount_prop|numeric|Food|Look at equivalent for coffee|
|sundae_days_since|integer|Food|Look at equivalent for coffee|
|sundae_days_since_discount|integer|Food|Look at equivalent for coffee|
|sundae_inc_prop|numeric|Food|Look at equivalent for coffee|
|sundae_units_prop|numeric|Food|Look at equivalent for coffee|
|sundae_spend_prop|numeric|Food|Look at equivalent for coffee|
|sundae_discount_prop|numeric|Food|Look at equivalent for coffee|
|big_mac_days_since|integer|Food|Look at equivalent for coffee|
|big_mac_days_since_discount|integer|Food|Look at equivalent for coffee|
|big_mac_inc_prop|numeric|Food|Look at equivalent for coffee|
|big_mac_units_prop|numeric|Food|Look at equivalent for coffee|
|big_mac_spend_prop|numeric|Food|Look at equivalent for coffee|
|big_mac_discount_prop|numeric|Food|Look at equivalent for coffee|
|qtr_p_days_since|integer|Food|Look at equivalent for coffee|
|qtr_p_days_since_discount|integer|Food|Look at equivalent for coffee|
|qtr_p_inc_prop|numeric|Food|Look at equivalent for coffee|
|qtr_p_units_prop|numeric|Food|Look at equivalent for coffee|
|qtr_p_spend_prop|numeric|Food|Look at equivalent for coffee|
|qtr_p_discount_prop|numeric|Food|Look at equivalent for coffee|
|cheeseburger_days_since|integer|Food|Look at equivalent for coffee|
|cheeseburger_days_since_discount|integer|Food|Look at equivalent for coffee|
|cheeseburger_inc_prop|numeric|Food|Look at equivalent for coffee|
|cheeseburger_units_prop|numeric|Food|Look at equivalent for coffee|
|cheeseburger_spend_prop|numeric|Food|Look at equivalent for coffee|
|cheeseburger_discount_prop|numeric|Food|Look at equivalent for coffee|
|nuggets_days_since|integer|Food|Look at equivalent for coffee|
|nuggets_days_since_discount|integer|Food|Look at equivalent for coffee|
|nuggets_inc_prop|numeric|Food|Look at equivalent for coffee|
|nuggets_units_prop|numeric|Food|Look at equivalent for coffee|
|nuggets_spend_prop|numeric|Food|Look at equivalent for coffee|
|nuggets_discount_prop|numeric|Food|Look at equivalent for coffee|
|hashbrown_days_since|integer|Food|Look at equivalent for coffee|
|hashbrown_days_since_discount|integer|Food|Look at equivalent for coffee|
|hashbrown_inc_prop|numeric|Food|Look at equivalent for coffee|
|hashbrown_units_prop|numeric|Food|Look at equivalent for coffee|
|hashbrown_spend_prop|numeric|Food|Look at equivalent for coffee|
|hashbrown_discount_prop|numeric|Food|Look at equivalent for coffee|
|mcmuffin_days_since|integer|Food|Look at equivalent for coffee|
|mcmuffin_days_since_discount|integer|Food|Look at equivalent for coffee|
|mcmuffin_inc_prop|numeric|Food|Look at equivalent for coffee|
|mcmuffin_units_prop|numeric|Food|Look at equivalent for coffee|
|mcmuffin_spend_prop|numeric|Food|Look at equivalent for coffee|
|mcmuffin_discount_prop|numeric|Food|Look at equivalent for coffee|
|propensity_overdue_or_underdue|numeric|Michael||
|transaction_overdue_or_underdue|numeric|Michael||
|avg_days_between_transaction_div_propensity|numeric|Michael||
|days_since_prop_eq_last_tran_flag|integer|Michael||
|propensity_last_week_div_last_month|numeric|Michael||
|propensity_last_week_div_last_3_months|numeric|Michael||
|propensity_last_month_div_last_3_months|numeric|Michael||
|propensity_last_week_diff_last_month|numeric|Michael||
|propensity_last_week_diff_last_3_months|numeric|Michael||
|propensity_last_month_diff_last_3_months|numeric|Michael||
|propensity_last_week_prop|numeric|Propensity|Proportion of transactions last week that included a propensity offer|
|propensity_last_month_prop|numeric|Propensity|Proportion of transactions last month that included a propensity offer|
|propensity_last_3_months_prop|numeric|Propensity|Proportion of transactions last three months that included a propensity offer|
|mass_offer_last_week_prop|numeric|Mass Offer|Proportion of transactions last week that included a mass offer|
|mass_offer_last_month_prop|numeric|Mass Offer|Proportion of transactions last month that included a mass offer|
|mass_offer_last_3_months_prop|numeric|Mass Offer|Proportion of transactions last three months that included a mass offer|
|discount_last_week_prop|numeric|Discount|Proportion of transactions last week that included a discount|
|discount_last_month_prop|numeric|Discount|Proportion of transactions last month that included a discount|
|discount_last_3_months_prop|numeric|Discount|Proportion of transactions last three months that included a discount|
|food_segment|character varying|Segment|The food segment|
|offer_segment|character varying|Segment|The offer dependence segment|
|monetary_segment|character varying|Segment|The monetary segment|
|rf_segment|character varying|Segment|The Recency and Frequency segment|
|customer_sent|bigint|Redemption|How many propensity offers has the customer received|
|customer_redeemed|bigint|Redemption|How many propensity offers has the customer redeemed|
|customer_redemption_rate|numeric|Redemption|What is the customers historical redemption rate|
|customer_last_redemption_sk|numeric|Redemption|When did a customer last redeem a propensity offer|
|customer_last_redemption_bk|date|Redemption|When did a customer last redeem a propensity offer|
|customer_days_since_redemption|integer|Redemption|How many days since the customer last redeemed a propensity offer|
|last_week_offers_sent|bigint|Redemption|How many offers did a customer recieve last week|
|last_week_offers_redeemed|bigint|Redemption|How many offers did a customer redeem last week|
|last_week_redemption_rate|numeric|Redemption|Their last week redemption rate|
|last_month_offers_sent|bigint|Redemption|How many offers did a customer recieve last month|
|last_month_offers_redeemed|bigint|Redemption|How many offers did a customer redeem last month|
|last_month_redemption_rate|numeric|Redemption|Their last month redemption rate|
|last_3_months_offers_sent|bigint|Redemption|How many offers did a customer recieve last month|
|last_3_months_offers_redeemed|bigint|Redemption|How many offers did a customer redeem last month|
|last_3_months_redemption_rate|numeric|Redemption|Their last month redemption rate|
|offer_sent|bigint|Redemption|How many times has this offer been sent (to any customer)|
|offer_redeemed|bigint|Redemption|How many times has this offer been redeemed (by any customer)|
|offer_redemption_rate|numeric|Redemption|What is this offers historical redemption rate|
|cust_off_sent|bigint|Redemption|How many times has this customer been sent this offer|
|cust_off_redeemed|bigint|Redemption|How many times has this customer redeemed this offer|
|cust_off_redemption_rate|numeric|Redemption|What is the redemption rate of this customer and this offer|
|cust_off_last_redemption_sk|numeric|Redemption|When did this customer last redeem this offer|
|cust_off_last_redemption_bk|date|Redemption|When did this customer last redeem this offer|
|cust_off_days_since_redemption|integer|Redemption|Days since this customer last redeem this offer|
|price|double precision|Offer Details|The price of the offer|
|og_price|double precision|Offer Details|How much would it cost without the offer discount|
|discount|double precision|Offer Details|The associated discount|
|discount_perc|double precision|Offer Details|The % discount compared to the og_price|
|offer_coffee|bigint|Offer Details|Does the offer contain a coffee (0, 1 flag)|
|offer_burger|bigint|Offer Details|Does the offer contain a burger (0, 1 flag)|
|offer_chicken|bigint|Offer Details|Does the offer contain a chicken (0, 1 flag)|
|offer_dessert|bigint|Offer Details|Does the offer contain a dessert (0, 1 flag)|
|offer_snack|bigint|Offer Details|Does the offer contain a snack (0, 1 flag)|
|offer_breakfast|bigint|Offer Details|Does the offer contain a breakfast (0, 1 flag)|
|offer_mcveggie|bigint|Offer Details|Does the offer contain a mcveggie (0, 1 flag)|
|offer_beef|bigint|Offer Details|Does the offer contain a beef (0, 1 flag)|
|offer_fish|bigint|Offer Details|Does the offer contain a fish (0, 1 flag)|
|offer_fries|bigint|Offer Details|Does the offer contain a fries (0, 1 flag)|
|offer_salad|bigint|Offer Details|Does the offer contain a salad (0, 1 flag)|
|offer_vm|bigint|Offer Details|Does the offer contain a value meal (0, 1 flag)|
|offer_happy_meal|bigint|Offer Details|Does the offer contain a happe meal (0, 1 flag)|
|offer_family|bigint|Offer Details|Does the offer contain a family (0, 1 flag)|
|offer_big_mac|bigint|Offer Details|Does the offer contain a big mac (0, 1 flag)|
|offer_cheeseburger|bigint|Offer Details|Does the offer contain a cheese burger (0, 1 flag)|
|offer_qtr_p|bigint|Offer Details|Does the offer contain a quarter pounder (0, 1 flag)|
|offer_nuggets|bigint|Offer Details|Does the offer contain a nugget (0, 1 flag)|
|offer_hashbrown|bigint|Offer Details|Does the offer contain a hash brown (0, 1 flag)|
|offer_mcmuffin|bigint|Offer Details|Does the offer contain a mcmuffin (0, 1 flag)|
|offer_sundae|bigint|Offer Details|Does the offer contain a sundae (0, 1 flag)|
|offer_shake|bigint|Offer Details|Does the offer contain a shake (0, 1 flag)|
|offer_similarity|numeric|Offer Similarity|How does the customers historical transaction history relate to the offer (sum({food}_inc_prop * offer_{food}|
|offer_discount_similarity|numeric|Offer Similarity|How does the customers historical discount transaction history relate to the offer (sum({food}_discount_prop * offer_{food}|
|offer_food_cats|bigint|Offer Similarity|How many food categories does the offer cover|
|offer_similarity_normalised|numeric|Offer Similarity|offer_similarity / offer_food_cats|
|offer_discount_similarity_normalised|numeric|Offer Similarity|offer_discount_similarity / offer_food_cats|
|target_redeemed|integer|Label|Did the customer redeem the sent offer|
|target_spend|numeric|Label|How much did the customer spend in the associated offer cycle|
|target_transactions|bigint|Label|How many transactions did the customer make in the associated offer cycle|
|target_propensity_spend|numeric|Label|How much did the customer spend when they redeemed the offer|
|target_gp_perc|numeric|Label|What was the gp % of the customer in the associated offer cycle|
|target_propensity_gp_perc|numeric|Label|What was the GP % of the customer for the propensity transaction for this offer cycle|