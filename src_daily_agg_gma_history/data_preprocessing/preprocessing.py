import pandas as pd
from src.utils import Redshift, Utils
from datetime import date, datetime, timedelta
from typing import Tuple
import logging


class DailyAggregates:
    """
    A class to manage the daily aggregate calculations and storage

    Parameters
    ----------

    table_name: str
    The name of the table where we will store all of the daily aggregates

    schema: str
    The schema where we will store all of the tables
    """
    
    def __init__(
        self,
        template_path: str = 'src_fe_gma_cycle/data_preprocessing/templates/daily_aggregates.sql',
        table_name: str = 'daily_aggregates_new',
        schema: str = 'annalect'
    ) -> None:

        # output table and schema
        self.table_name = table_name
        self.schema = schema

        # template for the daily aggregates sql code
        self.template_path = template_path

        self.logger = logging.getLogger(self.__class__.__name__)

    @property
    def full_table_name(self):
        """
        Combine the table name and schema into a single string
        """
        return f"{self.schema}.{self.table_name}"

    def run_daily_aggregate(self, rs: Redshift, current_date: int, date_sk: int, overwrite: bool = False) -> None:
        """
        Run the whole daily aggregate calculation process for a given `date_sk`

        Removes current entries so to remove duplication and then recalculates them

        Parameters
        ----------

        date_sk: int
        current_date: int
        The integer representation of the date in YYYYMMDD format
        """
        _start = datetime.now()

        # does the table exist already
        daily_agg_table_exists = rs.table_exists(
            self.table_name, self.schema)

        run_query = True

        if daily_agg_table_exists:

            self.logger.info(
                f"Checking for values in {self.full_table_name}"
                f" for transaction date sk {date_sk}"
            )

            count = rs.get_count(
                self.table_name,
                self.schema,
                transaction_date_sk=date_sk
            )

            self.logger.info(f'{count} values found')

            if count > 0:

                if overwrite:

                    self.logger.info(
                        f"Deleting values in {self.full_table_name}"
                        f" for transaction date sk {date_sk}"
                    )

                    # remove the data from the target table with transaction_date_sk = date_sk
                    rs.delete_from(
                        self.table_name,
                        self.schema,
                        transaction_date_sk=date_sk
                    )

                else:
                    self.logger.info(
                        'Values already exist for transaction_date_sk'
                        f' {date_sk} in table {self.full_table_name}'
                        ' To overwrite use `overwrite=True`'
                    )

                    run_query = False

        if run_query:
            # calculate the daily agg values
            self.create_daily_aggregate(rs, current_date, date_sk, daily_agg_table_exists)

        _end = datetime.now()
        self.logger.info(
            f'run_daily_aggregate processing time:  {str(_end - _start)}')

    def create_daily_aggregate(
        self,
        rs: Redshift,
        current_date: int, 
        date_sk: int,
        table_exists: bool = True
    ) -> None:
        """
        Run the query and store the outputs in the target table

        Parameters
        ----------

        date_sk: int
        current_date: int
        The integer representation of the date in YYYYMMDD format

        table_exists: bool
        Does the target table exist?
        """

        _start = datetime.now()

        self.logger.info(f"Processing daily_aggregate for {date_sk}")

        # if the table exists we just want to insert into it
        if table_exists:
            creation_insertion_statement = f'insert into {self.full_table_name} '

        # but if it doesn't we want to create it
        else:
            creation_insertion_statement = f'create table {self.full_table_name} as '

        # format the query with the desired values
        query = Utils.generate_query(
            path=self.template_path,
            current_date=current_date, 
            date_sk=date_sk,
            creation_insertion_statement=creation_insertion_statement
        )

        # run the query
        rs.query(query)

        _end = datetime.now()
        self.logger.info(
            f'create_daily_aggregate for {date_sk} processing time:  {str(_end - _start)}')

    def update(self, rs: Redshift, overwrite: bool = False) -> None:
        """
        The process to update the daily aggregates to the most recent date (yesterday)

        Calculates and stores the daily aggregates for all days from the most recent day in the table to yesterday
        """

        self.logger.info('Running Daily Aggregates Update')

        _start = datetime.now()

        # todays date
        today = datetime.now().date()

        # find the last date in the table
        df = rs.query(
            f"select max(transaction_date_sk) as max_date_sk from {self.full_table_name}",
            return_df=True
        )
        max_date_sk = int(df.max_date_sk.values[0])
        max_date = Utils.sk_to_date(max_date_sk)

        self.logger.info(
            f"Running Daily Aggregates from {max_date} to {(today - timedelta(days=1))}")

        # remove and replace the last day so it is up to date
        self.run_daily_aggregate(rs, current_date, max_date_sk, overwrite)

        # move on to the next day
        max_date = max_date + timedelta(days=1)

        # loop through all days until today and run the daily aggregate process
        while max_date < today:

            # calculate the daily agg values
            self.create_daily_aggregate(rs, Utils.date_to_sk(max_date))

            # move on to the next day
            max_date = max_date + timedelta(days=1)

        _end = datetime.now()
        self.logger.info(
            f'catch_up processing time:  {str(_end - _start)}')

