import pandas as pd
from src.utils import Redshift, Utils
from datetime import date, datetime, timedelta
from typing import Tuple
import logging


class OfferCycles:

    def __init__(
        self,
        schema: str = 'annalect',
        table_name: str = 'offer_cycle_dates'
    ) -> None:

        self.schema = schema
        self.table_name = table_name

        self.logger = logging.getLogger(self.__class__.__name__)

    @property
    def full_table_name(self):
        return f"{self.schema}.{self.table_name}"

    def table_exists(self, rs: Redshift):
        """Does the offer table exist

        Args:
            rs (Redshift): The redshift connection

        Returns:
            bool: Does the table exist
        """
        return rs.table_exists(self.table_name, self.schema)

    def first_offer_cycle(self, rs: Redshift, offer_cycle: int) -> int:
        """
        So this is used when we have the features for multiple cycles 
        calculated all at once. This happened in December when we had daily offers.

        The idea behind this is to avoid the duplication of features and hence save 
        space. Linking all offer cycles to the first one for that training date.

        Args:
            rs (Redshift): Redshift instance

            offer_cycle (int): The offer cycle in question

        Returns:
            int: The offer cycle of the first offer cycle trained on the same date
        """

        model_date_sk, _, _, _ = self.get_dates(rs, offer_cycle)

        if model_date_sk != -1:
            sql = f"""
            select
                min(offer_cycle) as offer_cycle
            from {self.full_table_name}
            where model_date_sk = {model_date_sk}
            """

            min_offer_cycle = rs.query(sql, return_df=True).values[0][0]

            return min_offer_cycle

        else:
            return -1

    def get_dates(self, rs: Redshift, offer_cycle: int) -> Tuple[int, int, int, int]:
        """
        Does the offer cycle exist in the `offer_cycle_dates` table

        And what are the dates associated with it

        Parameters
        ----------

        offer_cycle: int
        The offer cycle number

        Returns
        -------

        table_exists: bool
        Does the offer_cycle_dates table exist

        start_date_sk: int
        The start date of the offer cycle (-1 if not found)

        end_date_sk: int
        The end date of the offer cycle (-1 if not found)
         
        offer_cycle_length: int
        The number of offers within the offer_cycle (-1 if not found)       
        
        """

        _start = datetime.now()

        model_date_sk = cycle_start_date_sk = cycle_end_date_sk = offer_cycle_length = -1

        # find out if this offer cycle is already in the table
        offer_cycle_dates = rs.query(
            f'select * from {self.schema}.{self.table_name} where offer_cycle = {offer_cycle}',
            return_df=True
        )

        # does it already exist
        if offer_cycle_dates.shape[0] > 0:
            model_date_sk = offer_cycle_dates.loc[0, 'model_date_sk']
            cycle_start_date_sk = offer_cycle_dates.loc[0,
                                                        'cycle_start_date_sk']
            cycle_end_date_sk = offer_cycle_dates.loc[0, 'cycle_end_date_sk']
            offer_cycle_length = offer_cycle_dates.loc[0, 'offer_cycle_length']

        _end = datetime.now()
        self.logger.info(
            f'offer_cycle_dates processing time:  {str(_end - _start)}')

        return model_date_sk, cycle_start_date_sk, cycle_end_date_sk, offer_cycle_length

    def insert_offer_cycle_dates(
        self,
        rs: Redshift,
        offer_cycle: int,
        model_date: date,
        start_date: date,
        end_date: date,
        overwrite: bool = False
    ):
        """
        Insert the set of offer dates for a given offer cycle

        Parameters
        ----------

        offer_cycle: int
        The offer cycle number

        start_date: date
        The start date of the offer cycle

        end_date: date
        The end date of the offer cycle

        overwrite: bool (default=False)
        Do you want to overwrite the values in the table
        """

        # convert the dates to sk
        model_date_sk = Utils.date_to_sk(model_date)
        start_date_sk = Utils.date_to_sk(start_date)
        end_date_sk = Utils.date_to_sk(end_date)

        offer_cycle_length = end_date - start_date

        # should we upload the values (overwrite check)
        upload_values = True

        # Find the current dates for the offer cycle in the table
        ocd_model_date_sk, ocd_start_date_sk, ocd_end_date_sk, _ = self.get_dates(
            rs,
            offer_cycle
        )

        # If there is a value for the offer cycle in the table already
        if ocd_start_date_sk != -1:
            # if we are overwriting remove the relevant value in the table
            if overwrite:
                rs.delete_from(
                    self.table_name,
                    self.schema,
                    offer_cycle=offer_cycle
                )

            else:
                # they already exist so don't upload the values
                upload_values = False

                # are the date ranges the same
                if ocd_model_date_sk == model_date_sk and \
                        ocd_start_date_sk == start_date_sk and \
                        ocd_end_date_sk == end_date_sk:
                    self.logger.info(
                        f"Offer cycle {offer_cycle} already exists in the table with the same dates")
                else:
                    self.logger.info(
                        f"Offer cycle {offer_cycle} already exists in the table with different dates "
                        f"Table Model Dates: {ocd_model_date_sk} - {model_date_sk} "
                        f"Table Start Dates: {ocd_start_date_sk} - {start_date_sk} "
                        f"Table End Dates: {ocd_end_date_sk} - {end_date_sk} "
                        "Use overwrite=True to change them"
                    )

        # upload the new values
        if upload_values:
            df = pd.DataFrame(
                [offer_cycle, start_date_sk, end_date_sk, offer_cycle_length],
                columns=['offer_cycle', 'start_date_sk',
                         'end_date_sk', 'offer_cycle_length']
            )

            rs.upload_dataframe(df, 'offer_cycle_dates', self.schema)


class DailyAggregates:
    """
    A class to manage the daily aggregate calculations and storage

    Parameters
    ----------

    table_name: str
    The name of the table where we will store all of the daily aggregates

    schema: str
    The schema where we will store all of the tables
    """

    def __init__(
        self,
        template_path: str = 'src_fe_gma_cycle/data_preprocessing/templates/daily_aggregates.sql',
        table_name: str = 'daily_aggregates_new',
        schema: str = 'annalect'
    ) -> None:

        # output table and schema
        self.table_name = table_name
        self.schema = schema

        # template for the daily aggregates sql code
        self.template_path = template_path

        self.logger = logging.getLogger(self.__class__.__name__)

    @property
    def full_table_name(self):
        """
        Combine the table name and schema into a single string
        """
        return f"{self.schema}.{self.table_name}"

    def run_daily_aggregate(self, rs: Redshift, date_sk: int, overwrite: bool = False) -> None:
        """
        Run the whole daily aggregate calculation process for a given `date_sk`

        Removes current entries so to remove duplication and then recalculates them

        Parameters
        ----------

        date_sk: int
        The integer representation of the date in YYYYMMDD format
        """

        _start = datetime.now()

        # does the table exist already
        daily_agg_table_exists = rs.table_exists(
            self.table_name, self.schema)

        run_query = True

        if daily_agg_table_exists:

            self.logger.info(
                f"Checking for values in {self.full_table_name}"
                f" for transaction date sk {date_sk}"
            )

            count = rs.get_count(
                self.table_name,
                self.schema,
                transaction_date_sk=date_sk
            )

            self.logger.info(f'{count} values found')

            if count > 0:

                if overwrite:

                    self.logger.info(
                        f"Deleting values in {self.full_table_name}"
                        f" for transaction date sk {date_sk}"
                    )

                    # remove the data from the target table with transaction_date_sk = date_sk
                    rs.delete_from(
                        self.table_name,
                        self.schema,
                        transaction_date_sk=date_sk
                    )

                else:
                    self.logger.info(
                        'Values already exist for transaction_date_sk'
                        f' {date_sk} in table {self.full_table_name}'
                        ' To overwrite use `overwrite=True`'
                    )

                    run_query = False

        if run_query:
            # calculate the daily agg values
            self.create_daily_aggregate(rs, date_sk, daily_agg_table_exists)

        _end = datetime.now()
        self.logger.info(
            f'run_daily_aggregate processing time:  {str(_end - _start)}')

    def create_daily_aggregate(
        self,
        rs: Redshift,
        date_sk: int,
        table_exists: bool = True
    ) -> None:
        """
        Run the query and store the outputs in the target table

        Parameters
        ----------

        date_sk: int
        The integer representation of the date in YYYYMMDD format

        table_exists: bool
        Does the target table exist?
        """

        _start = datetime.now()

        self.logger.info(f"Processing daily_aggregate for {date_sk}")

        # if the table exists we just want to insert into it
        if table_exists:
            creation_insertion_statement = f'insert into {self.full_table_name} '

        # but if it doesn't we want to create it
        else:
            creation_insertion_statement = f'create table {self.full_table_name} as '

        # format the query with the desired values
        query = Utils.generate_query(
            path=self.template_path,
            date_sk=date_sk,
            creation_insertion_statement=creation_insertion_statement
        )

        # run the query
        rs.query(query)

        _end = datetime.now()
        self.logger.info(
            f'create_daily_aggregate for {date_sk} processing time:  {str(_end - _start)}')

    def update(self, rs: Redshift, overwrite: bool = False) -> None:
        """
        The process to update the daily aggregates to the most recent date (yesterday)

        Calculates and stores the daily aggregates for all days from the most recent day in the table to yesterday
        """

        self.logger.info('Running Daily Aggregates Update')

        _start = datetime.now()

        # todays date
        today = datetime.now().date()

        # find the last date in the table
        df = rs.query(
            f"select max(transaction_date_sk) as max_date_sk from {self.full_table_name}",
            return_df=True
        )
        max_date_sk = int(df.max_date_sk.values[0])
        max_date = Utils.sk_to_date(max_date_sk)

        self.logger.info(
            f"Running Daily Aggregates from {max_date} to {(today - timedelta(days=1))}")

        # remove and replace the last day so it is up to date
        self.run_daily_aggregate(rs, max_date_sk, overwrite)

        # move on to the next day
        max_date = max_date + timedelta(days=1)

        # loop through all days until today and run the daily aggregate process
        while max_date < today:

            # calculate the daily agg values
            self.create_daily_aggregate(rs, Utils.date_to_sk(max_date))

            # move on to the next day
            max_date = max_date + timedelta(days=1)

        _end = datetime.now()
        self.logger.info(
            f'catch_up processing time:  {str(_end - _start)}')


class Redemptions:
    """
    A class to manage the processed redemptions pipeline. 

    From finding the offers sent to finding out if it was redeemed or not.

    Parameters
    ----------

    table_name: str
    The name of the table where we will store all of the processed redemption fields

    schema: str
    The schema where we will store all of the tables
    """

    def __init__(
        self,
        template_path: str = 'src_fe_gma_cycle/data_preprocessing/templates/redemption_processing.sql',
        table_name: str = 'redemptions',
        schema: str = 'annalect',
        source_table: str = 'offers_sent_log'
    ) -> None:

        # output table and schema
        self.table_name = table_name
        self.schema = schema
        self.source_table = source_table

        # template for the daily aggregates sql code
        self.template_path = template_path

        self.logger = logging.getLogger(self.__class__.__name__)

    @property
    def full_table_name(self) -> str:
        return f"{self.schema}.{self.table_name}"

    def calculate_redemptions(
        self,
        rs: Redshift,
        ocd: OfferCycles,
        offer_cycle: int,
        overwrite: bool = False
    ):
        """
        This is the like the mitochondria of the class (The powerhouse).

        This is where the magic happens.

        This function runs the process of finding out if an offer was redeemed.

        Parameters
        ----------

        offer_cycle: int
        The offer cycle number
        """

        self.logger.info(
            f"Calculate redemptions for offer cycle {offer_cycle}")

        _start = datetime.now()

        run_query = True

        # does the offer cycle table exist
        if ocd.table_exists(rs):
            # what are the start and end dates for this offer cycle
            model_date_sk, _, _, _ = ocd.get_dates(rs, offer_cycle)

            # if we don't have values for this offer cycle throw an error
            if model_date_sk == -1:
                raise ValueError(
                    f"Offer Cycle {offer_cycle} does not exist in table `{ocd.schema}.{ocd.table_name}",
                    "Insert dates into table for this offer cycle before proceeding"
                )

        else:
            raise ValueError(
                f"`offer_cycle_dates` does not exist in the proposed schema {self.schema}")

        # does the target table already exist
        output_table_exists = rs.table_exists(
            self.table_name, schema=self.schema)

        # Remove the older versions
        if output_table_exists:

            self.logger.info(
                f"Checking if values already exist in {self.full_table_name} "
                f"for offer cycle {offer_cycle}"
            )

            count = rs.get_count(
                self.table_name,
                self.schema,
                offer_cycle=offer_cycle
            )

            self.logger.info(
                f"{count} values exist"
            )

            # are there current values in the table for this offer cycle
            if count > 0:

                # if overwrite delete the values currently in the table
                if overwrite:

                    self.logger.info(
                        f"Deleting values in {self.full_table_name} "
                        f"for offer cycle {offer_cycle}"
                    )

                    rs.delete_from(
                        self.table_name,
                        self.schema,
                        offer_cycle=offer_cycle
                    )

                # otherwise we don't want to do anything
                else:
                    self.logger.info(
                        f"Table {self.full_table_name} already exists"
                        f" and values for offer cycle {offer_cycle} already exist"
                        'To overwrite use `overwrite=True`'
                    )

                    # the values already exist and overwrite is false
                    run_query = False

            creation_insertion_statement = f'insert into {self.full_table_name} '

        # but if it doesn't we want to create it
        else:
            creation_insertion_statement = f'create table {self.full_table_name} as '

        if run_query:
            # format the query with the relevant values
            query = Utils.generate_query(
                path=self.template_path,
                offer_cycle=offer_cycle,
                schema=self.schema,
                source_table=self.source_table,
                creation_insertion_statement=creation_insertion_statement,
                offer_cycle_dates=ocd.table_name
            )

            self.logger.info("Running query for calculate_redemptions")

            # run the query
            rs.query(query)

        _end = datetime.now()
        self.logger.info(
            f'redemption_features processing time:  {str(_end - _start)}s')

    def update(self, rs: Redshift, ocd: OfferCycles, overwrite: bool = False):

        self.logger.info("Running Redemptions Update")
        _start = datetime.now()

        # find the most recent offer cycle in the processed redemptions
        max_offer_cycle = rs.query(
            f'select max(offer_cycle) from {self.full_table_name}',
            return_df=True
        ).values[0][0]

        # the current date in sk format
        # we don't want to process offer cycles which are still active
        today_sk = Utils.date_to_sk(datetime.now().date())

        # find the offer cycles which are not active and after the last update
        offer_cycles = rs.query(
            f'select offer_cycle from {ocd.full_table_name} '
            f'where offer_cycle >= {max_offer_cycle} '
            f'and cycle_end_date_sk < {today_sk}',
            return_df=True
        )['offer_cycle'].values

        self.logger.info(
            f"Running Redemptions from offer cycles: {', '.join(offer_cycles.astype(str))}")

        # loop through the remaining offer cycles and calc the redemptions
        for oc in offer_cycles:
            self.calculate_redemptions(
                rs,
                ocd,
                oc,
                overwrite
            )

        _end = datetime.now()
        self.logger.info(
            f'update processing time:  {str(_end - _start)}s')
