import pandas as pd
from sqlalchemy.engine import url as sa_url
from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import text
import logging
from datetime import datetime


class Redshift:
    """
    Class to manage the connection to redshift

    Parameters
    ----------

    host:str

    port:int

    dbname:str

    user:str

    password:str

    Properties
    ----------

    connection_url:str

    engine:sqlalchemy.Engine

    """

    def __init__(self, host, port, dbname, user, password, echo=False):

        self.logger = logging.getLogger(self.__class__.__name__)

        self.connection_url = sa_url.URL(
            drivername='postgresql+psycopg2',
            username=user,
            password=password,
            host=host,
            port=port,
            database=dbname
        )

        self.logger.debug(f'Connection URL: {self.connection_url}')

        try:
            self.engine = create_engine(self.connection_url, echo=echo)
        except Exception as e:
            self.logger.error("Error Connecting to redshift")
            self.logger.exception(e)
            raise e

    def query(self, query, return_df=False, chunksize=None):
        """
        Run a query in the connected database

        Parameters
        ----------
        query:str
        The SQL code to run in the database

        return_df:bool
        Do you want to return the results as a dataframe?

        Returns
        -------
        pd.DataFrame
        The results of your query
        """
        _s = datetime.now()

        # if you want to return the method to return a dataframe then we use a different method
        if return_df:
            output = self._df_query(query, chunksize)

        # If they do not want the results we can just execute the query on the redshift side
        else:
            self._raw_query(query)
            output = None

        _e = datetime.now()
        self.logger.info(f"Query took {str(_e - _s)}")

        return output

    def _df_query(self, query: str, chunksize: int = None) -> pd.DataFrame:
        """Run a query against redshift and return a dataframe

        Args:
            query (str): The query
            chunksize (int): To chunk the output enter a value. Defaults to None.

        Raises:
            e: Raises an error if the query fails

        Returns:
            pd.DataFrame: The output Dataframe
        """
        try:
            output = pd.read_sql(sql=query, con=self.engine,
                                 chunksize=chunksize)
            self.logger.info(f"Output df shape: {output.shape}")
        except Exception as e:
            self.logger.error("Error running query to get dataframe")
            self.logger.exception(e)
            raise e

        return output

    def _raw_query(self, query: str) -> None:
        """Execute a query in redshift and don't extract any results

        Args:
            query (str): The query

        Raises:
            e: Any error raised during execution
        """
        statement = text(query)

        try:
            with self.engine.connect() as con:
                con.execute(statement)
        except Exception as e:
            self.logger.error("Error running query")
            self.logger.exception(e)
            raise e

    def close(self) -> None:
        """
        Close the connection to redshift
        """
        self.engine.dispose()

    def table_exists(self, table_name: str, schema: str = None) -> bool:
        """
        Check if a table exists in the data warehouse
        """
        return self.engine.dialect.has_table(self.engine, table_name, schema)

    def upload_dataframe(
        self,
        df: pd.DataFrame,
        table_name: str,
        schema: str = None,
        if_exists: str = 'append',
        index: str = False,
        index_label: str = None,
        chunksize: int = 10000,
        dtype: dict = None,
        method: str = 'multi'
    ) -> None:
        """
        Upload a dataframe to the redshift data warehouse. 

        Used the `pd.DataFrame.to_sql` method but changed some defaults

        Parameters
        ----------
        Look at the parameters for `pd.DataFrame.to_sql`
        """

        self.logger.info(f"Uploading dataframe of shape: {df.shape}")
        _s = datetime.now()

        df.to_sql(
            table_name,
            con=self.engine,
            schema=schema,
            if_exists=if_exists,
            index=index,
            index_label=index_label,
            chunksize=chunksize,
            dtype=dtype,
            method=method
        )

        _e = datetime.now()
        self.logger.info(f"upload_table took {str(_e - _s)}s")

    def drop_table(self, table_name: str, schema: str = None) -> None:
        """
        Drop a table in redshift
        """

        _s = datetime.now()

        self.logger.info(f"Drop Table {schema}.{table_name}")
        self.query(f'drop table if exists {schema}.{table_name};')

        _e = datetime.now()
        self.logger.info(f"drop_table took {str(_e - _s)}s")

    def get_count(self, table_name: str, schema: str,  **kwargs) -> int:
        """
        Get the number of rows in a given table given conditions
        """

        where_conditions = '\n'.join(
            [f"and {k} = {v}" for k, v in kwargs.items()])

        sql = f"""
        select count(*) from {schema}.{table_name}
        where 1=1
        {where_conditions}
        """

        count = self.query(sql, return_df=True)

        return count.values[0][0]

    def delete_from(self, table_name: str, schema: str, **kwargs) -> None:
        """
        Delete from a table given conditions
        """

        where_conditions = '\n'.join(
            [f"and {k} = {v}" for k, v in kwargs.items()])

        sql = f"""
        delete from {schema}.{table_name}
        where 1=1
        {where_conditions}
        """

        self.query(sql)
