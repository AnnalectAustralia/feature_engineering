D:
conda activate model_v2

cd D:\Annalect\Production\Bitbucket\feature_engineering

python main_fe_gma_cycle.py -c config_fe.yml -r credentials.yml -o preprocessing # this runs cycle-wise GMA FE
python main_dagg_gma_history.py -c config_fe.yml -r credentials.yml -o preprocessing # this runs historical GMA daily_agg (dates need to be updated in the main_dagg file)
python main_dagg_token_enriched.py -c config_token.yml -r credentials.yml -o preprocessing # this runs enriched GMA daily_agg(dates need to be updated in the main_dagg file if history run is needed)
python main_dagg_token_offline.py -c config_token_offline.yml -r credentials.yml -o preprocessing # this runs offline cusotmers using token(dates need to be updated in the main_dagg file)