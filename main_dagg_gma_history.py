import argparse
from datetime import datetime, date, timedelta
from logging import exception
from src.utils import Utils, Redshift
from src_daily_agg_gma_history.data_preprocessing import DailyAggregates

import logging
import pathlib
import os


class FeatureEngineering:
    """A master class to orchestrate all the different methods

    Args:
        daily_agg (DailyAggregates): class for daily aggregates
        schema (str, optional): The schema in redshift. Defaults to 'annalect'.
    """

    def __init__(
        self,
        daily_agg: DailyAggregates,
        schema: str = 'annalect'
    ) -> None:

        self.logger = logging.getLogger(__class__.__name__)

        self.schema = schema
        self.daily_agg = daily_agg


    def update_preprocessing(
        self,
        rs: Redshift,
        overwrite: bool = False
    ) -> None:
        try:
            self.logger.info("UPDATE PROCESSING")
            hour_cutoff=11
            if datetime.now().hour >= hour_cutoff:
                dt1 = datetime.now()
            else:
                dt1 = datetime.now() - timedelta(days=1)
                
            end_date_sk= 20221011 #int(f'{dt1.year}{dt1.month:02d}{dt1.day:02d}')
            start_date_sk = 20221009 #int(f'{dt1.year}{dt1.month:02d}{dt1.day-5:02d}')  #20211001 
  

            # convert the date sks into dates
            counter = Utils.sk_to_date(start_date_sk)
            end_date = Utils.sk_to_date(end_date_sk)

            # loop through all dates
            while counter <= end_date:
                # convert back into an sk
                date_sk = Utils.date_to_sk(counter)
                current_date = end_date_sk+1 #20220326  #TODO: update
                # run daily aggregate
                self.daily_agg.run_daily_aggregate(rs, current_date, date_sk, overwrite)

                # increment it by a day
                counter = counter + timedelta(days=1)

        except Exception as e:
            self.logger.error("Error updating preprocessing")
            self.logger.exception(e)


def build_feature_engineering(config):


    daily_agg = DailyAggregates(
        schema=config['schema'],
        **config['daily_aggregates']
    )


    return FeatureEngineering(
        daily_agg,
        config.get('schema', 'annalect')
    )


def build_parser():

    parser = argparse.ArgumentParser(
        prog='Feature Engineering',
        description="Run various processes to create the features for the Offer Allocation Engine"
    )
    subparsers = parser.add_subparsers(dest='command')

    parser.add_argument(
        '-c', '--config', type=str,
        help='The path to the config file', required=True
    )

    parser.add_argument(
        '-r', '--redshift', type=str,
        help="The path to the redshift credentials", required=True
    )

    # all methods have an overwrite argument so add it to the top line
    parser.add_argument(
        '-o', '--overwrite',
        dest='overwrite', action='store_true',
        help="Would you like to overwrite the existing values in redshift?"
    )
    parser.set_defaults(overwrite=False)

    prep = subparsers.add_parser('preprocessing')
    prep.add_argument(
        '-oc', '--offer-cycle',
        dest='offer_cycle', type=int, default=-1,
        help="The offer cycle in question. If not provided it will run all preprocessing since the last update."
    )

    return parser


if __name__ == "__main__":

    # build the logger
    logging.basicConfig(
        filename=os.path.join(
            pathlib.Path(__file__).parent.absolute(),
            'log_dagg_gma_history.log'
        ),
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.DEBUG
    )

    # set up the parser
    parser = build_parser()

    # parse the args
    args = parser.parse_args()

    logging.info(f"ARGS: {args}")

    # read in the yaml
    config = Utils.process_yaml(args.config)
    credentials = Utils.process_yaml(args.redshift)

    # set up the redshift connection using the details in the config
    rs = Redshift(
        **credentials
    )

    # Feature engineering
    fe = build_feature_engineering(config)

    if args.command == 'preprocessing':
        fe.update_preprocessing(rs, args.overwrite)

    logging.info('Complete')
