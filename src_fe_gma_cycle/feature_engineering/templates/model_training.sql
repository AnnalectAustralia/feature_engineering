{creation_insertion_statement}
with offer_details as (
select
	*
	 /* How many food categories does the offer cover */
    ,offer_coffee + offer_burger + offer_chicken + offer_dessert + offer_snack + offer_breakfast + offer_mcveggie + offer_beef + offer_fish + 
    offer_fries + offer_salad + offer_vm + offer_happy_meal + offer_family + offer_big_mac + offer_cheeseburger + offer_qtr_p + offer_nuggets + 
    offer_hashbrown + offer_mcmuffin + offer_sundae + offer_shake as offer_food_cats
from {schema}.{offer_details_table}
)

/* Find out what the user did for that cycle*/
/* How much did they spend and what was the GP% */
,transaction_labels as 
(
    select
        offer_cycle
        ,mobile_customer_id
        ,sum(num_transactions) as num_transactions
        ,max(propensity) as redemptions
        ,sum(tld_net_amt) as spend
        ,sum(propensity_spend) as propensity_spend
        ,1 - (sum(fp_costs) / nullif(sum(gp_spend), 0)) as gp_perc -- if zero spend then null it
        ,1 - (sum(propensity_fp_costs) / nullif(sum(propensity_gp_spend), 0)) as propensity_gp_perc
    from {schema}.{daily_agg_table} da

    inner join {schema}.offer_cycle_dates oc
    on oc.offer_cycle between {start_cycle} and {end_cycle}
    and da.transaction_date_sk between cycle_start_date_sk and cycle_end_date_sk
    and exclude_fp = 0 -- don't want voted offers etc in target variables
	
	group by 1,2
)

/* Memory issues so limit to just the cycles we care about */
,transaction_features as
(
select * from {schema}.{transaction_features_table}
where offer_cycle between {start_cycle} and {end_cycle}
)

/* Memory issues so limit to just the cycles we care about */
,redemption_features as
(
select 
	* 
from {schema}.{redemption_features_table} rf

where offer_cycle between {start_cycle} and {end_cycle}
)

/* Memory issues so limit to just the cycles we care about */
,redemptions as
(
select 
	* 
from {schema}.{redemptions_table} r

where offer_cycle between {start_cycle} and {end_cycle}
)

/* Limit the redemption features to just the features associated with the offer sent out */
,redemptions_combined as
(
select
	rf.*
	,redeemed
from redemptions r

inner join redemption_features rf
on r.mobile_customer_id = rf.mobile_customer_id
and r.offer_id = rf.offer_id
)

select
    getdate() as audit_training_datetime
    ,r.offer_id
    ,offer_name
    ,t.*

   /* Customer Stats */
   ,customer_sent
   ,customer_redeemed
   ,customer_redemption_rate
   ,customer_last_redemption_sk
   ,customer_last_redemption_bk
   ,customer_days_since_redemption
   ,last_week_offers_sent
   ,last_week_offers_redeemed
   ,last_week_redemption_rate
   ,last_month_offers_sent
   ,last_month_offers_redeemed
   ,last_month_redemption_rate
   ,last_3_months_offers_sent
   ,last_3_months_offers_redeemed
   ,last_3_months_redemption_rate

   /* Offer Stats */
   ,offer_sent
   ,offer_redeemed
   ,offer_redemption_rate

   /* Customer Offer Stats */
   ,cust_off_sent
   ,cust_off_redeemed
   ,cust_off_redemption_rate
   ,cust_off_last_redemption_sk
   ,cust_off_last_redemption_bk
   ,cust_off_days_since_redemption

    /* Offer details features */
    ,price
    ,og_price
    ,discount
    ,discount_perc
    ,offer_coffee
    ,offer_burger
    ,offer_chicken
    ,offer_dessert
    ,offer_snack
    ,offer_breakfast
    ,offer_mcveggie
    ,offer_beef
    ,offer_fish
    ,offer_fries
    ,offer_salad
    ,offer_vm
    ,offer_happy_meal
    ,offer_family
    ,offer_big_mac
    ,offer_cheeseburger
    ,offer_qtr_p
    ,offer_nuggets
    ,offer_hashbrown
    ,offer_mcmuffin
    ,offer_sundae
    ,offer_shake


    /* How similar is the offer to the previous purchasing activity of the customer */
    ,(coffee_inc_prop * offer_coffee) + (burger_inc_prop * offer_burger) + (chicken_inc_prop * offer_chicken) + (snack_inc_prop * offer_snack) + 
    (dessert_inc_prop * offer_dessert) + (breakfast_inc_prop * offer_breakfast) + (beef_inc_prop * offer_beef) + (fish_inc_prop * offer_fish) + 
    (fries_inc_prop * offer_fries) + (mcveggie_inc_prop * offer_mcveggie) + (salad_inc_prop * offer_salad) + (happy_meal_inc_prop * offer_happy_meal) + 
    (family_inc_prop * offer_family) + (vm_inc_prop * offer_vm) + (shake_inc_prop * offer_shake) + (sundae_inc_prop * offer_sundae) + 
    (big_mac_inc_prop * offer_big_mac) + (qtr_p_inc_prop * offer_qtr_p) + (cheeseburger_inc_prop * offer_cheeseburger) + 
    (nuggets_inc_prop * offer_nuggets) + (hashbrown_inc_prop * offer_hashbrown) + (mcmuffin_inc_prop * offer_mcmuffin) as offer_similarity
	
    /* Same as above but looks at specifically discount transactions */
    ,(coffee_discount_prop * offer_coffee) + (burger_discount_prop * offer_burger) + (chicken_discount_prop * offer_chicken) + 
    (snack_discount_prop * offer_snack) + (dessert_discount_prop * offer_dessert) + (breakfast_discount_prop * offer_breakfast) + 
    (beef_discount_prop * offer_beef) + (fish_discount_prop * offer_fish) + (fries_discount_prop * offer_fries) + 
    (mcveggie_discount_prop * offer_mcveggie) + (salad_discount_prop * offer_salad) + (happy_meal_discount_prop * offer_happy_meal) + 
    (family_discount_prop * offer_family) + (vm_discount_prop * offer_vm) + (shake_discount_prop * offer_shake) + (sundae_discount_prop * offer_sundae) + 
    (big_mac_discount_prop * offer_big_mac) + (qtr_p_discount_prop * offer_qtr_p) + (cheeseburger_discount_prop * offer_cheeseburger) + 
    (nuggets_discount_prop * offer_nuggets) + (hashbrown_discount_prop * offer_hashbrown) + (mcmuffin_discount_prop * offer_mcmuffin) as offer_discount_similarity
    
    /* How many food categories does the offer cover */
    ,offer_food_cats
	
    /* Normalising the offer similarity depending on how many categories are covered by the offer */
    ,(
    (coffee_inc_prop * offer_coffee) + (burger_inc_prop * offer_burger) + (chicken_inc_prop * offer_chicken) + (snack_inc_prop * offer_snack) + 
    (dessert_inc_prop * offer_dessert) + (breakfast_inc_prop * offer_breakfast) + (beef_inc_prop * offer_beef) + (fish_inc_prop * offer_fish) + 
    (fries_inc_prop * offer_fries) + (mcveggie_inc_prop * offer_mcveggie) + (salad_inc_prop * offer_salad) + (happy_meal_inc_prop * offer_happy_meal) + 
    (family_inc_prop * offer_family) + (vm_inc_prop * offer_vm) + (shake_inc_prop * offer_shake) + (sundae_inc_prop * offer_sundae) + 
    (big_mac_inc_prop * offer_big_mac) + (qtr_p_inc_prop * offer_qtr_p) + (cheeseburger_inc_prop * offer_cheeseburger) + 
    (nuggets_inc_prop * offer_nuggets) + (hashbrown_inc_prop * offer_hashbrown) + (mcmuffin_inc_prop * offer_mcmuffin)
    ) * 1.0 / offer_food_cats as offer_similarity_normalised
    
    /* Normalising the offer discount similarity depending on how many categories are covered by the offer */
    ,(
    (coffee_discount_prop * offer_coffee) + (burger_discount_prop * offer_burger) + (chicken_discount_prop * offer_chicken) + 
    (snack_discount_prop * offer_snack) + (dessert_discount_prop * offer_dessert) + (breakfast_discount_prop * offer_breakfast) + 
    (beef_discount_prop * offer_beef) + (fish_discount_prop * offer_fish) + (fries_discount_prop * offer_fries) + 
    (mcveggie_discount_prop * offer_mcveggie) + (salad_discount_prop * offer_salad) + (happy_meal_discount_prop * offer_happy_meal) + 
    (family_discount_prop * offer_family) + (vm_discount_prop * offer_vm) + (shake_discount_prop * offer_shake) + (sundae_discount_prop * offer_sundae) + 
    (big_mac_discount_prop * offer_big_mac) + (qtr_p_discount_prop * offer_qtr_p) + (cheeseburger_discount_prop * offer_cheeseburger) + 
    (nuggets_discount_prop * offer_nuggets) + (hashbrown_discount_prop * offer_hashbrown) + (mcmuffin_discount_prop * offer_mcmuffin)
    ) * 1.0 / offer_food_cats as offer_discount_similarity_normalised

    ,r.redeemed as target_redeemed
    ,coalesce(l.spend, 0) as target_spend -- if they didn't spend anything in that cycle set it to zero
    ,coalesce(l.num_transactions, 0) as target_transactions
    ,coalesce(l.propensity_spend, 0) as target_propensity_spend
    ,l.gp_perc as target_gp_perc -- gp perc can be null as a gp perc on zero spend is / 0 hence infinite
    ,l.propensity_gp_perc as target_propensity_gp_perc

from transaction_features t

-- find out which offer was sent to which user and whether or not it was redeemed
-- Also the redemption features are in here now
inner join redemptions_combined r
on t.mobile_customer_id = r.mobile_customer_id

-- The offer attributes
inner join offer_details o
on r.offer_id = o.offer_id

-- The transaction labels how much did they spend and their GP%%
left join transaction_labels l
on t.offer_cycle = l.offer_cycle
and t.mobile_customer_id = l.mobile_customer_id