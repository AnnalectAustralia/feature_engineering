{creation_insertion_statement}
with transaction_features as (
select
    {offer_cycle} offer_cycle
    ,getdate() audit_transaction_datetime
    ,mobile_customer_id
    ,to_date('{model_date_sk}', 'YYYYMMDD') - min(transaction_date_bk) as days_since_first_transaction
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(transaction_date_bk) as days_since_last_transaction

    /* Aggregate values */
    ,count(1) as active_days
    ,sum(num_transactions) as num_transactions
    ,sum(tld_net_amt) as tld_net_amt
    ,sum(tld_units_cnt) as total_units
    ,sum(gp_spend * mod(exclude_fp + 1, 2)) as tld_gp_spend
    ,sum(fp_costs * mod(exclude_fp + 1, 2)) as tld_fp_costs

    /* Proportion of transactions that are excluded */
    ,sum(num_transactions * exclude_fp) * 1.0 / sum(num_transactions) as exclude_prop
    
    /* Normalised aggregates */
    ,count(1) * 1.0 / (to_date('{model_date_sk}', 'YYYYMMDD') - min(transaction_date_bk)) as active_days_prop
    ,sum(num_transactions) * 1.0 / (to_date('{model_date_sk}', 'YYYYMMDD') - min(transaction_date_bk)) as transactions_per_day
    ,(to_date('{model_date_sk}', 'YYYYMMDD') - min(transaction_date_bk)) * 1.0 / sum(num_transactions) as avg_days_between_transactions
    ,sum(tld_net_amt) / sum(num_transactions) as avg_cheque
    ,sum(tld_units_cnt) * 1.0 / sum(num_transactions) as avg_units_per_transaction

    /* GP% */
    ,1.0 - sum(fp_costs * mod(exclude_fp + 1, 2)) / nullif(sum(gp_spend * mod(exclude_fp + 1, 2)), 0) as gp_perc
    ,1.0 - sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 7 then fp_costs * mod(exclude_fp + 1, 2) end) / 
    nullif(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 7 then gp_spend * mod(exclude_fp + 1, 2) end), 0) as gp_perc_last_week
    ,1.0 - sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 30 then fp_costs * mod(exclude_fp + 1, 2) end) / 
    nullif(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 30 then gp_spend * mod(exclude_fp + 1, 2) end), 0) as gp_perc_last_month
    ,1.0 - sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 90 then fp_costs * mod(exclude_fp + 1, 2) end) / 
    nullif(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 90 then gp_spend * mod(exclude_fp + 1, 2) end), 0) as gp_perc_last_3_months

    /* Cheque Bands */
    ,sum(mini_cheque_band) * 1.0 / sum(num_transactions) as mini_cheque_band_prop
    ,sum(small_cheque_band) * 1.0 / sum(num_transactions) as small_cheque_band_prop
    ,sum(medium_cheque_band) * 1.0 / sum(num_transactions) as medium_cheque_band_prop
    ,sum(large_cheque_band) * 1.0 / sum(num_transactions) as large_cheque_band_prop
    ,sum(supersize_cheque_band) * 1.0 / sum(num_transactions) as supersize_cheque_band_prop

    /* Propensity */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when propensity > 0 then transaction_date_bk end) as days_since_propensity
    ,sum(propensity) * 1.0 / sum(num_transactions) as propensity_prop
    ,sum(propensity) * 1.0 / 
        (to_date('{model_date_sk}', 'YYYYMMDD') - 
        case 
            when min(transaction_date_sk) > 20190710 then min(transaction_date_bk)
            else to_date('20190710', 'YYYYMMDD')
        end -- propensity only started on the 10th July 2019
    ) as propensity_per_day
    ,(to_date('{model_date_sk}', 'YYYYMMDD') - 
        case 
            when min(transaction_date_sk) > 20190710 then min(transaction_date_bk)
            else to_date('20190710', 'YYYYMMDD')
        end -- propensity only started on the 10th July 2019
    ) * 1.0 / nullif(sum(propensity), 0) as avg_days_between_propensity
    ,sum(propensity_spend) / nullif(sum(propensity), 0) as propensity_avg_cheque
    ,sum((propensity_gp_spend - propensity_fp_costs) * mod(exclude_fp + 1, 2)) / nullif(sum(propensity_gp_spend * mod(exclude_fp + 1, 2)), 0) as propensity_gp_perc
    ,coalesce(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 7 then propensity end), 0) as propensity_last_week
    ,coalesce(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 30 then propensity end), 0) as propensity_last_month
    ,coalesce(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 90 then propensity end), 0) as propensity_last_3_months

    /* Mass Offer */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when mass_offer > 0 then transaction_date_bk end) as days_since_mass_offer
    ,sum(mass_offer) * 1.0 / sum(num_transactions) as mass_offer_prop
    ,sum(mass_offer) * 1.0 / (to_date('{model_date_sk}', 'YYYYMMDD') - min(transaction_date_bk)) as mass_offer_per_day
    ,(to_date('{model_date_sk}', 'YYYYMMDD') - min(transaction_date_bk)) * 1.0 / nullif(sum(mass_offer), 0) as avg_days_between_mass_offer
    ,sum(mass_offer_spend) / nullif(sum(mass_offer), 0) as mass_offer_avg_cheque
    ,sum((mass_offer_spend - mass_offer_fp_costs) * mod(exclude_fp + 1, 2)) / nullif(sum(mass_offer_gp_spend * mod(exclude_fp + 1, 2)), 0) as mass_offer_gp_perc
    ,coalesce(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 7 then mass_offer end), 0) as mass_offer_last_week
    ,coalesce(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 30 then mass_offer end), 0) as mass_offer_last_month
    ,coalesce(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 90 then mass_offer end), 0) as mass_offer_last_3_months

    /* Discount */
    /* ,sum(discount_amt) / sum(num_transactions) as avg_discountdiscount_price */
    /* ,coalesce(sum(discount_amt) / sum(tld_net_amt + discount_amt), 0.0) as discount_perc */
    /* ,sum(discount_amt) / sum(case when discount_amt > 0 then tld_net_amt + discount_amt end) as used_discount_perc */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when discount > 0 then transaction_date_bk end) as days_since_discount
    ,sum(discount) * 1.0 / (to_date('{model_date_sk}', 'YYYYMMDD') - min(transaction_date_bk)) as discount_per_day
    ,(to_date('{model_date_sk}', 'YYYYMMDD') - min(transaction_date_bk)) * 1.0 / nullif(sum(discount), 0) as avg_days_between_discount
    ,sum(discount) * 1.0 / sum(num_transactions) as discount_prop
    ,sum(discount_price) * 1.0 / sum(num_transactions) as discount_price_prop
    ,sum(discount_promo) * 1.0 / sum(num_transactions) as discount_promo_prop 
    ,coalesce(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 7 then discount end), 0) as discount_last_week
    ,coalesce(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 30 then discount end), 0) as discount_last_month
    ,coalesce(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 90 then discount end), 0) as discount_last_3_months

    /* In Last X Days/Weeks/Months */
    ,sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 7 then 1 else 0 end) * 1.0 / 7 as last_week_active_prop
    ,sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 30 then 1 else 0 end) * 1.0 / 30 as last_month_active_prop
    ,sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 90 then 1 else 0 end) * 1.0 / 90 as last_3_months_active_prop

    ,sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 7 then tld_net_amt else 0 end) / 
        nullif(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 7 then 1 else 0 end), 0) as last_week_avg_cheque
    ,sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 30 then tld_net_amt else 0 end) / 
        nullif(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 30 then 1 else 0 end), 0) as last_month_avg_cheque
    ,sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 90 then tld_net_amt else 0 end) / 
        nullif(sum(case when to_date('{model_date_sk}', 'YYYYMMDD') - transaction_date_bk <= 90 then 1 else 0 end), 0) as last_3_months_avg_cheque
    
    /* Temporal */
    ,sum(case day_type when 'Weekend' then breakfast else 0 end) * 1.0 / sum(num_transactions) as weekend_breakfast
    ,sum(case day_type when 'Weekend' then lunch else 0 end) * 1.0 / sum(num_transactions) as weekend_lunch
    ,sum(case day_type when 'Weekend' then tea else 0 end) * 1.0 / sum(num_transactions) as weekend_tea
    ,sum(case day_type when 'Weekend' then dinner else 0 end) * 1.0 / sum(num_transactions) as weekend_dinner
    ,sum(case day_type when 'Weekend' then late else 0 end) * 1.0 / sum(num_transactions) as weekend_late
    ,sum(case day_type when 'Workday' then breakfast else 0 end) * 1.0 / sum(num_transactions) as workday_breakfast
    ,sum(case day_type when 'Workday' then lunch else 0 end) * 1.0 / sum(num_transactions) as workday_lunch
    ,sum(case day_type when 'Workday' then tea else 0 end) * 1.0 / sum(num_transactions) as workday_tea
    ,sum(case day_type when 'Workday' then dinner else 0 end) * 1.0 / sum(num_transactions) as workday_dinner
    ,sum(case day_type when 'Workday' then late else 0 end) * 1.0 / sum(num_transactions) as workday_late

    ,sum(case day_type when 'Workday' then tld_net_amt else 0 end) / nullif(sum(case day_type when 'Workday' then num_transactions end), 0) as avg_cheque_workday
    ,sum(case day_type when 'Workday' then tld_units_cnt else 0 end) * 1.0 / nullif(sum(case day_type when 'Workday' then num_transactions end), 0) as avg_units_workday
    ,sum(case day_type when 'Weekend' then tld_net_amt else 0 end) / nullif(sum(case day_type when 'Weekend' then num_transactions end), 0) as avg_cheque_weekend
    ,sum(case day_type when 'Weekend' then tld_units_cnt else 0 end) * 1.0 / nullif(sum(case day_type when 'Weekend' then num_transactions end), 0) as avg_units_weekend

    /* Geo */
    ,sum(city) * 1.0 / sum(num_transactions) as city
    ,sum(suburban) * 1.0 / sum(num_transactions) as suburban
    ,sum(country) * 1.0 / sum(num_transactions) as country

    /* Store Type */
    ,sum(store_free_standing) * 1.0 / sum(num_transactions) as store_free_standing
    ,sum(store_instore) * 1.0 / sum(num_transactions) as store_instore
    ,sum(store_food_court) * 1.0 / sum(num_transactions) as store_food_court
    ,sum(store_servo) * 1.0 / sum(num_transactions) as store_servo
    ,sum(store_other) * 1.0 / sum(num_transactions) as store_other

    /* Point of Delivery */
    ,sum(online) * 1.0 / sum(num_transactions) as online_proc
    ,sum(mobile) * 1.0 / sum(num_transactions) as mobile_proc
    ,sum(in_store) * 1.0 / sum(num_transactions) as in_store_proc
    ,sum(drive_thru) * 1.0 / sum(num_transactions) as drive_thru_proc

    /* Food */
    
    /* coffee */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when coffee_inc > 0 then transaction_date_bk end) as coffee_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when coffee_discount > 0 then transaction_date_bk end) as coffee_days_since_discount
    ,sum(coffee_inc) * 1.0 / sum(num_transactions) as coffee_inc_prop  
    ,coalesce(sum(coffee_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as coffee_units_prop
    ,coalesce(sum(coffee_spend) / nullif(sum(tld_net_amt), 0.0), 0) as coffee_spend_prop
    ,coalesce(sum(coffee_discount) * 1.0 / nullif(sum(coffee_inc), 0.0), 0) as coffee_discount_prop

    /* burger */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when burger_inc > 0 then transaction_date_bk end) as burger_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when burger_discount > 0 then transaction_date_bk end) as burger_days_since_discount
    ,sum(burger_inc) * 1.0 / sum(num_transactions) as burger_inc_prop
    ,coalesce(sum(burger_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as burger_units_prop
    ,coalesce(sum(burger_spend) / nullif(sum(tld_net_amt), 0.0), 0) as burger_spend_prop
    ,coalesce(sum(burger_discount) * 1.0 / nullif(sum(burger_inc), 0.0), 0) as burger_discount_prop

    /* chicken */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when chicken_inc > 0 then transaction_date_bk end) as chicken_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when chicken_discount > 0 then transaction_date_bk end) as chicken_days_since_discount
    ,sum(chicken_inc) * 1.0 / sum(num_transactions) as chicken_inc_prop
    ,coalesce(sum(chicken_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as chicken_units_prop
    ,coalesce(sum(chicken_spend) / nullif(sum(tld_net_amt), 0.0), 0) as chicken_spend_prop
    ,coalesce(sum(chicken_discount) * 1.0 / nullif(sum(chicken_inc), 0.0), 0) as chicken_discount_prop

    /* snack */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when snack_inc > 0 then transaction_date_bk end) as snack_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when snack_discount > 0 then transaction_date_bk end) as snack_days_since_discount
    ,sum(snack_inc) * 1.0 / sum(num_transactions) as snack_inc_prop
    ,coalesce(sum(snack_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as snack_units_prop
    ,coalesce(sum(snack_spend) / nullif(sum(tld_net_amt), 0.0), 0) as snack_spend_prop
    ,coalesce(sum(snack_discount) * 1.0 / nullif(sum(snack_inc), 0.0), 0) as snack_discount_prop

    /* dessert */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when dessert_inc > 0 then transaction_date_bk end) as dessert_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when dessert_discount > 0 then transaction_date_bk end) as dessert_days_since_discount
    ,sum(dessert_inc) * 1.0 / sum(num_transactions) as dessert_inc_prop
    ,coalesce(sum(dessert_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as dessert_units_prop
    ,coalesce(sum(dessert_spend) / nullif(sum(tld_net_amt), 0.0), 0) as dessert_spend_prop
    ,coalesce(sum(dessert_discount) * 1.0 / nullif(sum(dessert_inc), 0.0), 0) as dessert_discount_prop

    /* breakfast */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when breakfast_inc > 0 then transaction_date_bk end) as breakfast_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when breakfast_discount > 0 then transaction_date_bk end) as breakfast_days_since_discount
    ,sum(breakfast_inc) * 1.0 / sum(num_transactions) as breakfast_inc_prop
    ,coalesce(sum(breakfast_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as breakfast_units_prop
    ,coalesce(sum(breakfast_spend) / nullif(sum(tld_net_amt), 0.0), 0) as breakfast_spend_prop
    ,coalesce(sum(breakfast_discount) * 1.0 / nullif(sum(breakfast_inc), 0.0), 0) as breakfast_discount_prop

    /* beef */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when beef_inc > 0 then transaction_date_bk end) as beef_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when beef_discount > 0 then transaction_date_bk end) as beef_days_since_discount
    ,sum(beef_inc) * 1.0 / sum(num_transactions) as beef_inc_prop
    ,coalesce(sum(beef_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as beef_units_prop
    ,coalesce(sum(beef_spend) / nullif(sum(tld_net_amt), 0.0), 0) as beef_spend_prop
    ,coalesce(sum(beef_discount) * 1.0 / nullif(sum(beef_inc), 0.0), 0) as beef_discount_prop

    /* fish */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when fish_inc > 0 then transaction_date_bk end) as fish_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when fish_discount > 0 then transaction_date_bk end) as fish_days_since_discount
    ,sum(fish_inc) * 1.0 / sum(num_transactions) as fish_inc_prop
    ,coalesce(sum(fish_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as fish_units_prop
    ,coalesce(sum(fish_spend) / nullif(sum(tld_net_amt), 0.0), 0) as fish_spend_prop
    ,coalesce(sum(fish_discount) * 1.0 / nullif(sum(fish_inc), 0.0), 0) as fish_discount_prop

    /* fries */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when fries_inc > 0 then transaction_date_bk end) as fries_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when fries_discount > 0 then transaction_date_bk end) as fries_days_since_discount
    ,sum(fries_inc) * 1.0 / sum(num_transactions) as fries_inc_prop
    ,coalesce(sum(fries_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as fries_units_prop
    ,coalesce(sum(fries_spend) / nullif(sum(tld_net_amt), 0.0), 0) as fries_spend_prop
    ,coalesce(sum(fries_discount) * 1.0 / nullif(sum(fries_inc), 0.0), 0) as fries_discount_prop

    /* mcveggie */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when mcveggie_inc > 0 then transaction_date_bk end) as mcveggie_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when mcveggie_discount > 0 then transaction_date_bk end) as mcveggie_days_since_discount
    ,sum(mcveggie_inc) * 1.0 / sum(num_transactions) as mcveggie_inc_prop
    ,coalesce(sum(mcveggie_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as mcveggie_units_prop
    ,coalesce(sum(mcveggie_spend) / nullif(sum(tld_net_amt), 0.0), 0) as mcveggie_spend_prop
    ,coalesce(sum(mcveggie_discount) * 1.0 / nullif(sum(mcveggie_inc), 0.0), 0) as mcveggie_discount_prop

    /* salad */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when salad_inc > 0 then transaction_date_bk end) as salad_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when salad_discount > 0 then transaction_date_bk end) as salad_days_since_discount
    ,sum(salad_inc) * 1.0 / sum(num_transactions) as salad_inc_prop
    ,coalesce(sum(salad_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as salad_units_prop
    ,coalesce(sum(salad_spend) / nullif(sum(tld_net_amt), 0.0), 0) as salad_spend_prop
    ,coalesce(sum(salad_discount) * 1.0 / nullif(sum(salad_inc), 0.0), 0) as salad_discount_prop

    /* happy_meal */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when happy_meal_inc > 0 then transaction_date_bk end) as happy_meal_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when happy_meal_discount > 0 then transaction_date_bk end) as happy_meal_days_since_discount
    ,sum(happy_meal_inc) * 1.0 / sum(num_transactions) as happy_meal_inc_prop
    ,coalesce(sum(happy_meal_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as happy_meal_units_prop
    ,coalesce(sum(happy_meal_spend) / nullif(sum(tld_net_amt), 0.0), 0) as happy_meal_spend_prop
    ,coalesce(sum(happy_meal_discount) * 1.0 / nullif(sum(happy_meal_inc), 0.0), 0) as happy_meal_discount_prop

    /* family */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when family_inc > 0 then transaction_date_bk end) as family_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when family_discount > 0 then transaction_date_bk end) as family_days_since_discount
    ,sum(family_inc) * 1.0 / sum(num_transactions) as family_inc_prop
    ,coalesce(sum(family_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as family_units_prop
    ,coalesce(sum(family_spend) / nullif(sum(tld_net_amt), 0.0), 0) as family_spend_prop
    ,coalesce(sum(family_discount) * 1.0 / nullif(sum(family_inc), 0.0), 0) as family_discount_prop

    /* vm */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when vm_inc > 0 then transaction_date_bk end) as vm_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when vm_discount > 0 then transaction_date_bk end) as vm_days_since_discount
    ,sum(vm_inc) * 1.0 / sum(num_transactions) as vm_inc_prop
    ,coalesce(sum(vm_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as vm_units_prop
    ,coalesce(sum(vm_spend) / nullif(sum(tld_net_amt), 0.0), 0) as vm_spend_prop
    ,coalesce(sum(vm_discount) * 1.0 / nullif(sum(vm_inc), 0.0), 0) as vm_discount_prop

    /* shake */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when shake_inc > 0 then transaction_date_bk end) as shake_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when shake_discount > 0 then transaction_date_bk end) as shake_days_since_discount
    ,sum(shake_inc) * 1.0 / sum(num_transactions) as shake_inc_prop
    ,coalesce(sum(shake_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as shake_units_prop
    ,coalesce(sum(shake_spend) / nullif(sum(tld_net_amt), 0.0), 0) as shake_spend_prop
    ,coalesce(sum(shake_discount) * 1.0 / nullif(sum(shake_inc), 0.0), 0) as shake_discount_prop

    /* sundae */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when sundae_inc > 0 then transaction_date_bk end) as sundae_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when sundae_discount > 0 then transaction_date_bk end) as sundae_days_since_discount
    ,sum(sundae_inc) * 1.0 / sum(num_transactions) as sundae_inc_prop
    ,coalesce(sum(sundae_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as sundae_units_prop
    ,coalesce(sum(sundae_spend) / nullif(sum(tld_net_amt), 0.0), 0) as sundae_spend_prop
    ,coalesce(sum(sundae_discount) * 1.0 / nullif(sum(sundae_inc), 0.0), 0) as sundae_discount_prop

    /* big_mac */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when big_mac_inc > 0 then transaction_date_bk end) as big_mac_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when big_mac_discount > 0 then transaction_date_bk end) as big_mac_days_since_discount
    ,sum(big_mac_inc) * 1.0 / sum(num_transactions) as big_mac_inc_prop
    ,coalesce(sum(big_mac_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as big_mac_units_prop
    ,coalesce(sum(big_mac_spend) / nullif(sum(tld_net_amt), 0.0), 0) as big_mac_spend_prop
    ,coalesce(sum(big_mac_discount) * 1.0 / nullif(sum(big_mac_inc), 0.0), 0) as big_mac_discount_prop

    /* qtr_p */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when qtr_p_inc > 0 then transaction_date_bk end) as qtr_p_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when qtr_p_discount > 0 then transaction_date_bk end) as qtr_p_days_since_discount
    ,sum(qtr_p_inc) * 1.0 / sum(num_transactions) as qtr_p_inc_prop
    ,coalesce(sum(qtr_p_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as qtr_p_units_prop
    ,coalesce(sum(qtr_p_spend) / nullif(sum(tld_net_amt), 0.0), 0) as qtr_p_spend_prop
    ,coalesce(sum(qtr_p_discount) * 1.0 / nullif(sum(qtr_p_inc), 0.0), 0) as qtr_p_discount_prop

    /* cheeseburger */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when cheeseburger_inc > 0 then transaction_date_bk end) as cheeseburger_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when cheeseburger_discount > 0 then transaction_date_bk end) as cheeseburger_days_since_discount
    ,sum(cheeseburger_inc) * 1.0 / sum(num_transactions) as cheeseburger_inc_prop
    ,coalesce(sum(cheeseburger_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as cheeseburger_units_prop
    ,coalesce(sum(cheeseburger_spend) / nullif(sum(tld_net_amt), 0.0), 0) as cheeseburger_spend_prop
    ,coalesce(sum(cheeseburger_discount) * 1.0 / nullif(sum(cheeseburger_inc), 0.0), 0) as cheeseburger_discount_prop

    /* nuggets */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when nuggets_inc > 0 then transaction_date_bk end) as nuggets_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when nuggets_discount > 0 then transaction_date_bk end) as nuggets_days_since_discount
    ,sum(nuggets_inc) * 1.0 / sum(num_transactions) as nuggets_inc_prop
    ,coalesce(sum(nuggets_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as nuggets_units_prop
    ,coalesce(sum(nuggets_spend) / nullif(sum(tld_net_amt), 0.0), 0) as nuggets_spend_prop
    ,coalesce(sum(nuggets_discount) * 1.0 / nullif(sum(nuggets_inc), 0.0), 0) as nuggets_discount_prop

    /* hashbrown */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when hashbrown_inc > 0 then transaction_date_bk end) as hashbrown_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when hashbrown_discount > 0 then transaction_date_bk end) as hashbrown_days_since_discount
    ,sum(hashbrown_inc) * 1.0 / sum(num_transactions) as hashbrown_inc_prop
    ,coalesce(sum(hashbrown_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as hashbrown_units_prop
    ,coalesce(sum(hashbrown_spend) / nullif(sum(tld_net_amt), 0.0), 0) as hashbrown_spend_prop
    ,coalesce(sum(hashbrown_discount) * 1.0 / nullif(sum(hashbrown_inc), 0.0), 0) as hashbrown_discount_prop

    /* mcmuffin */
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when mcmuffin_inc > 0 then transaction_date_bk end) as mcmuffin_days_since
    ,to_date('{model_date_sk}', 'YYYYMMDD') - max(case when mcmuffin_discount > 0 then transaction_date_bk end) as mcmuffin_days_since_discount
    ,sum(mcmuffin_inc) * 1.0 / sum(num_transactions) as mcmuffin_inc_prop
    ,coalesce(sum(mcmuffin_units) * 1.0 / nullif(sum(tld_units_cnt), 0.0), 0) as mcmuffin_units_prop
    ,coalesce(sum(mcmuffin_spend) / nullif(sum(tld_net_amt), 0.0), 0) as mcmuffin_spend_prop
    ,coalesce(sum(mcmuffin_discount) * 1.0 / nullif(sum(mcmuffin_inc), 0.0), 0) as mcmuffin_discount_prop

from {schema}.{source_table}

where transaction_date_sk < {model_date_sk}
and mobile_customer_id not in ('0') -- a defect mobile id

group by 1,2,3
)

select
    *

    /* MD features */
    ,days_since_propensity / nullif(avg_days_between_propensity, 0) as propensity_overdue_or_underdue
    ,days_since_last_transaction / nullif(avg_days_between_transactions, 0) as transaction_overdue_or_underdue
    ,avg_days_between_transactions / nullif(avg_days_between_propensity, 0) as avg_days_between_transaction_div_propensity

    ,case 
        when days_since_propensity = days_since_last_transaction then 1
        else 0
    end as days_since_prop_eq_last_tran_flag

    ,propensity_last_week / nullif(propensity_last_month * 7.0 / 30, 0) as propensity_last_week_div_last_month
    ,propensity_last_week / nullif(propensity_last_3_months * 7.0 / 90, 0) as propensity_last_week_div_last_3_months
    ,propensity_last_month / nullif(propensity_last_3_months * 1.0 / 3, 0) as propensity_last_month_div_last_3_months

    ,propensity_last_week - nullif(propensity_last_month * 7.0 / 30, 0) as propensity_last_week_diff_last_month
    ,propensity_last_week - nullif(propensity_last_3_months * 7.0 / 90, 0) as propensity_last_week_diff_last_3_months
    ,propensity_last_month - nullif(propensity_last_3_months * 1.0 / 3, 0) as propensity_last_month_diff_last_3_months

    /* Proportion of transactions in the last X weeks/months that include a given discount type */

    ,propensity_last_week / nullif(last_week_active_prop * 7, 0) as propensity_last_week_prop
    ,propensity_last_month / nullif(last_month_active_prop * 30, 0) as propensity_last_month_prop
    ,propensity_last_3_months / nullif(last_3_months_active_prop * 90, 0) as propensity_last_3_months_prop

    ,mass_offer_last_week / nullif(last_week_active_prop * 7, 0) as mass_offer_last_week_prop
    ,mass_offer_last_month / nullif(last_month_active_prop * 30, 0) as mass_offer_last_month_prop
    ,mass_offer_last_3_months / nullif(last_3_months_active_prop * 90, 0) as mass_offer_last_3_months_prop

    ,discount_last_week / nullif(last_week_active_prop * 7, 0) as discount_last_week_prop
    ,discount_last_month / nullif(last_month_active_prop * 30, 0) as discount_last_month_prop
    ,discount_last_3_months / nullif(last_3_months_active_prop * 90, 0) as discount_last_3_months_prop

    /* Food Segment */
    ,case

        when days_since_first_transaction <= 15
        then 'New_User'

        when num_transactions < 4
        then 'Low_Transaction_User'

        when family_inc_prop > 0.1
        and vm_inc_prop > 0.5
        then 'family_or_happy_meal_lover'

        when mcveggie_inc_prop > 0.4
        then 'salad_veggie_lover'

        when burger_inc_prop > 0.5
        and salad_inc_prop > 0.2
        then 'salad_veggie_lover'

        when fish_inc_prop > 0.3
        then 'fish_lover'    

        when happy_meal_inc_prop > 0.4
        then 'family_or_happy_meal_lover'

        when snack_inc_prop > 0.4
        and dessert_inc_prop > 0.4
        then 'snack_or_dessert_lover'

        when dessert_inc_prop > 0.4
        then 'dessert_lover'

        when shake_inc_prop > 0.4
        then 'shake_lover'

        when burger_inc_prop > 0.5
        and chicken_inc_prop > 0.4
        then 'chicken_burger'

        when breakfast_inc_prop > 0.4
        and coffee_inc_prop > 0.4
        then 'breakfast_with_coffee_lover'

        when coffee_inc_prop > 0.5
        then 'coffee_lover'

        when nuggets_inc_prop > 0.3
        then 'nuggets_lover'

        when burger_inc_prop > 0.5
        and beef_inc_prop > 0.5
        then 'beef_burger_lover'

        else 'other'
    end as food_segment

    /* Offer dependency segment */
    ,case

        when days_since_first_transaction <= 15
        then 'New_User'

        when num_transactions < 4
        then 'Low_Transaction_User'

        when discount_prop < 0.25
        then 'Offer1_0-25PctOffers'

        when discount_prop < 0.50
        then 'Offer2_25-50PctOffers'

        when discount_prop < 0.8
        then 'Offer3_50-80PctOffers'

        when discount_prop <= 0.95
        then 'Offer4_80-95PctOffers'

        else 'Offer5_95-100PctOffers'
    end as offer_segment

    /* Monetary Segments */
    ,case

        when days_since_first_transaction <= 15
        then 'New_User'

        when num_transactions < 4
        then 'Low_Transaction_User'

        when avg_cheque <= 4
        then 'M1_Mini'

        when avg_cheque <= 7
        then 'M2_Small'

        when avg_cheque <= 10
        then 'M3_Medium'

        when avg_cheque <= 15
        then 'M4_Large'

        else 'M5_Supersize'
    end as monetary_segment

    /* Recency & Frequency Segment */
    ,case

        when days_since_first_transaction <= 15
        then 'New_User'

        when num_transactions < 4 AND days_since_last_transaction <= 21
        then 'Low_Transaction_User_1_0-21Days'

        when num_transactions < 4 AND days_since_last_transaction <= 45
        then 'Low_Transaction_User_2_22-45Days'

        when num_transactions < 4 AND days_since_last_transaction <= 90
        then 'Low_Transaction_User_3_46-90Days'

        when num_transactions < 4
        then 'Low_Transaction_User_4_90DaysPlus'

        when days_since_last_transaction <= 3
        and avg_days_between_transactions <= 7
        then 'RF1_Super'

        when days_since_last_transaction > 180
        or avg_days_between_transactions > 180
        then 'RF5_Dormant'

        when days_since_last_transaction <= 7
        and avg_days_between_transactions <= 14
        then 'RF1_Super'

        when days_since_last_transaction <= 21
        and avg_days_between_transactions <= 21
        then 'RF2_Regular'

        when days_since_last_transaction > 21
        and days_since_last_transaction <= 45
        and avg_days_between_transactions <= 14
        then 'RF_Dormant_Regular'

        when avg_days_between_transactions > 21
        and avg_days_between_transactions <=45
        and days_since_last_transaction <= 7
        then 'RF_Recent_Dormant'

        when days_since_last_transaction <= 45
        and avg_days_between_transactions <= 45
        then 'RF3_At_Risk'

        when days_since_last_transaction > 45
        and days_since_last_transaction <= 90
        and avg_days_between_transactions > 7
        and avg_days_between_transactions <= 21
        then 'RF_Dormant_Regular'

        when avg_days_between_transactions > 45
        and avg_days_between_transactions <= 180
        and days_since_last_transaction <= 21
        then 'RF_Recent_Dormant'

        when days_since_last_transaction > 90
        and days_since_last_transaction <= 180
        and avg_days_between_transactions <= 60
        then 'RF_Dormant_Regular'

        when days_since_last_transaction > 45
        and days_since_last_transaction <= 180
        and avg_days_between_transactions <= 7
        then 'RF_Dormant_Regular'

        when days_since_last_transaction > 21
        and days_since_last_transaction <= 60
        and avg_days_between_transactions > 60
        and avg_days_between_transactions <= 180
        then 'RF_Recent_Dormant'

        when days_since_last_transaction > 60
        and avg_days_between_transactions > 90
        then 'RF4_Inactive'

        when days_since_last_transaction <= 90
        and avg_days_between_transactions <= 180
        then 'RF4_Inactive'

        else 'RF4_Inactive'
    end as rf_segment

from transaction_features