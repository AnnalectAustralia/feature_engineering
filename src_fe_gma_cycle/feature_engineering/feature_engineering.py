from src_fe_gma_cycle.data_preprocessing.preprocessing import OfferCycles
from src.utils import Redshift, Utils
from datetime import date, datetime
import logging


class Features(object):
    """
    A class to generate the Transaction or Redemption Features for the Model

    Parameters
    ----------

    rs: RedShift
    The redshift connection

    table_name_prefix: str
    The prefix for the output table. The output table will be named {table_name_prefix}_{model_date_sk}.

    schema: str
    The schema for all the tables

    template_path: str
    The path for the SQL template for this query

    source_table: str
    The source table for the feature engineering.
    Redemptions: "redemptions"
    Transactions: "daily_aggregates_new"
    """

    def __init__(
        self,
        table_name='redemption_features',
        schema='annalect',
        template_path='src_fe_gma_cycle/feature_engineering/templates/redemption_features.sql',
        source_table='redemptions'
    ) -> None:

        # output table and schema
        self.table_name = table_name
        self.schema = schema

        # path to sql template
        self.template_path = template_path

        # the source table where the preprocessed table lies
        self.source_table = source_table

        # create the local logger
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.debug(
            f"Feature class created with "
            f"schema: {schema}, table_name: {table_name}, "
            f"source_table: {source_table} and template_path: {template_path}"
        )

    @property
    def full_table_name(self):
        return f"{self.schema}.{self.table_name}"

    def create_features(
        self,
        rs: Redshift,
        model_date: date,
        offer_cycle: int,
        overwrite: bool = False
    ) -> None:

        self.logger.info(
            f"Create features for {self.table_name} for offer cycle {offer_cycle} for date {model_date}")
        _start = datetime.now()

        # convert the date to an sk
        model_date_sk = Utils.date_to_sk(model_date)

        # assume table doesn't exist so create a
        creation_insertion_statement = f'create table {self.full_table_name} as '

        # do we run the query?
        run_query = True

        # does the table exist
        if rs.table_exists(self.table_name, self.schema):

            creation_insertion_statement = f'insert into {self.full_table_name} '

            self.logger.info(f'Check for values in {self.full_table_name}')

            count = rs.get_count(
                self.table_name,
                self.schema,
                offer_cycle=offer_cycle
            )

            self.logger.info(
                f"{count} values exist in {self.full_table_name}")

            # are there values from this query
            if count > 0:

                # if we want to overwrite just drop the table
                if overwrite:
                    self.logger.info(
                        f"Deleting values for offer cycle {offer_cycle}"
                        f" in {self.full_table_name}"
                    )
                    rs.delete_from(
                        self.table_name,
                        self.schema,
                        offer_cycle=offer_cycle
                    )

                # otherwise finish the function
                else:
                    self.logger.info(
                        f"Values already exist for offer cycle {offer_cycle} "
                        f"in table {self.full_table_name} ."
                        "Set `overwrite=True` to overwrite."
                    )

                    run_query = False

        if run_query:
            query = Utils.generate_query(
                path=self.template_path,
                model_date_sk=model_date_sk,
                schema=self.schema,
                offer_cycle=offer_cycle,
                creation_insertion_statement=creation_insertion_statement,
                source_table=self.source_table
            )

            self.logger.info("Running create_features query")

            # run it
            rs.query(query)

        _end = datetime.now()
        self.logger.info(
            f'create_features processing time:  {str(_end - _start)}')

    def update(self, rs: Redshift, ocd: OfferCycles, overwrite: bool = False):
        self.logger.info(
            f"Update {self.table_name}")
        _start = datetime.now()

        # find the most recent offer cycle in the processed redemptions
        max_offer_cycle = rs.query(
            f'select max(offer_cycle) from {self.full_table_name}',
            return_df=True
        ).values[0][0]

        # the current date in sk format
        # we don't want to process offer cycles which are still active
        today_sk = Utils.date_to_sk(datetime.now().date())

        # find the offer cycles since the last update and need to be calculated
        offer_cycles = rs.query(
            f'select offer_cycle, model_date_sk from {ocd.full_table_name} '
            f'where offer_cycle >= {max_offer_cycle} '
            f'and model_date_sk <= {today_sk}',
            return_df=True
        )

        for _, r in offer_cycles.iterrows():

            self.create_features(
                rs,
                Utils.sk_to_date(r['model_date_sk']),
                r['offer_cycle'],
                overwrite
            )

        _end = datetime.now()
        self.logger.info(
            f'update processing time:  {str(_end - _start)}')

    def features_exist(self, rs: Redshift, offer_cycle: int):
        """[summary]

        Args:
            rs (Redshift): [description]
            offer_cycle (int): [description]

        Returns:
            bool: Do the features already exist
        """
        features_exist = True

        self.logger.debug(
            f"Checking if values for offer cycle {offer_cycle}"
            f" in {self.schema}.{self.table_name} exist"
        )

        # does the table exist
        if rs.table_exists(self.table_name, self.schema):

            # find out how many rows
            feature_count = rs.get_count(
                self.table_name,
                self.schema,
                offer_cycle=offer_cycle
            )

            if feature_count == 0:
                self.logger.info(
                    f"No rows for offer cycle {offer_cycle} in {self.schema}.{self.table_name} exist"
                )
                features_exist = False
            else:
                self.logger.info(
                    f"{feature_count} values exist for"
                    f"offer cycle {offer_cycle} in {self.schema}.{self.table_name}"
                )
        else:
            self.logger.info(
                f"Table {self.schema}.{self.table_name} does not exist"
            )
            features_exist = False

        return features_exist


class RedemptionFeatures:

    def __init__(
        self,
        customer_stats: Features,
        offer_stats: Features,
        customer_offer_stats: Features,
        schema: str = 'annalect',
        view_name: str = 'redemption_features',
        template_path: str = 'src_fe_gma_cycle/feature_engineering/templates/redemption_features_view.sql'
    ) -> None:

        # class for each of the types of stats we collect
        self.customer_stats = customer_stats
        self.customer_offer_stats = customer_offer_stats
        self.offer_stats = offer_stats

        # stuff for the view creation
        self.schema = schema
        self.view_name = view_name
        self.template_path = template_path

        self.logger = logging.getLogger(self.__class__.__name__)

    def create_view(self, rs: Redshift):

        # if the view does not already exist create it
        if not rs.table_exists(self.view_name, self.schema):

            self.logger.info(f'Creating {self.schema}.{self.view_name} view')

            sql = Utils.generate_query(
                path=self.template_path,
                schema=self.schema,
                view_name=self.view_name,
                customer_stats_table_name=self.customer_stats.table_name,
                offer_stats_table_name=self.offer_stats.table_name,
                customer_offer_stats_table_name=self.customer_offer_stats.table_name,
            )

            rs.query(sql)

    def create_features(
        self,
        rs: Redshift,
        model_date: date,
        offer_cycle: int,
        overwrite: bool = False
    ) -> None:

        self.logger.info(
            f"Create Redemption Features for offer cycle {offer_cycle}")

        # create/update the component tables
        self.customer_stats.create_features(
            rs, model_date, offer_cycle, overwrite)
        self.offer_stats.create_features(
            rs, model_date, offer_cycle, overwrite)
        self.customer_offer_stats.create_features(
            rs, model_date, offer_cycle, overwrite)

        # create the view if it doesn't exist already
        self.create_view(rs)

    def update(self, rs: Redshift, ocd: OfferCycles, overwrite: bool = False):

        # update all of the component parts
        self.customer_stats.update(rs, ocd, overwrite)
        self.offer_stats.update(rs, ocd, overwrite)
        self.customer_offer_stats.update(rs, ocd, overwrite)

    def features_exist(self, rs: Redshift, offer_cycle: int):

        # check if the view exists
        if rs.table_exists(self.view_name, self.schema):

            # check each of the component tables
            return self.customer_stats.features_exist(rs, offer_cycle) and \
                self.offer_stats.features_exist(rs, offer_cycle) and \
                self.customer_offer_stats.features_exist(rs, offer_cycle)

        else:
            self.logger.info(
                f'View {self.schema}.{self.view_name} does not exist')

    @property
    def table_name(self):
        return self.view_name

    @property
    def source_table(self):
        return self.customer_stats.source_table


class ModelFeatures:

    def __init__(
        self,
        redemptions: RedemptionFeatures,
        transactions: Features,
        infer_table_name: str = 'model_infer',
        training_table_name: str = 'model_training',
        infer_template_path: str = 'src_fe_gma_cycle/feature_engineering/templates/model_infer.sql',
        training_template_path: str = 'src_fe_gma_cycle/feature_engineering/templates/model_training.sql',
        offer_details_table_name: str = 'offer_details',
        schema: str = 'annalect'
    ) -> None:

        self.schema = schema
        self.infer_table_name = infer_table_name
        self.training_table_name = training_table_name
        self.infer_template_path = infer_template_path
        self.training_template_path = training_template_path
        self.offer_details_table_name = offer_details_table_name

        # create the redemption features class
        self.redemptions = redemptions

        # create the transaction features class
        self.transactions = transactions

        # create the local logger
        self.logger = logging.getLogger(self.__class__.__name__)

    def generate_infer(
        self,
        rs: Redshift,
        model_date: date,
        offer_cycle: int,
        overwrite=False
    ) -> None:
        """Complete all processes to generate the infer data set:
            - Create the transaction features
            - Create the redemption features
            - Join them together and calculate offer similarity etc

        Args:
            model_date (date): The date of the model
            offer_cycle (int): The offer cycle number
            overwrite (bool, optional): Should we overwrite existing values. Defaults to False.
        """

        _start = datetime.now()

        # convert the model date to a sk
        model_date_sk = Utils.date_to_sk(model_date)

        # make sure the redemption features are there
        if not self.redemptions.features_exist(rs, offer_cycle):
            self.logger.info(
                f"Redemption Features do not exist for offer_cycle {offer_cycle}."
                " Please generate features before creating infer data."
            )

        # make sure the transaction features are there
        elif not self.transactions.features_exist(rs, offer_cycle):
            self.logger.info(
                f"Transaction Features do not exist for offer_cycle {offer_cycle}."
                " Please generate features before creating infer data."
            )
        else:
            # combine them all together
            self.run_infer(model_date_sk, offer_cycle, overwrite)

        _end = datetime.now()
        self.logger.info(
            f'generate_infer processing time:  {str(_end - _start)}')

    def run_infer(
        self,
        rs: Redshift,
        model_date_sk: int,
        offer_cycle: int,
        overwrite: bool = False
    ) -> None:
        """
        Create the infer data set for the model predictions

        The process will check all the precursors:
            - Does the table/values exist

        Then it will populate the SQL template and send the resulting query to redshift

        Parameters
        ----------

        model_date_sk: int
        The date of the run in integer format YYYYMMDD

        offer_cycle: int
        The offer cycle number for the inference

        overwrite: bool (default=False)
        Do you want to overwrite the current table
        """

        self.logger.info(
            f"Generate infer data for offer cycle {offer_cycle}"
            f" on model date {Utils.sk_to_date(model_date_sk)}"
        )

        _start = datetime.now()

        # assume table doesn't exist so create a table
        creation_insertion_statement = f'create table {self.schema}.{self.infer_table_name} as '

        run_query = True

        if rs.table_exists(self.infer_table_name, self.schema):

            # the table does exist so overwrite it
            creation_insertion_statement = f'insert into {self.schema}.{self.infer_table_name} '

            count = rs.get_count(
                self.infer_table_name,
                self.schema,
                offer_cycle=offer_cycle
            )

            # are there values from this query
            if count > 0:

                # if we want to overwrite just drop the table
                if overwrite:
                    rs.delete_from(
                        self.infer_table_name,
                        self.schema,
                        offer_cycle=offer_cycle
                    )

                # otherwise finish the function
                else:
                    self.logger.info(
                        f"Training values for offer cycle {offer_cycle}"
                        " already exist in {self.schema}.{self.infer_table_name}"
                        "Use `overwrite=True` to overwrite the values"
                    )
                    run_query = False

        if run_query:
            # formulate the query
            query = Utils.generate_query(
                path=self.infer_template_path,
                schema=self.schema,
                offer_cycle=offer_cycle,
                creation_insertion_statement=creation_insertion_statement,
                transaction_features_table=self.transactions.table_name,
                redemption_features_table=self.redemptions.table_name,
                model_date_sk=model_date_sk,
                offer_details_table=self.offer_details_table_name
            )

            # run the query
            rs.query(query)

        # logging
        _end = datetime.now()
        self.logger.info(
            f'run_infer processing time:  {str(_end - _start)}')

    def _generate_training_cycle(
        self,
        rs: Redshift,
        ocd: OfferCycles,
        offer_cycle: int,
        overwrite: bool = False
    ) -> None:
        """Generate the data for model training for a single cycle

        Args:
            offer_cycle (int): The offer cycle to create the data for
            overwrite (bool, optional): Do you want to overwrite the existing data. Defaults to False.
        """

        self.logger.info(
            f"Creating training data for offer cycle {offer_cycle}")

        _start = datetime.now()

        # assume creation
        creation_insertion_statement = f'create table {self.schema}.{self.training_table_name} as '

        # boolean as to whether we need to run the query
        run_query = True

        # does the offer cycle table exist
        if not ocd.table_exists(rs):
            raise ValueError(
                f"The offer dates table {ocd.full_table_name} does not exist"
            )

        # do we have the dates for this offer cycle
        elif ocd.get_dates(rs, offer_cycle)[0] < 0:
            raise ValueError(
                f"The offer dates for offer cycle {offer_cycle}"
                f" do not exist in {ocd.full_table_name}"
            )

        # make sure the redemption features are there
        elif not self.redemptions.features_exist(rs, offer_cycle):
            run_query = False
            self.logger.info(
                f"Redemption Features do not exist for offer_cycle {offer_cycle}."
                " Please generate features before creating training data."
            )

        # make sure the transaction features are there
        elif not self.transactions.features_exist(rs, offer_cycle):
            run_query = False
            self.logger.info(
                f"Transaction Features do not exist for offer_cycle {offer_cycle}."
                " Please generate features before creating training data."
            )

        elif rs.table_exists(self.training_table_name, self.schema):

            # if the table exists we can just insert
            creation_insertion_statement = f'insert into {self.schema}.{self.training_table_name} '

            # if we overwrite
            if overwrite:

                self.logger.info(
                    f"Delete entries in {self.schema}.{self.training_table_name}"
                    f" for offer cycle {offer_cycle}"
                )

                # delete the data that overlaps
                rs.delete_from(
                    self.training_table_name,
                    self.schema,
                    offer_cycle=offer_cycle
                )

            else:

                self.logger.info(
                    f"Checking for existing values in {self.schema}.{self.training_table_name}"
                    f" for offer cycle {offer_cycle}"
                )

                count = rs.get_count(
                    self.training_table_name,
                    self.schema,
                    offer_cycle=offer_cycle
                )

                self.logger.info(
                    f"{count} values exist"
                )

                # if overwrite is false and the values exist just finish as we don't need to run anything
                if count > 0:
                    self.logger.info(
                        f"Training values for offer cycle {offer_cycle}"
                        f" already exist in {self.schema}.{self.training_table_name}"
                        " Use `overwrite=True` to overwrite the values"
                    )
                    run_query = False

        if run_query:

            query = Utils.generate_query(
                path=self.training_template_path,
                start_cycle=offer_cycle,
                end_cycle=offer_cycle,
                offer_cycle_dates=ocd.table_name,
                schema=self.schema,
                creation_insertion_statement=creation_insertion_statement,
                transaction_features_table=self.transactions.table_name,
                redemption_features_table=self.redemptions.view_name,
                offer_details_table=self.offer_details_table_name,
                daily_agg_table=self.transactions.source_table,
                redemptions_table=self.redemptions.source_table
            )

            self.logger.info("Running _generate_training_cycle query")

            rs.query(query)

        _end = datetime.now()
        self.logger.info(
            f'generate_training_cycle for offer cycle {offer_cycle} processing time:  {str(_end - _start)}')

    def generate_training(
        self,
        rs: Redshift,
        ocd: OfferCycles,
        start_cycle: int,
        end_cycle: int,
        overwrite=False
    ) -> None:
        """Generate the data for model training

        Args:
            start_cycle (int): The first cycle to generate the training data for
            end_cycle (int): The last cycle to generate the training data for
            overwrite (bool, optional): Overwrite existing values?. Defaults to False.
        """

        self.logger.info(
            f"Generating Training data for offer cycles {start_cycle} to {end_cycle}")

        _start = datetime.now()

        # loop through all offer cycles and calculate the training data
        for offer_cycle in range(start_cycle, end_cycle + 1):
            self._generate_training_cycle(rs, ocd, offer_cycle, overwrite)

        _end = datetime.now()
        self.logger.info(
            f'generate_training processing time:  {str(_end - _start)}')
