{creation_insertion_statement}
WITH token_data AS (
    SELECT distinct tk.acctnum, tk.transaction_id, tk.visa_transaction_date as transaction_date_sk, st1.store_sk, 
    st1.store_type_dsc
    FROM cbi.tokenised_visa_data tk
    INNER JOIN dw_pre.D_STLD_STORE st1 ON tk.store_bk=st1.store_bk
    WHERE tk.visa_transaction_date >= cast(replace(date(CONVERT(datetime, convert(varchar(10), date(DATEADD(mm,-36,GETDATE()))))),'-','') as int) ---todo: change to -12
    AND (gma_customer_id IS NULL OR gma_customer_id = 0)
),
-- adding net_amt to visa table
token_null as (
    SELECT tkn.acctnum, tldo.transaction_id, tldo.transaction_date_sk, tldo.store_sk, tkn.store_type_dsc, tldo.menu_item_sk, tldo.adj_menu_item_sk, tldo.item_level, 
    tldo.tld_units_cnt, tldo.tld_net_amt, tldo.qcr_food_cost_amt, tldo.qcr_paper_cost_amt
    from dw_pre.f_stld_detail tldo
    LEFT JOIN token_data tkn ON tkn.transaction_id = tldo.transaction_id AND tkn.transaction_date_sk = tldo.transaction_date_sk AND tkn.store_sk = tldo.store_sk
    WHERE tldo.transaction_date_sk >= cast(replace(date(CONVERT(datetime, convert(varchar(10), date(DATEADD(mm,-36,GETDATE()))))),'-','') as int) ---todo: change to -12
    AND tldo.mobile_customer_id  in ('', 0)
    and acctnum is not null
),
--getting token monthly agg for mapping table creation
token_month_agg AS (
    SELECT acctnum, gma_customer_id,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date)) < date(DATEADD(mm,-6,GETDATE())) THEN 1 ELSE 0 END) AS month_6_and_before,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-6,GETDATE())) 
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(DATEADD(mm,-5,GETDATE())) THEN 1 ELSE 0 END) AS month_5,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-5,GETDATE())) 
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(DATEADD(mm,-4,GETDATE())) THEN 1 ELSE 0 END) AS month_4,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-4,GETDATE())) 
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(DATEADD(mm,-3,GETDATE())) THEN 1 ELSE 0 END) AS month_3,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-3,GETDATE())) 
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <=date(DATEADD(mm,-2,GETDATE())) THEN 1 ELSE 0 END) AS month_2,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-2,GETDATE()))
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(DATEADD(mm,-1,GETDATE())) THEN 1 ELSE 0 END) AS month_1,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-1,GETDATE()))
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(GETDATE()) THEN 1 ELSE 0 END) AS current_month
    FROM cbi.tokenised_visa_data
    WHERE visa_transaction_date >= cast(replace(date(CONVERT(datetime, convert(varchar(10), date(DATEADD(mm,-36,GETDATE()))))),'-','') as int) ---todo: change to -12
    AND gma_customer_id IS NOT NULL
    AND gma_customer_id!=0
    group by 1, 2
 ),
--aggregation token monthly agg to get weighted sum of transactions for mapping table creation
token_month_agg_wgt AS (
    SELECT acctnum, gma_customer_id,
    SUM(month_6_and_before + (2*month_5) + (2.5*month_4) + (3*month_3) + (3.5*month_2) + (4*month_1) + (4.5*current_month)) AS num_transactions_wgt
    FROM token_month_agg
    GROUP BY 1, 2
    ORDER BY acctnum, gma_customer_id DESC
),
--mapping table
mapping_table_1 AS (
    SELECT sd.acctnum, sd.gma_customer_id
    FROM (
        SELECT *, ROW_NUMBER() OVER(PARTITION BY acctnum ORDER BY num_transactions_wgt DESC) rowNumber
        FROM token_month_agg_wgt
        )sd 
    WHERE sd.rowNumber =1
),
--formating mapping table
mapping_table AS (
    SELECT acctnum, CAST(gma_customer_id AS varchar) AS gma_customer_id
    FROM mapping_table_1
),
-- mapping token null data
token_null_mapped AS (
    SELECT tnu.*, m.gma_customer_id 
    FROM token_null tnu
    LEFT JOIN mapping_table m ON tnu.acctnum = m.acctnum
), 
token_with_nogma as (
    SELECT acctnum, transaction_id, transaction_date_sk, store_type_dsc, store_sk, menu_item_sk, adj_menu_item_sk, item_level, 
    tld_units_cnt, tld_net_amt, qcr_food_cost_amt, qcr_paper_cost_amt 
    FROM token_null_mapped
    WHERE gma_customer_id IS NULL
    OR gma_customer_id = 0
),

item_level as (
select 
    acctnum, transaction_date_sk, transaction_id, store_sk, store_type_dsc, tld_net_amt
    ,case when tld_units_cnt < 0 then 0 else tld_units_cnt end as tld_units_cnt
    ,case when mi.category1_dsc = 'Non Food' OR mi.category1_dsc = 'Non Product' THEN 0 else tld_net_amt end as gp_spend

    /* Food and Paper */
    ,CASE
        WHEN mi.category1_dsc = 'Non Food' OR mi.category1_dsc = 'Non Product' THEN 0
        WHEN t.item_level <> 1 then (t.qcr_food_cost_amt + t.qcr_paper_cost_amt) * case when tld_units_cnt < 0 then 0 else tld_units_cnt end
        WHEN ami.category1_dsc = 'Breakfast' AND mi.category1_dsc = 'Breakfast' AND t.item_level = 1 and ami.evm_ind = 'Y' and ami.size_code in ('S', 'M', 'L')  THEN 0 
        WHEN ami.category3_dsc = mi.category3_dsc AND t.item_level = 1 and ami.evm_ind = 'Y' and ami.size_code in ('S', 'M', 'L') THEN 0
        WHEN ami.category3_dsc = mi.category3_dsc AND t.item_level = 1 and ami.happymeal_ind = 'Y' THEN 0
        WHEN ami.category1_dsc in ('Drinks Hot', 'Drinks Cold') AND ami.category3_dsc = mi.category3_dsc AND t.item_level = 1 THEN 0
        ELSE (t.qcr_food_cost_amt + t.qcr_paper_cost_amt) * case when tld_units_cnt < 0 then 0 else tld_units_cnt end
    END AS FP_Costs

   
from token_with_nogma t

left join dw_pre.d_stld_menu_item mi on t.menu_item_sk = mi.menu_item_sk
left join dw_pre.d_stld_menu_item ami on t.adj_menu_item_sk = ami.menu_item_sk

/* Filter non mobile customers and to specified date */
where t.transaction_date_sk = {date_sk}
),
-- Aggregate up to a transaction level
transaction_level as (
select
    acctnum, transaction_date_sk, transaction_id, store_sk
    ,case store_type_dsc
        when 'FREE STANDING' then 'FREE STANDING'
        when 'INSTORE' then 'INSTORE'
        when 'FOOD COURT' then 'FOOD COURT'
        when 'MALL' then 'FOOD COURT'
        when 'Oil Alliance' then 'SERVO'
        else 'OTHER' /* the remaining stores make up such a small proportion */
        /* EXPRESS, DRIVE THRU ONLY, SPECIAL */
    end as store_type
    ,sum(tld_net_amt) as tld_net_amt
    ,sum(tld_units_cnt) as tld_units_cnt, sum(gp_spend) as gp_spend, sum(FP_Costs) as FP_Costs
from item_level
group by 1,2,3,4,5
)
-- Aggregate up to a customer level
-- So a row will represent the users activity in a given day

select
    acctnum, transaction_date_sk
    ,count(transaction_id) as num_transactions 
    ,sum(tld_net_amt) as tld_net_amt, sum(tld_units_cnt) as tld_units_cnt, sum(gp_spend) as gp_spend, sum(fp_costs) as fp_costs
    ,sum(case store_type when 'FREE STANDING' then 1 else 0 end) as store_free_standing
    ,sum(case store_type when 'INSTORE' then 1 else 0 end) as store_instore
    ,sum(case store_type when 'FOOD COURT' then 1 else 0 end) as store_food_court
    ,sum(case store_type when 'SERVO' then 1 else 0 end) as store_servo
    ,sum(case store_type when 'OTHER' then 1 else 0 end) as store_other
from transaction_level t

group by 1,2