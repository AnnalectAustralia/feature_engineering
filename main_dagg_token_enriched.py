import argparse
from datetime import datetime, date, timedelta
from logging import exception
from src.utils import Utils, Redshift
from src_daily_agg_token_mapped.data_preprocessing import DailyAggregates

import logging
import pathlib
import os


class FeatureEngineering:
    """A master class to orchestrate all the different methods

    Args:
        daily_agg (DailyAggregates): class for daily aggregates
        schema (str, optional): The schema in redshift. Defaults to 'annalect'.
    """

    def __init__(
        self,
        daily_agg: DailyAggregates,
        schema: str = 'annalect'
    ) -> None:

        self.logger = logging.getLogger(__class__.__name__)

        self.schema = schema
        self.daily_agg = daily_agg


    def update_preprocessing(
        self,
        rs: Redshift,
        overwrite: bool = False
    ) -> None:
        try:
            self.logger.info("UPDATE PROCESSING")
            dt1 = datetime.now() - timedelta(days=1)
            query = f"""
            select max(visa_transaction_date) as end_date, 
            cast(replace(date(CONVERT(datetime, convert(varchar(10), date(DATEADD(mm,-3,GETDATE()))))),'-','') as int) as start_date
            from cbi.tokenised_visa_data
            """
            df = rs.query(query, True)
            #end_date_sk = int(df['end_date'][0])
            #start_date_sk = int(df['start_date'][0])
            end_date_sk= 20220803 #20220529 #20210615 #20220529  #TODO: use if manual run
            start_date_sk = 20220801 #20210615 #20200701 #20210615 #TODO: use if manual run
            

            # convert the date sks into dates
            counter = Utils.sk_to_date(start_date_sk)
            end_date = Utils.sk_to_date(end_date_sk)

            # loop through all dates
            while counter <= end_date:
                # convert back into an sk
                date_sk = Utils.date_to_sk(counter)
#                 current_date = end_date_sk+1 #20220326  #TODO: update
                # run daily aggregate
                self.daily_agg.run_daily_aggregate(rs, date_sk, overwrite)

                # increment it by a day
                counter = counter + timedelta(days=1)

        except Exception as e:
            self.logger.error("Error updating preprocessing")
            self.logger.exception(e)


def build_feature_engineering(config):


    daily_agg = DailyAggregates(
        schema=config['schema'],
        **config['daily_aggregates']
    )


    return FeatureEngineering(
        daily_agg,
        config.get('schema', 'annalect')
    )


def build_parser():

    parser = argparse.ArgumentParser(
        prog='Feature Engineering',
        description="Run various processes to create the features for the Offer Allocation Engine"
    )
    subparsers = parser.add_subparsers(dest='command')

    parser.add_argument(
        '-c', '--config', type=str,
        help='The path to the config file', required=True
    )

    parser.add_argument(
        '-r', '--redshift', type=str,
        help="The path to the redshift credentials", required=True
    )

    # all methods have an overwrite argument so add it to the top line
    parser.add_argument(
        '-o', '--overwrite',
        dest='overwrite', action='store_true',
        help="Would you like to overwrite the existing values in redshift?"
    )
    parser.set_defaults(overwrite=False)

    prep = subparsers.add_parser('preprocessing')
    prep.add_argument(
        '-oc', '--offer-cycle',
        dest='offer_cycle', type=int, default=-1,
        help="The offer cycle in question. If not provided it will run all preprocessing since the last update."
    )

    return parser


if __name__ == "__main__":

    # build the logger
    logging.basicConfig(
        filename=os.path.join(
            pathlib.Path(__file__).parent.absolute(),
            'log_dagg_token_enriched.log'
        ),
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.DEBUG
    )

    # set up the parser
    parser = build_parser()

    # parse the args
    args = parser.parse_args()

    logging.info(f"ARGS: {args}")

    # read in the yaml
    config = Utils.process_yaml(args.config)
    credentials = Utils.process_yaml(args.redshift)

    # set up the redshift connection using the details in the config
    rs = Redshift(
        **credentials
    )

    # Feature engineering
    fe = build_feature_engineering(config)

    if args.command == 'preprocessing':
        fe.update_preprocessing(rs, args.overwrite)

    logging.info('Complete')
