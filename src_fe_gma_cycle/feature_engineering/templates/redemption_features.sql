
{creation_insertion_statement}
with offer_cycles as (
-- Find the offer cycles in the past
    select
        offer_cycle
        ,cycle_start_date_sk
        ,cycle_end_date_sk
        ,to_date('{model_date_sk}', 'YYYYMMDD') - to_date(cycle_start_date_sk, 'YYYYMMDD') as days_since_offer_cycle_start
        ,to_date('{model_date_sk}', 'YYYYMMDD') - to_date(cycle_end_date_sk, 'YYYYMMDD') as days_since_offer_cycle_end
    from {schema}.offer_cycle_dates
    where cycle_end_date_sk < {model_date_sk} -- All completed offer cycles (do we want to include ongoing ones?)
)

-- What is the customers historic interactions with offers look like
,customer_stats as (
    select
        mobile_customer_id
        ,customer_sent
        ,customer_redeemed
        ,customer_redemption_rate

        ,customer_last_redemption_sk
        ,to_date(customer_last_redemption_sk, 'YYYYMMDD') as customer_last_redemption_bk
        ,to_date('{model_date_sk}', 'YYYYMMDD') - to_date(customer_last_redemption_sk, 'YYYYMMDD') as customer_days_since_redemption

        ,last_week_offers_sent
        ,last_week_offers_redeemed
        ,coalesce(last_week_offers_redeemed * 1.0 / nullif(last_week_offers_sent, 0), 0) as last_week_redemption_rate
        
        ,last_month_offers_sent
        ,last_month_offers_redeemed
        ,coalesce(last_month_offers_redeemed * 1.0 / nullif(last_month_offers_sent, 0), 0) as last_month_redemption_rate
        
        ,last_3_months_offers_sent
        ,last_3_months_offers_redeemed
        ,coalesce(last_3_months_offers_redeemed * 1.0 / nullif(last_3_months_offers_sent, 0), 0) as last_3_months_redemption_rate
    from (
        select
            mobile_customer_id
            ,count(1) as customer_sent
            ,coalesce(sum(redeemed), 0) as customer_redeemed
            ,coalesce(sum(redeemed), 0) * 1.0 / count(1) as customer_redemption_rate

            ,max(transaction_date_sk) as customer_last_redemption_sk

            /* last week redemption details */
            ,sum(case when days_since_offer_cycle_start <= 7 then 1 else 0 end) as last_week_offers_sent
            ,sum(case when days_since_offer_cycle_start <= 7 then redeemed else 0 end) as last_week_offers_redeemed

            /* last month redemption details */
            ,sum(case when days_since_offer_cycle_start <= 30 then 1 else 0 end) as last_month_offers_sent
            ,sum(case when days_since_offer_cycle_start <= 30 then redeemed else 0 end) as last_month_offers_redeemed

            /* last three months redemption details */
            ,sum(case when days_since_offer_cycle_start <= 90 then 1 else 0 end) as last_3_months_offers_sent
            ,sum(case when days_since_offer_cycle_start <= 90 then redeemed else 0 end) as last_3_months_offers_redeemed
        from {schema}.{source_table} r

        inner join offer_cycles o
        on r.offer_cycle = o.offer_cycle
        and mobile_customer_id not in ('0') -- defunct mobile customer id

        group by 1
        ) r
)
    
-- Offer specific stats
-- What is the redemption rate for a given offer across the whole population
,offer_stats as (
    select
        offer_id
        ,count(1) as offer_sent
        ,coalesce(sum(redeemed), 0) as offer_redeemed
        ,coalesce(sum(redeemed), 0) * 1.0 / count(1) as offer_redemption_rate
    from {schema}.{source_table} r

    inner join offer_cycles o
    on r.offer_cycle = o.offer_cycle
    and mobile_customer_id not in ('0') -- defunct mobile customer id
    
    group by 1
)

-- What is the reltionship of an offer with a given customer
-- Has this customer even redeemed this offer before?
,customer_offer_stats as (
    select
        mobile_customer_id
        ,offer_id
        ,count(1) as cust_off_sent
        ,coalesce(sum(redeemed), 0) as cust_off_redeemed
        ,coalesce(sum(redeemed), 0) * 1.0 / count(1) as cust_off_redemption_rate

        ,max(transaction_date_sk) as cust_off_last_redemption_sk
        ,to_date(max(transaction_date_sk), 'YYYYMMDD') as cust_off_last_redemption_bk

        ,to_date('{model_date_sk}', 'YYYYMMDD') - to_date(max(transaction_date_sk), 'YYYYMMDD') as cust_off_days_since_redemption
    from {schema}.{source_table} r

    inner join offer_cycles o
    on r.offer_cycle = o.offer_cycle
    and mobile_customer_id not in ('0') -- defunct mobile customer id
    
    group by 1,2
)

select
    {offer_cycle} as offer_cycle
    ,getdate() as redemption_audit_time
    ,c.mobile_customer_id
    ,o.offer_id
    
    /* Customer Stats */
    ,customer_sent
    ,customer_redeemed
    ,customer_redemption_rate
    ,customer_last_redemption_sk
    ,customer_last_redemption_bk
    ,customer_days_since_redemption
    ,last_week_offers_sent
    ,last_week_offers_redeemed
    ,last_week_redemption_rate
    ,last_month_offers_sent
    ,last_month_offers_redeemed
    ,last_month_redemption_rate
    ,last_3_months_offers_sent
    ,last_3_months_offers_redeemed
    ,last_3_months_redemption_rate

    /* Offer Stats */
    ,offer_sent
    ,offer_redeemed
    ,offer_redemption_rate

    /* Customer Offer Stats */
    ,cust_off_sent
    ,cust_off_redeemed
    ,cust_off_redemption_rate
    ,cust_off_last_redemption_sk
    ,cust_off_last_redemption_bk
    ,cust_off_days_since_redemption

from customer_stats c

cross join offer_stats o

left join customer_offer_stats co
on co.mobile_customer_id = c.mobile_customer_id
and co.offer_id = o.offer_id