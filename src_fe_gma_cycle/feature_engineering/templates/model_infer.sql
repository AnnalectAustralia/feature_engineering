{creation_insertion_statement}
with offer_details as (
select
	*
	 /* How many food categories does the offer cover */
    ,offer_coffee + offer_burger + offer_chicken + offer_dessert + offer_snack + offer_breakfast + offer_mcveggie + offer_beef + offer_fish + 
    offer_fries + offer_salad + offer_vm + offer_happy_meal + offer_family + offer_big_mac + offer_cheeseburger + offer_qtr_p + offer_nuggets + 
    offer_hashbrown + offer_mcmuffin + offer_sundae + offer_shake as offer_food_cats
from {schema}.{offer_details_table}

where offer_id between 8000 and 8999
)

/* Separating out the specified offer cycles for memory issues */
,transaction_features as (
    select 
        * 
    from {schema}.{transaction_features_table} 
    where offer_cycle = {offer_cycle}
)

,redemption_features as (
    select 
        * 
    from {schema}.{redemption_features_table} 
    where offer_cycle = {offer_cycle}
)

select
    to_date('{model_date_sk}', 'YYYYMMDD') as model_date
    ,getdate() as audit_infer_datetime
    ,o.offer_id
    ,offer_name
    ,t.*

    /* Customer Stats */
    ,customer_sent
    ,customer_redeemed
    ,customer_redemption_rate
    ,customer_last_redemption_sk
    ,customer_last_redemption_bk
    ,customer_days_since_redemption
    ,last_week_offers_sent
    ,last_week_offers_redeemed
    ,last_week_redemption_rate
    ,last_month_offers_sent
    ,last_month_offers_redeemed
    ,last_month_redemption_rate
    ,last_3_months_offers_sent
    ,last_3_months_offers_redeemed
    ,last_3_months_redemption_rate

    /* Offer Stats */
    ,offer_sent
    ,offer_redeemed
    ,offer_redemption_rate

    /* Customer Offer Stats */
    ,cust_off_sent
    ,cust_off_redeemed
    ,cust_off_redemption_rate
    ,cust_off_last_redemption_sk
    ,cust_off_last_redemption_bk
    ,cust_off_days_since_redemption

    /* Offer details features */
    ,price
    ,og_price
    ,discount
    ,discount_perc
    ,offer_coffee
    ,offer_burger
    ,offer_chicken
    ,offer_dessert
    ,offer_snack
    ,offer_breakfast
    ,offer_mcveggie
    ,offer_beef
    ,offer_fish
    ,offer_fries
    ,offer_salad
    ,offer_vm
    ,offer_happy_meal
    ,offer_family
    ,offer_big_mac
    ,offer_cheeseburger
    ,offer_qtr_p
    ,offer_nuggets
    ,offer_hashbrown
    ,offer_mcmuffin
    ,offer_sundae
    ,offer_shake

    /* How similar is the offer to the previous purchasing activity of the customer */
    ,(coffee_inc_prop * offer_coffee) + (burger_inc_prop * offer_burger) + (chicken_inc_prop * offer_chicken) + (snack_inc_prop * offer_snack) + 
    (dessert_inc_prop * offer_dessert) + (breakfast_inc_prop * offer_breakfast) + (beef_inc_prop * offer_beef) + (fish_inc_prop * offer_fish) + 
    (fries_inc_prop * offer_fries) + (mcveggie_inc_prop * offer_mcveggie) + (salad_inc_prop * offer_salad) + (happy_meal_inc_prop * offer_happy_meal) + 
    (family_inc_prop * offer_family) + (vm_inc_prop * offer_vm) + (shake_inc_prop * offer_shake) + (sundae_inc_prop * offer_sundae) + 
    (big_mac_inc_prop * offer_big_mac) + (qtr_p_inc_prop * offer_qtr_p) + (cheeseburger_inc_prop * offer_cheeseburger) + 
    (nuggets_inc_prop * offer_nuggets) + (hashbrown_inc_prop * offer_hashbrown) + (mcmuffin_inc_prop * offer_mcmuffin) as offer_similarity
	
    /* Same as above but looks at specifically discount transactions */
    ,(coffee_discount_prop * offer_coffee) + (burger_discount_prop * offer_burger) + (chicken_discount_prop * offer_chicken) + 
    (snack_discount_prop * offer_snack) + (dessert_discount_prop * offer_dessert) + (breakfast_discount_prop * offer_breakfast) + 
    (beef_discount_prop * offer_beef) + (fish_discount_prop * offer_fish) + (fries_discount_prop * offer_fries) + 
    (mcveggie_discount_prop * offer_mcveggie) + (salad_discount_prop * offer_salad) + (happy_meal_discount_prop * offer_happy_meal) + 
    (family_discount_prop * offer_family) + (vm_discount_prop * offer_vm) + (shake_discount_prop * offer_shake) + (sundae_discount_prop * offer_sundae) + 
    (big_mac_discount_prop * offer_big_mac) + (qtr_p_discount_prop * offer_qtr_p) + (cheeseburger_discount_prop * offer_cheeseburger) + 
    (nuggets_discount_prop * offer_nuggets) + (hashbrown_discount_prop * offer_hashbrown) + (mcmuffin_discount_prop * offer_mcmuffin) as offer_discount_similarity
    
    /* How many food categories does the offer cover */
    ,offer_food_cats
	
    /* Normalising the offer similarity depending on how many categories are covered by the offer */
    ,(
    (coffee_inc_prop * offer_coffee) + (burger_inc_prop * offer_burger) + (chicken_inc_prop * offer_chicken) + (snack_inc_prop * offer_snack) + 
    (dessert_inc_prop * offer_dessert) + (breakfast_inc_prop * offer_breakfast) + (beef_inc_prop * offer_beef) + (fish_inc_prop * offer_fish) + 
    (fries_inc_prop * offer_fries) + (mcveggie_inc_prop * offer_mcveggie) + (salad_inc_prop * offer_salad) + (happy_meal_inc_prop * offer_happy_meal) + 
    (family_inc_prop * offer_family) + (vm_inc_prop * offer_vm) + (shake_inc_prop * offer_shake) + (sundae_inc_prop * offer_sundae) + 
    (big_mac_inc_prop * offer_big_mac) + (qtr_p_inc_prop * offer_qtr_p) + (cheeseburger_inc_prop * offer_cheeseburger) + 
    (nuggets_inc_prop * offer_nuggets) + (hashbrown_inc_prop * offer_hashbrown) + (mcmuffin_inc_prop * offer_mcmuffin)
    ) * 1.0 / offer_food_cats as offer_similarity_normalised
    
    /* Normalising the offer discount similarity depending on how many categories are covered by the offer */
    ,(
    (coffee_discount_prop * offer_coffee) + (burger_discount_prop * offer_burger) + (chicken_discount_prop * offer_chicken) + 
    (snack_discount_prop * offer_snack) + (dessert_discount_prop * offer_dessert) + (breakfast_discount_prop * offer_breakfast) + 
    (beef_discount_prop * offer_beef) + (fish_discount_prop * offer_fish) + (fries_discount_prop * offer_fries) + 
    (mcveggie_discount_prop * offer_mcveggie) + (salad_discount_prop * offer_salad) + (happy_meal_discount_prop * offer_happy_meal) + 
    (family_discount_prop * offer_family) + (vm_discount_prop * offer_vm) + (shake_discount_prop * offer_shake) + (sundae_discount_prop * offer_sundae) + 
    (big_mac_discount_prop * offer_big_mac) + (qtr_p_discount_prop * offer_qtr_p) + (cheeseburger_discount_prop * offer_cheeseburger) + 
    (nuggets_discount_prop * offer_nuggets) + (hashbrown_discount_prop * offer_hashbrown) + (mcmuffin_discount_prop * offer_mcmuffin)
    ) * 1.0 / offer_food_cats as offer_discount_similarity_normalised
	
from transaction_features t

-- match every user to every offer
cross join offer_details o

-- Find the redemptions stats for that offer and that user
left join redemption_features rf
on t.mobile_customer_id = r.mobile_customer_id
and o.offer_id = rf.offer_id
;