{creation_insertion_statement}
-- token data
WITH token_data AS (
    SELECT tk.acctnum, CAST(tk.gma_customer_id AS varchar) AS gma_customer_id, tk.transaction_id, CAST(tk.visa_transaction_date AS float) AS transaction_date_sk, st1.store_sk
    FROM cbi.tokenised_visa_data tk
    INNER JOIN dw_pre.D_STLD_STORE st1 ON tk.store_bk=st1.store_bk
),
-- adding net_amt to visa table
token_with_tld as (
    SELECT tkn.*, tldo.net_amt
    from token_data tkn
    INNER JOIN dw_pre.f_stld_order tldo ON 
    tkn.transaction_id = tldo.transaction_id
    AND tkn.transaction_date_sk = tldo.transaction_date_sk
    AND tkn.store_sk = tldo.store_sk
),
-- token null gma data
token_null AS (
    SELECT distinct acctnum, transaction_id, transaction_date_sk, net_amt
    FROM token_with_tld
    WHERE gma_customer_id IS NULL
    OR gma_customer_id = 0
),
-- token not null gma data
token_notnull AS (
    SELECT distinct acctnum, gma_customer_id, transaction_id, transaction_date_sk, net_amt
    FROM token_with_tld
    WHERE gma_customer_id IS NOT NULL
    AND gma_customer_id != 0
),

--getting token monthly agg for mapping table creation
token_month_agg AS (
    SELECT acctnum, gma_customer_id,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date)) < date(DATEADD(mm,-6,GETDATE())) THEN 1 ELSE 0 END) AS month_6_and_before,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-6,GETDATE())) 
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(DATEADD(mm,-5,GETDATE())) THEN 1 ELSE 0 END) AS month_5,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-5,GETDATE())) 
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(DATEADD(mm,-4,GETDATE())) THEN 1 ELSE 0 END) AS month_4,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-4,GETDATE())) 
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(DATEADD(mm,-3,GETDATE())) THEN 1 ELSE 0 END) AS month_3,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-3,GETDATE())) 
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <=date(DATEADD(mm,-2,GETDATE())) THEN 1 ELSE 0 END) AS month_2,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-2,GETDATE()))
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(DATEADD(mm,-1,GETDATE())) THEN 1 ELSE 0 END) AS month_1,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-1,GETDATE()))
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(GETDATE()) THEN 1 ELSE 0 END) AS current_month
    FROM cbi.tokenised_visa_data
    WHERE gma_customer_id IS NOT NULL
    AND gma_customer_id!=0
    group by 1, 2
 ),
--aggregation token monthly agg to get weighted sum of transactions for mapping table creation
token_month_agg_wgt AS (
    SELECT acctnum, gma_customer_id,
    SUM(month_6_and_before + (2*month_5) + (2.5*month_4) + (3*month_3) + (3.5*month_2) + (4*month_1) + (4.5*current_month)) AS num_transactions_wgt
    FROM token_month_agg
    GROUP BY 1, 2
    ORDER BY acctnum, gma_customer_id DESC
),
--mapping table
mapping_table_1 AS (
    SELECT sd.acctnum, sd.gma_customer_id
    FROM (
        SELECT *, ROW_NUMBER() OVER(PARTITION BY acctnum ORDER BY num_transactions_wgt DESC) rowNumber
        FROM token_month_agg_wgt
        )sd 
    WHERE sd.rowNumber =1
),
--formating mapping table
mapping_table AS (
    SELECT acctnum, CAST(gma_customer_id AS varchar) AS gma_customer_id
    FROM mapping_table_1
),
-- mapping token null data
token_null_mapped AS (
    SELECT tnu.*, m.gma_customer_id 
    FROM token_null tnu
    LEFT JOIN mapping_table m ON tnu.acctnum = m.acctnum
),
-- token data union
token_union1 AS (
    SELECT acctnum, gma_customer_id, transaction_id, transaction_date_sk, net_amt 
    FROM token_null_mapped 
    UNION 
    SELECT acctnum, gma_customer_id, transaction_id, transaction_date_sk, net_amt
    FROM token_notnull
), 
--dividing acctnum to null and not null gma
token_with_gma as (
    SELECT acctnum, transaction_date_sk, 'True' as gma_flag, gma_customer_id, count(transaction_id) as num_transactions, sum(net_amt) as net_amt 
    FROM token_union1
    WHERE gma_customer_id IS NOT NULL
    AND gma_customer_id != 0
    group by 1, 2, 3, 4
), token_with_nogma as (
    SELECT acctnum, transaction_date_sk, 'False' as gma_flag, gma_customer_id, count(transaction_id) as num_transactions, sum(net_amt) as net_amt
    FROM token_union1
    WHERE gma_customer_id IS NULL
    OR gma_customer_id = 0
    group by 1, 2, 3, 4
), 
-- final token data union
token_union AS (
    SELECT acctnum, transaction_date_sk, gma_customer_id, num_transactions, net_amt, gma_flag 
    FROM token_with_gma 
    UNION 
    SELECT acctnum, transaction_date_sk, gma_customer_id, num_transactions, net_amt, gma_flag
    FROM token_with_nogma
)
select * from token_union
where transaction_date_sk = {date_sk}
