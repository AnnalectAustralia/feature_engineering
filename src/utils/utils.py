from datetime import date, datetime
from abc import ABC
import yaml
import os


class Utils(ABC):

    @staticmethod
    def date_to_sk(date: date):
        return date.year * 10000 + date.month * 100 + date.day

    @staticmethod
    def sk_to_date(date_sk: int):
        return datetime(int(date_sk / 10000), int((date_sk % 10000) / 100), date_sk % 100).date()

    @staticmethod
    def generate_query(
        path: str,
        **kwargs
    ):
        # template for the daily aggregates sql code
        with open(path, 'r') as f:
            template = f.read()

        # populate the query with the respective values
        query = template.format(
            **kwargs
        )

        return query

    @staticmethod
    def process_yaml(path):

        if os.path.exists(path):
            with open(path, 'r') as f:

                # load in the yaml file
                output = yaml.safe_load(f)

                return output

        else:
            raise OSError(f"File {path} does not exist")
