create view {schema}.{view_name} as
select
    c.offer_cycle
    ,c.mobile_customer_id
    ,o.offer_id
    
    /* Customer Stats */
    ,customer_sent
    ,customer_redeemed
    ,customer_redemption_rate
    ,customer_last_redemption_sk
    ,customer_last_redemption_bk
    ,customer_days_since_redemption
    ,last_week_offers_sent
    ,last_week_offers_redeemed
    ,last_week_redemption_rate
    ,last_month_offers_sent
    ,last_month_offers_redeemed
    ,last_month_redemption_rate
    ,last_3_months_offers_sent
    ,last_3_months_offers_redeemed
    ,last_3_months_redemption_rate

    /* Offer Stats */
    ,offer_sent
    ,offer_redeemed
    ,offer_redemption_rate

    /* Customer Offer Stats */
    ,cust_off_sent
    ,cust_off_redeemed
    ,cust_off_redemption_rate
    ,cust_off_last_redemption_sk
    ,cust_off_last_redemption_bk
    ,cust_off_days_since_redemption

from {schema}.{customer_stats_table_name} c

inner join {schema}.{offer_stats_table_name} o
on c.offer_cycle = o.offer_cycle

left join {schema}.{customer_offer_stats_table_name} co
on co.offer_cycle = c.offer_cycle
and co.mobile_customer_id = c.mobile_customer_id
and co.offer_id = o.offer_id