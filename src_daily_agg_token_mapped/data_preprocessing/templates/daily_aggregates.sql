{creation_insertion_statement}
WITH tld_detail_notnull AS (
    SELECT *
    FROM dw_pre.f_stld_detail
    WHERE transaction_date_sk >= cast(replace(date(CONVERT(datetime, convert(varchar(10), date(DATEADD(mm,-12,GETDATE()))))),'-','') as int)  
    AND mobile_customer_id NOT IN ('', '0')
),
-- tld detail_null data
tld_detail_null AS (
    SELECT distinct business_date_sk, transaction_date_sk, transaction_time_sk, store_sk, adj_menu_item_sk, menu_item_sk, point_of_payment_type_sk, point_of_delivery_type_sk, 
    transaction_type_sk, transaction_kind_sk, promotion_sk, pos_id, transaction_id, item_level, item_sequence, promotion_flag, offer_flag, offer_id,
    tld_units_cnt, tld_promo_units_cnt, tld_gross_amt, tld_tax_amt, tld_net_amt, tld_unit_price_amt, tld_unit_tax_amt, tld_bd_price_amt, tld_bd_tax_amt, tld_bp_price_amt,
    tld_bp_tax_amt, tld_tax_rate_pct, tld_base_amt, tld_allocated_unit_price_amt, tld_allocated_base_amt, qcr_food_cost_amt, qcr_paper_cost_amt, adt_created, 
    adt_modified, table_service_flag, event_type, po_payment_pos_id, voided_quantity, sale_start_date, sale_start_time, sale_end_date, sale_end_time, campaign_sk
    FROM dw_pre.f_stld_detail
    WHERE transaction_date_sk >= cast(replace(date(CONVERT(datetime, convert(varchar(10), date(DATEADD(mm,-12,GETDATE()))))),'-','') as int)  
    AND mobile_customer_id IN ('', '0')
),
-- token null data
token_null_1 AS (
    SELECT distinct acctnum, transaction_id, CAST(visa_transaction_date AS float) AS transaction_date_sk, store_bk
    FROM cbi.tokenised_visa_data
    WHERE visa_transaction_date >= cast(replace(date(CONVERT(datetime, convert(varchar(10), date(DATEADD(mm,-12,GETDATE()))))),'-','') as int)  
    AND (gma_customer_id IS NULL OR gma_customer_id=0)
),
-- adding store_sk to use as mapping key
token_null AS (
    SELECT tn1.*, st1.store_sk
    FROM token_null_1 tn1
    INNER JOIN dw_pre.D_STLD_STORE st1 ON tn1.store_bk=st1.store_bk
),
--getting token monthly agg for mapping table creation
token_month_agg AS (
    SELECT acctnum, gma_customer_id,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date)) < date(DATEADD(mm,-6,GETDATE())) THEN 1 ELSE 0 END) AS month_6_and_before,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-6,GETDATE())) 
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(DATEADD(mm,-5,GETDATE())) THEN 1 ELSE 0 END) AS month_5,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-5,GETDATE())) 
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(DATEADD(mm,-4,GETDATE())) THEN 1 ELSE 0 END) AS month_4,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-4,GETDATE())) 
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(DATEADD(mm,-3,GETDATE())) THEN 1 ELSE 0 END) AS month_3,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-3,GETDATE())) 
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <=date(DATEADD(mm,-2,GETDATE())) THEN 1 ELSE 0 END) AS month_2,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-2,GETDATE()))
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(DATEADD(mm,-1,GETDATE())) THEN 1 ELSE 0 END) AS month_1,
        SUM(CASE WHEN CONVERT(datetime, convert(varchar(8), visa_transaction_date))> date(DATEADD(mm,-1,GETDATE()))
            AND CONVERT(datetime, convert(varchar(8), visa_transaction_date)) <= date(GETDATE()) THEN 1 ELSE 0 END) AS current_month
    FROM cbi.tokenised_visa_data
    WHERE visa_transaction_date >= cast(replace(date(CONVERT(datetime, convert(varchar(10), date(DATEADD(mm,-12,GETDATE()))))),'-','') as int) ---todo: change to -12
    AND gma_customer_id IS NOT NULL
    AND gma_customer_id!=0
    group by 1, 2
 ),
--aggregation token monthly agg to get weighted sum of transactions for mapping table creation
token_month_agg_wgt AS (
    SELECT acctnum, gma_customer_id,
    SUM(month_6_and_before + (2*month_5) + (2.5*month_4) + (3*month_3) + (3.5*month_2) + (4*month_1) + (4.5*current_month)) AS num_transactions_wgt
    FROM token_month_agg
    GROUP BY 1, 2
    ORDER BY acctnum, gma_customer_id DESC
),
--mapping table
mapping_table_1 AS (
    SELECT sd.acctnum, sd.gma_customer_id
    FROM (
        SELECT *, ROW_NUMBER() OVER(PARTITION BY acctnum ORDER BY num_transactions_wgt DESC) rowNumber
        FROM token_month_agg_wgt
        )sd 
    WHERE sd.rowNumber =1
),
--formating mapping table
mapping_table AS (
    SELECT acctnum, CAST(gma_customer_id AS varchar) AS mobile_customer_id
    FROM mapping_table_1
),
-- mapping token null data
token_null_mapped AS (
    SELECT tnu.*, m.mobile_customer_id
    FROM token_null tnu
    INNER JOIN mapping_table m ON tnu.acctnum = m.acctnum
),
-- only existing and mapped gma in token data
token_mapped AS (
    SELECT *, 'True' AS mapped_flag 
    FROM token_null_mapped
    WHERE mobile_customer_id IS NOT NULL
),
-- mapping tld null data
tld_detail_null_mapped AS (
    SELECT tldnu.*, tom.mobile_customer_id, tom.mapped_flag 
    FROM tld_detail_null tldnu
    INNER JOIN token_mapped tom ON  
    tldnu.transaction_id = tom.transaction_id
    AND tldnu.transaction_date_sk = tom.transaction_date_sk
    AND tldnu.store_sk = tom.store_sk
),
-- tld mapped data
tld_detail_mapped AS (
SELECT mobile_customer_id, business_date_sk, transaction_date_sk, transaction_time_sk, store_sk, adj_menu_item_sk, menu_item_sk, 
    point_of_payment_type_sk, point_of_delivery_type_sk, 
    transaction_type_sk, transaction_kind_sk, promotion_sk, pos_id, transaction_id, item_level, item_sequence, promotion_flag, offer_flag, offer_id,
    tld_units_cnt, tld_promo_units_cnt, tld_gross_amt, tld_tax_amt, tld_net_amt, tld_unit_price_amt, tld_unit_tax_amt, tld_bd_price_amt, tld_bd_tax_amt, tld_bp_price_amt,
    tld_bp_tax_amt, tld_tax_rate_pct, tld_base_amt, tld_allocated_unit_price_amt, tld_allocated_base_amt, qcr_food_cost_amt, qcr_paper_cost_amt, adt_created, 
    adt_modified, table_service_flag, event_type, po_payment_pos_id, voided_quantity, sale_start_date, sale_start_time, sale_end_date, sale_end_time, campaign_sk,
    'False' AS mapped_flag
FROM tld_detail_notnull
UNION
SELECT mobile_customer_id, business_date_sk, transaction_date_sk, transaction_time_sk, store_sk, adj_menu_item_sk, menu_item_sk, 
    point_of_payment_type_sk, point_of_delivery_type_sk, transaction_type_sk, transaction_kind_sk, promotion_sk, pos_id, transaction_id, item_level, item_sequence, 
    promotion_flag, offer_flag, offer_id, tld_units_cnt, tld_promo_units_cnt, tld_gross_amt, tld_tax_amt, tld_net_amt, tld_unit_price_amt, tld_unit_tax_amt, 
    tld_bd_price_amt, tld_bd_tax_amt, tld_bp_price_amt, tld_bp_tax_amt, tld_tax_rate_pct, tld_base_amt, tld_allocated_unit_price_amt, tld_allocated_base_amt, 
    qcr_food_cost_amt, qcr_paper_cost_amt, adt_created, adt_modified, table_service_flag, event_type, po_payment_pos_id, voided_quantity, sale_start_date, 
    sale_start_time, sale_end_date, sale_end_time, campaign_sk, mapped_flag
FROM tld_detail_null_mapped), 

menu_items as (
-- Classify all menu items into various types
select
    mi.*
    /* Food Type */
    ,case
        when mi.category1_dsc = 'Drinks Hot' or mi.category2_dsc like '%Coffee%' or (category2_dsc = 'Cold Drinks Other' and category3_dsc in ('Iced Chocolate', 'Iced Tea'))
        then 1 else 0 
    end as coffee
    ,case when mi.category1_dsc = 'Burgers' then 1 else 0 end as burger
    ,case when upper(mi.menu_item_dsc) like '%CHICKEN%' then 1 else 0 end as chicken
    ,case when upper(mi.menu_item_dsc) like '%FISH%' then 1 else 0 end as fish
    ,case when upper(mi.menu_item_dsc) like '%MCVEGGIE%' then 1 else 0 end as mcveggie
    ,case when upper(mi.menu_item_dsc) like '%SALAD%' then 1 else 0 end as salad
    ,case when upper(mi.menu_item_dsc) like '%FRIES%' then 1 else 0 end as fries
    ,case
        when upper(mi.menu_item_dsc) like '%BEEF%' or upper(mi.menu_item_dsc) like '%QUARTER%'
        or upper(mi.menu_item_dsc) like '%BIG MAC%' or upper(mi.menu_item_dsc) like '%CHEESEBURGER%'
        then 1 else 0
    end as beef
    ,case when mi.category4_dsc = 'Snack' then 1 else 0 end as snack
    ,case when mi.category1_dsc = 'Desserts' then 1 else 0 end as dessert
    ,case when mi.category1_dsc = 'Breakfast' then 1 else 0 end as breakfast
    ,case when mi.happymeal_ind = 'Y' then 1 else 0 end as happy_meal
    ,case when mi.evm_ind = 'Y' then 1 else 0 end as vm
    ,case when upper(mi.menu_item_dsc) like '%FAMILY%' then 1 else 0 end as family
    ,case when category3_dsc = 'Shakes' then 1 else 0 end as shake
    ,case when category3_dsc = 'Sundae' then 1 else 0 end as sundae
    ,case when category3_dsc = 'Big Mac' then 1 else 0 end as big_mac
    ,case when category3_dsc = 'QTR P' then 1 else 0 end as qtr_p
    ,case when category3_dsc = 'Cheeseburger' then 1 else 0 end as cheeseburger
    ,case when category2_dsc = 'Hashbrown' then 1 else 0 end as hashbrown
    ,case when category2_dsc = 'Nuggets' then 1 else 0 end as nuggets
    ,case when category2_dsc = 'BF McMuffin' then 1 else 0 end as mcmuffin

from dw_pre.d_stld_menu_item mi
),

-- Get all campaigns and find the offer id if it exists
campaigns as (
select *
    -- Crew offers need to be excluded from gp calcs
    ,case when upper(campaign_title) like '%CREW%OFFER%' then 1 else 0 end as crew_offer

    -- visa promotion is excluded from gp calcs
    ,case when offer_id = 2000 then 1 when upper(campaign_title) like '%VISA%' then 1 else 0 end as visa

    -- st george promotion is excluded from gp calcs
    ,case when offer_id = 2500 then 1 else 0 end as st_george
from (
    select c.campaign_sk, c.campaign_bk, c.campaign_title
      ,case 
            when substring(c.campaign_title, 1, 4) like '8%' 
                --or substring(c.campaign_title, 1, 4) like '9%' or substring(c.campaign_title, 1, 4) like '1%' --to seperate mop and mccafe
                then cast(substring(c.campaign_title, 1, 4) as int)
            when substring(c.campaign_title, 5, 4) like '8%' 
                --or substring(c.campaign_title, 5, 4) like '9%' or substring(c.campaign_title, 5, 4) like '1%' --to seperate mop and mccafe
                then cast(substring(c.campaign_title, 5, 4) as int)
            when (substring(c.campaign_title, 9, 4) in ('2500', '5000', '7500') or lower(c.campaign_title) like '%%birthday campaign%%') then 1000
            else null
        end as offer_id
        ,case when coalesce(vo.campaign_bk, 'null') = 'null' then 0 else 1 end as voted_offer
        ,case when (substring(c.campaign_title, 9, 4) in ('2500', '5000', '7500') or lower(c.campaign_title) like '%%birthday campaign%%') then 1 else 0 end as loyalty_prize  
        ,case when substring(c.campaign_title, 9, 4)='2500' then 1 else 0 end as loyalty_prize_tier_1
        ,case when substring(c.campaign_title, 9, 4)='5000' then 1 else 0 end as loyalty_prize_tier_2
        ,case when substring(c.campaign_title, 9, 4)='7500' then 1 else 0 end as loyalty_prize_tier_3
        ,case when lower(campaign_title) like '%%punch%%' then 1 else 0 end as punchcard
    from dw_pre.d_campaign c

    -- There are these voted offers that we don't care about and want to exclude from GP% calcs
    left join cbi.gma_voted_offers vo on vo.campaign_bk = c.campaign_bk
    )
where campaign_sk != 9933 -- stupid chicken mcpieces offer
),

-- Analyse each item individually, Spend & F&P Costs, What type of products does it include? Was a discount applied?
item_level as (
select 
    mobile_customer_id, transaction_date_sk, transaction_time_sk, transaction_id, store_sk, point_of_delivery_type_sk 
    ,case when t.mapped_flag = 'True' THEN 1 else 0 end as visa_token_mapped
    ,tld_net_amt
    ,case when tld_units_cnt < 0 then 0 else tld_units_cnt end as tld_units_cnt
    ,coffee, burger, chicken, dessert, snack, breakfast, mcveggie, fish, fries, salad, beef, vm, happy_meal, family, shake
    ,sundae, big_mac, qtr_p, nuggets, hashbrown, cheeseburger, mcmuffin
    ,case when mi.category1_dsc = 'Non Food' OR mi.category1_dsc = 'Non Product' THEN 0 else tld_net_amt end as gp_spend

    /* Food and Paper */
    ,CASE
        WHEN mi.category1_dsc = 'Non Food' OR mi.category1_dsc = 'Non Product' THEN 0
        WHEN t.item_level <> 1 then (t.qcr_food_cost_amt + t.qcr_paper_cost_amt) * case when tld_units_cnt < 0 then 0 else tld_units_cnt end
        WHEN ami.category1_dsc = 'Breakfast' AND mi.category1_dsc = 'Breakfast' AND t.item_level = 1 and ami.evm_ind = 'Y' and ami.size_code in ('S', 'M', 'L')  THEN 0 
        WHEN ami.category3_dsc = mi.category3_dsc AND t.item_level = 1 and ami.evm_ind = 'Y' and ami.size_code in ('S', 'M', 'L') THEN 0
        WHEN ami.category3_dsc = mi.category3_dsc AND t.item_level = 1 and ami.happymeal_ind = 'Y' THEN 0
        WHEN ami.category1_dsc in ('Drinks Hot', 'Drinks Cold') AND ami.category3_dsc = mi.category3_dsc AND t.item_level = 1 THEN 0
        ELSE (t.qcr_food_cost_amt + t.qcr_paper_cost_amt) * case when tld_units_cnt < 0 then 0 else tld_units_cnt end
    END AS FP_Costs_b4_prize

    /* Campaign */
    ,camp.offer_id, camp.crew_offer, camp.visa, camp.st_george, camp.voted_offer, case when t.item_level = 0 then camp.loyalty_prize else 0 end as loyalty_prize
    ,case when t.item_level = 0 then camp.loyalty_prize_tier_1 else 0 end as loyalty_prize_tier_1
    ,case when t.item_level = 0 then camp.loyalty_prize_tier_2 else 0 end as loyalty_prize_tier_2 
    ,case when t.item_level = 0 then camp.loyalty_prize_tier_3 else 0 end as loyalty_prize_tier_3
    ,case when (t.item_level = 0 and transaction_date_sk >= 20220301) then camp.punchcard else 0 end as punchcard

    -- exclude employee and manager payments
    ,case when point_of_payment_type_sk in (5, 11) then 1 else 0 end as employee_payment

    /* If it falls into the below categories we don't want to include the food and paper */
    ,case when camp.crew_offer + camp.visa + camp.st_george + camp.voted_offer > 0 then 1 when point_of_payment_type_sk in (5, 11) then 1 else 0 end as exclude_fp

    /* Discount */
    ,case when discount_amt > 0 then 1 else 0 end as discount /* Was a discount applied */
    ,case discount_type when 'Promo' then 1 else 0 end as discount_promo
    ,case discount_type when 'Price' then 1 else 0 end as discount_price
    ,discount_amt
from tld_detail_mapped t

left join menu_items mi on t.menu_item_sk = mi.menu_item_sk
left join dw_pre.d_stld_menu_item ami on t.adj_menu_item_sk = ami.menu_item_sk
left join campaigns as camp on t.campaign_sk = camp.campaign_sk
left join dw_pre.d_stld_promotion promo on t.promotion_sk = promo.promotion_sk

/* Filter non mobile customers and to specified date */
where t.mobile_customer_id <> ''
and t.transaction_date_sk = {date_sk}
),

-- Aggregate up to a transaction level
transaction_level as (
select
    mobile_customer_id, transaction_date_sk, transaction_time_sk, transaction_id, store_sk, point_of_delivery_type_sk, visa_token_mapped
    ,sum(tld_net_amt) as tld_net_amt
    ,sum(tld_units_cnt) as tld_units_cnt, sum(gp_spend) as gp_spend, sum(FP_Costs_b4_prize) as fp_costs_b4_prize
    ,max(discount) as discount, max(discount_promo) as discount_promo, max(discount_price) as discount_price

    /* Campaigns */
    ,max(case when offer_id between 8000 and 9000 then offer_id else 0 end) as propensity_offer_id
    ,max(case when offer_id between 8000 and 8999 then 1 else 0 end) as propensity
    /* Are there other offers we can profile apart from propensity e.g mcafe */
    ,max(case when offer_id not between 8000 and 8999 then offer_id else 0 end) as mass_offer_id
    ,max(case when coalesce(offer_id, 8000) not between 8000 and 8999 then 1 else 0 end) as mass_offer

    /* Exclusional campaign types */
    ,coalesce(max(exclude_fp)) as exclude_fp
    ,coalesce(max(loyalty_prize)) as loyalty_prize
    ,coalesce(max(loyalty_prize_tier_1)) as loyalty_prize_tier_1
    ,coalesce(max(loyalty_prize_tier_2)) as loyalty_prize_tier_2
    ,coalesce(max(loyalty_prize_tier_3)) as loyalty_prize_tier_3
    ,coalesce(max(punchcard)) as punchcard

    ,max(coffee) as coffee_inc, sum(coffee * tld_net_amt) as coffee_spend, sum(coffee * tld_units_cnt) as coffee_units, max(coffee * discount) as coffee_discount
    ,max(burger) as burger_inc, sum(burger * tld_net_amt) as burger_spend, sum(burger * tld_units_cnt) as burger_units, max(burger * discount) as burger_discount
    ,max(snack) as snack_inc, sum(snack * tld_net_amt) as snack_spend, sum(snack * tld_units_cnt) as snack_units, max(snack * discount) as snack_discount
    ,max(chicken) as chicken_inc, sum(chicken * tld_net_amt) as chicken_spend, sum(chicken * tld_units_cnt) as chicken_units, max(chicken * discount) as chicken_discount
    ,max(dessert) as dessert_inc, sum(dessert * tld_net_amt) as dessert_spend, sum(dessert * tld_units_cnt) as dessert_units, max(dessert * discount) as dessert_discount
    ,max(breakfast) as breakfast_inc, sum(breakfast * tld_net_amt) as breakfast_spend, sum(breakfast * tld_units_cnt) as breakfast_units
    ,max(breakfast * discount) as breakfast_discount
    ,max(beef) as beef_inc, sum(beef * tld_net_amt) as beef_spend, sum(beef * tld_units_cnt) as beef_units, max(beef * discount) as beef_discount
    ,max(fish) as fish_inc, sum(fish * tld_net_amt) as fish_spend, sum(fish * tld_units_cnt) as fish_units, max(fish * discount) as fish_discount
    ,max(salad) as salad_inc, sum(salad * tld_net_amt) as salad_spend, sum(salad * tld_units_cnt) as salad_units, max(salad * discount) as salad_discount
    ,max(mcveggie) as mcveggie_inc, sum(mcveggie * tld_net_amt) as mcveggie_spend, sum(mcveggie * tld_units_cnt) as mcveggie_units, max(mcveggie * discount) as mcveggie_discount
    ,max(fries) as fries_inc, sum(fries * tld_net_amt) as fries_spend, sum(fries * tld_units_cnt) as fries_units, max(fries * discount) as fries_discount
    ,max(happy_meal) as happy_meal_inc, sum(happy_meal * tld_net_amt) as happy_meal_spend, sum(happy_meal * tld_units_cnt) as happy_meal_units
    ,max(happy_meal * discount) as happy_meal_discount
    ,max(vm) as vm_inc, sum(vm * tld_net_amt) as vm_spend, sum(vm * tld_units_cnt) as vm_units, max(vm * discount) as vm_discount
    ,max(family) as family_inc, sum(family * tld_net_amt) as family_spend, sum(family * tld_units_cnt) as family_units, max(family * discount) as family_discount
    ,max(shake) as shake_inc, sum(shake * tld_net_amt) as shake_spend, sum(shake * tld_units_cnt) as shake_units, max(shake * discount) as shake_discount
    ,max(sundae) as sundae_inc, sum(sundae * tld_net_amt) as sundae_spend, sum(sundae * tld_units_cnt) as sundae_units, max(sundae * discount) as sundae_discount
    ,max(big_mac) as big_mac_inc, sum(big_mac * tld_net_amt) as big_mac_spend, sum(big_mac * tld_units_cnt) as big_mac_units, max(big_mac * discount) as big_mac_discount
    ,max(qtr_p) as qtr_p_inc, sum(qtr_p * tld_net_amt) as qtr_p_spend, sum(qtr_p * tld_units_cnt) as qtr_p_units, max(qtr_p * discount) as qtr_p_discount
    ,max(cheeseburger) as cheeseburger_inc, sum(cheeseburger * tld_net_amt) as cheeseburger_spend, sum(cheeseburger * tld_units_cnt) as cheeseburger_units
    ,max(cheeseburger * discount) as cheeseburger_discount
    ,max(nuggets) as nuggets_inc, sum(nuggets * tld_net_amt) as nuggets_spend, sum(nuggets * tld_units_cnt) as nuggets_units, max(nuggets * discount) as nuggets_discount
    ,max(hashbrown) as hashbrown_inc, sum(hashbrown * tld_net_amt) as hashbrown_spend, sum(hashbrown * tld_units_cnt) as hashbrown_units
    ,max(hashbrown * discount) as hashbrown_discount
    ,max(mcmuffin) as mcmuffin_inc, sum(mcmuffin * tld_net_amt) as mcmuffin_spend, sum(mcmuffin * tld_units_cnt) as mcmuffin_units
    ,max(mcmuffin * discount) as mcmuffin_discount
from item_level

group by 1,2,3,4,5,6,7
), 
    
-- Add extra transaction details such as the location and payment type
transaction_level_detail as (
select
    --t.*
    t.mobile_customer_id, t.transaction_date_sk, t.transaction_time_sk, t.transaction_id, t.store_sk, t.point_of_delivery_type_sk, t.visa_token_mapped, t.tld_net_amt
    ,t.tld_units_cnt, t.gp_spend, t.fp_costs_b4_prize, t.discount, t.discount_promo, t.discount_price --,t.num_item
    ,t.propensity_offer_id, t.propensity, t.mass_offer_id, t.mass_offer
    ,t.loyalty_prize, t.loyalty_prize_tier_1, t.loyalty_prize_tier_2, t.loyalty_prize_tier_3,  t.punchcard
    ,case 
        when t.exclude_fp = 1 then 1
        when t.tld_net_amt = 0 then 1 
        else 0 
    end as exclude_fp

    ,coffee_inc, coffee_spend, coffee_units, coffee_discount
    ,burger_inc, burger_spend, burger_units, burger_discount
    ,snack_inc, snack_spend, snack_units, snack_discount
    ,chicken_inc, chicken_spend, chicken_units, chicken_discount
    ,dessert_inc, dessert_spend, dessert_units, dessert_discount
    ,breakfast_inc, breakfast_spend, breakfast_units, breakfast_discount
    ,beef_inc, beef_spend, beef_units, beef_discount
    ,fish_inc, fish_spend, fish_units, fish_discount
    ,salad_inc, salad_spend, salad_units, salad_discount
    ,mcveggie_inc, mcveggie_spend, mcveggie_units, mcveggie_discount
    ,fries_inc, fries_spend, fries_units, fries_discount
    ,happy_meal_inc, happy_meal_spend, happy_meal_units, happy_meal_discount
    ,vm_inc, vm_spend, vm_units, vm_discount
    ,family_inc, family_spend, family_units, family_discount
    ,shake_inc, shake_spend, shake_units, shake_discount
    ,sundae_inc, sundae_spend, sundae_units, sundae_discount
    ,big_mac_inc, big_mac_spend, big_mac_units, big_mac_discount
    ,qtr_p_inc, qtr_p_spend, qtr_p_units, qtr_p_discount
    ,cheeseburger_inc, cheeseburger_spend, cheeseburger_units, cheeseburger_discount
    ,nuggets_inc, nuggets_spend, nuggets_units, nuggets_discount
    ,hashbrown_inc, hashbrown_spend, hashbrown_units, hashbrown_discount
    ,mcmuffin_inc, mcmuffin_spend, mcmuffin_units, mcmuffin_discount

    /* Time of Day */
    ,date_bk as business_date_bk
    ,case when day_no_in_week between 1 and 5 then 'Workday' else 'Weekend' end as day_type
    ,case
        when transaction_time_sk between 500 and 1050 then 'Breakfast'
        when transaction_time_sk between 1050 and 1400 then 'Lunch'
        when transaction_time_sk between 1400 and 1700 then 'Tea'
        when transaction_time_sk between 1700 and 2100 then 'Dinner'
        else 'Late'
    end as time_type
    /* geographic */
    ,case locality_dsc when 'CITY' then 1 else 0 end as city
    ,case locality_dsc when 'SUBURBAN' then 1 else 0 end as suburban
    ,case locality_dsc when 'COUNTRY' then 1 else 0 end as country
    /* Cheque Bands */
    ,case 
        when tld_net_amt < 5 then 'Mini'
        when tld_net_amt < 10 then 'Small'
        when tld_net_amt < 20 then 'Medium'
        when tld_net_amt < 40 then 'Large'
        else 'Supersize'
    end as cheque_band
    ,pod_type_dsc
    ,case
        /* UBER EATS, MENULOG etc */
        when point_of_delivery_type_sk in (21, 32, 50, 34) then 'ONLINE'
        when point_of_delivery_type_sk = 9 then 'MOBILE'
        when point_of_delivery_type_sk = 6 then 'DRIVE THRU'
        /* Front Desk, Self Service, McCafe etc */
        when point_of_delivery_type_sk in (1, 5, 8, 4) then 'IN STORE'
        else 'OTHER'
    end as point_of_delivery

    ,case store_type_dsc
        when 'FREE STANDING' then 'FREE STANDING'
        when 'INSTORE' then 'INSTORE'
        when 'FOOD COURT' then 'FOOD COURT'
        when 'MALL' then 'FOOD COURT'
        when 'Oil Alliance' then 'SERVO'
        else 'OTHER' /* the remaining stores make up such a small proportion */
        /* EXPRESS, DRIVE THRU ONLY, SPECIAL */
    end as store_type
from transaction_level t

inner join dw_pre.d_date dt on t.transaction_date_sk = dt.date_sk
/* Exclude any of the GP exclusions */

left join dw_pre.d_stld_store store on t.store_sk = store.store_sk
left join dw_pre.d_pod_type as pod on t.point_of_delivery_type_sk = pod.pod_type_sk
),
        
loyalty_dtl_transactions as (
select 
    Distinct l.mobile_customer_id, l.transaction_date_sk, l.transaction_time_sk, l.transaction_id, l.store_sk, l.point_of_delivery_type_sk, l.visa_token_mapped, l.loyalty_prize
    ,l.punchcard
    ,t.item_sequence
    ,case 
        when l.exclude_fp = 1 then 1
        when l.tld_net_amt = 0 then 1 
        else 0 
    end as exclude_fp
from transaction_level l
inner join dw_pre.f_stld_detail t on l.mobile_customer_id = t.mobile_customer_id
    and l.transaction_date_sk = t.transaction_date_sk
    and l.transaction_time_sk = t.transaction_time_sk
    and l.transaction_id = t.transaction_id
    and l.store_sk = t.store_sk
inner join campaigns as camp on t.campaign_sk = camp.campaign_sk
where (l.loyalty_prize = 1 or l.punchcard = 1)
and (substring(camp.campaign_title, 9, 4) in ('2500', '5000', '7500') or lower(camp.campaign_title) like '%%punch%%')
),

prize_fp_cost as (
select 
    l.mobile_customer_id, l.transaction_date_sk, l.transaction_time_sk, l.transaction_id, l.store_sk, l.point_of_delivery_type_sk, l.exclude_fp
    --, l.loyalty_prize
    ,CASE when l.loyalty_prize + l.punchcard > 0 then 1 else 0 end as loyalty_prize

    ,CASE when t.item_level = 0 then l.loyalty_prize else 0 end as loyalty_prize_redeem

     ,CASE
        WHEN mi.category1_dsc = 'Non Food' OR mi.category1_dsc = 'Non Product' THEN 0
        WHEN t.item_level <> 1 then (t.qcr_food_cost_amt + t.qcr_paper_cost_amt) * case when tld_units_cnt < 0 then 0 else tld_units_cnt end
        WHEN ami.category1_dsc = 'Breakfast' AND mi.category1_dsc = 'Breakfast' AND t.item_level = 1 and ami.evm_ind = 'Y' and ami.size_code in ('S', 'M', 'L')  THEN 0 
        WHEN ami.category3_dsc = mi.category3_dsc AND t.item_level = 1 and ami.evm_ind = 'Y' and ami.size_code in ('S', 'M', 'L') THEN 0
        WHEN ami.category3_dsc = mi.category3_dsc AND t.item_level = 1 and ami.happymeal_ind = 'Y' THEN 0
        WHEN ami.category1_dsc in ('Drinks Hot', 'Drinks Cold') AND ami.category3_dsc = mi.category3_dsc AND t.item_level = 1 THEN 0
        ELSE (t.qcr_food_cost_amt + t.qcr_paper_cost_amt) * case when tld_units_cnt < 0 then 0 else tld_units_cnt end
    END AS FP_Costs_prize

from loyalty_dtl_transactions l
left join dw_pre.f_stld_detail t on l.mobile_customer_id = t.mobile_customer_id
    and l.transaction_date_sk = t.transaction_date_sk
    and l.transaction_time_sk = t.transaction_time_sk
    and l.transaction_id = t.transaction_id
    and l.store_sk = t.store_sk
    and l.item_sequence = t.item_sequence

left join dw_pre.d_stld_menu_item mi on t.menu_item_sk = mi.menu_item_sk
left join dw_pre.d_stld_menu_item ami on t.adj_menu_item_sk = ami.menu_item_sk
),

-- Aggregate up to a customer level
-- So a row will represent the users activity in a given day
customer_level_b4_prize as (
select
    mobile_customer_id, exclude_fp
    ,CASE when loyalty_prize + punchcard > 0 then 1 else 0 end as loyalty_prize
    ,transaction_date_sk, business_date_bk, day_type 
    ,count(1) as num_transactions
    ,sum(visa_token_mapped) as visa_token_mapped
    --,count(distinct CASE WHEN visa_token_mapped !=0 THEN 1 END) as visa_token_mapped
    ,sum(loyalty_prize_tier_1) as loyalty_prize_tier_1, sum(loyalty_prize_tier_2) as loyalty_prize_tier_2, sum(loyalty_prize_tier_3) as loyalty_prize_tier_3 
    ,sum(tld_net_amt) as tld_net_amt, sum(tld_units_cnt) as tld_units_cnt, sum(gp_spend) as gp_spend, sum(fp_costs_b4_prize) as fp_costs_b4_prize

    ,sum(case cheque_band when 'Mini' then 1 else 0 end) as mini_cheque_band
    ,sum(case cheque_band when 'Small' then 1 else 0 end) as small_cheque_band
    ,sum(case cheque_band when 'Medium' then 1 else 0 end) as medium_cheque_band
    ,sum(case cheque_band when 'Large' then 1 else 0 end) as large_cheque_band
    ,sum(case cheque_band when 'Supersize' then 1 else 0 end) as supersize_cheque_band

    ,sum(propensity) as propensity
    ,max(propensity_offer_id) as propensity_offer_id
    ,sum(propensity * tld_net_amt) as propensity_spend
    ,sum(propensity * gp_spend) as propensity_gp_spend
    ,sum(propensity * fp_costs_b4_prize) as propensity_fp_costs_b4_prize

    ,sum(mass_offer) as mass_offer
    ,max(mass_offer_id) as mass_offer_id
    ,sum(mass_offer * tld_net_amt) as mass_offer_spend
    ,sum(mass_offer * gp_spend) as mass_offer_gp_spend
    ,sum(mass_offer * fp_costs_b4_prize) as mass_offer_fp_costs_b4_prize

    ,sum(discount) as discount, sum(discount_promo) as discount_promo ,sum(discount_price) as discount_price

    ,sum(coffee_inc) as coffee_inc, sum(coffee_spend) as coffee_spend, sum(coffee_units) as coffee_units, sum(coffee_discount) as coffee_discount
    ,sum(burger_inc) as burger_inc, sum(burger_spend) as burger_spend, sum(burger_units) as burger_units, sum(burger_discount) as burger_discount
    ,sum(chicken_inc) as chicken_inc, sum(chicken_spend) as chicken_spend, sum(chicken_units) as chicken_units, sum(chicken_discount) as chicken_discount
    ,sum(snack_inc) as snack_inc, sum(snack_spend) as snack_spend, sum(snack_units) as snack_units, sum(snack_discount) as snack_discount
    ,sum(dessert_inc) as dessert_inc, sum(dessert_spend) as dessert_spend, sum(dessert_units) as dessert_units, sum(dessert_discount) as dessert_discount
    ,sum(breakfast_inc) as breakfast_inc, sum(breakfast_spend) as breakfast_spend, sum(breakfast_units) as breakfast_units, sum(breakfast_discount) as breakfast_discount
    ,sum(fries_inc) as fries_inc, sum(fries_spend) as fries_spend, sum(fries_units) as fries_units, sum(fries_discount) as fries_discount
    ,sum(beef_inc) as beef_inc, sum(beef_spend) as beef_spend, sum(beef_units) as beef_units, sum(beef_discount) as beef_discount
    ,sum(fish_inc) as fish_inc, sum(fish_spend) as fish_spend, sum(fish_units) as fish_units, sum(fish_discount) as fish_discount
    ,sum(salad_inc) as salad_inc, sum(salad_spend) as salad_spend, sum(salad_units) as salad_units, sum(salad_discount) as salad_discount
    ,sum(mcveggie_inc) as mcveggie_inc, sum(mcveggie_spend) as mcveggie_spend, sum(mcveggie_units) as mcveggie_units, sum(mcveggie_discount) as mcveggie_discount
    ,sum(happy_meal_inc) as happy_meal_inc, sum(happy_meal_spend) as happy_meal_spend, sum(happy_meal_units) as happy_meal_units, sum(happy_meal_discount) as happy_meal_discount
    ,sum(vm_inc) as vm_inc, sum(vm_spend) as vm_spend, sum(vm_units) as vm_units, sum(vm_discount) as vm_discount
    ,sum(family_inc) as family_inc, sum(family_spend) as family_spend, sum(family_units) as family_units, sum(family_discount) as family_discount
    ,sum(shake_inc) as shake_inc, sum(shake_spend) as shake_spend, sum(shake_units) as shake_units, sum(shake_discount) as shake_discount
    ,sum(sundae_inc) as sundae_inc, sum(sundae_spend) as sundae_spend, sum(sundae_units) as sundae_units, sum(sundae_discount) as sundae_discount
    ,sum(big_mac_inc) as big_mac_inc, sum(big_mac_spend) as big_mac_spend, sum(big_mac_units) as big_mac_units, sum(big_mac_discount) as big_mac_discount
    ,sum(nuggets_inc) as nuggets_inc, sum(nuggets_spend) as nuggets_spend, sum(nuggets_units) as nuggets_units, sum(nuggets_discount) as nuggets_discount
    ,sum(qtr_p_inc) as qtr_p_inc, sum(qtr_p_spend) as qtr_p_spend, sum(qtr_p_units) as qtr_p_units, sum(qtr_p_discount) as qtr_p_discount
    ,sum(cheeseburger_inc) as cheeseburger_inc, sum(cheeseburger_spend) as cheeseburger_spend, sum(cheeseburger_units) as cheeseburger_units
    ,sum(cheeseburger_discount) as cheeseburger_discount
    ,sum(hashbrown_inc) as hashbrown_inc, sum(hashbrown_spend) as hashbrown_spend, sum(hashbrown_units) as hashbrown_units, sum(hashbrown_discount) as hashbrown_discount
    ,sum(mcmuffin_inc) as mcmuffin_inc, sum(mcmuffin_spend) as mcmuffin_spend, sum(mcmuffin_units) as mcmuffin_units, sum(mcmuffin_discount) as mcmuffin_discount

    ,sum(city) as city, sum(suburban) as suburban, sum(country) as country

    ,sum(case store_type when 'FREE STANDING' then 1 else 0 end) as store_free_standing
    ,sum(case store_type when 'INSTORE' then 1 else 0 end) as store_instore
    ,sum(case store_type when 'FOOD COURT' then 1 else 0 end) as store_food_court
    ,sum(case store_type when 'SERVO' then 1 else 0 end) as store_servo
    ,sum(case store_type when 'OTHER' then 1 else 0 end) as store_other

    ,sum(case when time_type = 'Breakfast' then 1 else 0 end) as breakfast
    ,sum(case when time_type = 'Lunch' then 1 else 0 end) as lunch
    ,sum(case when time_type = 'Tea' then 1 else 0 end) as tea
    ,sum(case when time_type = 'Dinner' then 1 else 0 end) as dinner
    ,sum(case when time_type = 'Late' then 1 else 0 end) as late

    ,sum(case point_of_delivery when 'ONLINE' then 1 else 0 end) as online
    ,sum(case point_of_delivery when 'MOBILE' then 1 else 0 end) as mobile
    ,sum(case point_of_delivery when 'IN STORE' then 1 else 0 end) as in_store
    ,sum(case point_of_delivery when 'DRIVE THRU' then 1 else 0 end) as drive_thru

from transaction_level_detail t

group by 1,2,3,4,5,6
),

-- Customer and Transaction level aggregation
customer_level_prize as (
select l.mobile_customer_id, l.exclude_fp, l.loyalty_prize, l.transaction_date_sk,
sum(FP_Costs_prize) fp_costs_prize, sum(loyalty_prize_redeem) as num_prize_redemptions
from prize_fp_cost l
group by 1,2,3,4
)

select t.* 
    ,isnull(l.fp_costs_prize, 0) as fp_costs_prize
    ,isnull(l.num_prize_redemptions, 0) as num_prize_redemptions
    ,(t.fp_costs_b4_prize - isnull(l.fp_costs_prize, 0)) as fp_costs
    ,(t.propensity_fp_costs_b4_prize - (case when t.propensity = 0 then 0 else isnull(l.fp_costs_prize, 0) end)) as propensity_fp_costs
    ,(t.mass_offer_fp_costs_b4_prize - (case when t.mass_offer = 0 then 0 else isnull(l.fp_costs_prize, 0) end)) as mass_offer_fp_costs
    ,getdate() as audit_daily_datetime
from customer_level_b4_prize t
    left join customer_level_prize l on t.mobile_customer_id = l.mobile_customer_id
        and t.exclude_fp = l.exclude_fp
        and t.loyalty_prize = l.loyalty_prize
        and t.transaction_date_sk = l.transaction_date_sk
