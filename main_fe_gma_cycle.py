import argparse
from datetime import datetime, date, timedelta
from logging import exception
from src.utils import Utils, Redshift
from src_fe_gma_cycle.data_preprocessing import DailyAggregates, Redemptions, OfferCycles
from src_fe_gma_cycle.feature_engineering import ModelFeatures, Features, RedemptionFeatures
import logging
import pathlib
import os


class FeatureEngineering:
    """A master class to orchestrate all the different methods

    Args:
        ocd (OfferCycles): Class for offer cycle dates
        daily_agg (DailyAggregates): class for daily aggregates
        redemptions (Redemptions): class for redemptions preprocessing
        model_features (ModelFeatures): class for model features
        schema (str, optional): The schema in redshift. Defaults to 'annalect'.
    """

    def __init__(
        self,
        ocd: OfferCycles,
        daily_agg: DailyAggregates,
        redemptions: Redemptions,
        model_features: ModelFeatures,
        schema: str = 'annalect'
    ) -> None:

        self.logger = logging.getLogger(__class__.__name__)

        self.schema = schema
        self.ocd = ocd
        self.daily_agg = daily_agg
        self.redemptions = redemptions
        self.model_features = model_features

    def update_preprocessing(
        self,
        rs: Redshift,
        offer_cycle: int = -1,
        overwrite: bool = False
    ) -> None:
        try:
            self.logger.info("UPDATE PROCESSING")
            self.logger.info(
                f"Offer Cycle: {offer_cycle}, overwrite: {overwrite}")

            # if the offer cycle is specified just update the values for that cycle
            if offer_cycle > 0:

                self.logger.info(
                    f"Update preprocessing for offer cycle {offer_cycle}")

                # run the daily aggregates for the specified dates
                _, start_date_sk, end_date_sk, _ = self.ocd.get_dates(
                    rs, offer_cycle)

                # convert the date sks into dates
                counter = Utils.sk_to_date(start_date_sk)
                end_date = Utils.sk_to_date(end_date_sk)

                # loop through all dates in the offer cycle
                while counter <= end_date:
                    # convert back into an sk
                    date_sk = Utils.date_to_sk(counter)

                    # run daily aggregate
                    self.daily_agg.run_daily_aggregate(rs, date_sk, overwrite)

                    # increment it by a day
                    counter = counter + timedelta(days=1)

                # update the redemptions table for that offer cycle
                self.redemptions.calculate_redemptions(
                    rs, self.ocd, offer_cycle, overwrite)

            # otherwise update from the last input to the most up to date
            else:
                self.logger.info(
                    f"Update preprocessing up to most recent")
                self.daily_agg.update(rs, overwrite)
                self.redemptions.update(rs, self.ocd, overwrite)

        except Exception as e:
            self.logger.error("Error updating preprocessing")
            self.logger.exception(e)

    def update_features(self, rs: Redshift, offer_cycle: int = -1, overwrite: bool = False):
        """[summary]

        Args:
            rs (Redshift): [description]
            offer_cycle (int, optional): [description]. Defaults to -1.
            overwrite (bool, optional): [description]. Defaults to False.
        """
        try:
            self.logger.info("UPDATE FEATURES")
            self.logger.info(
                f"Offer Cycle: {offer_cycle}, overwrite: {overwrite}")

            # if the offer cycle is specified just update the values for that cycle
            if offer_cycle > 0:

                self.logger.info(
                    f"Update features for offer cycle {offer_cycle}")

                # run the daily aggregates for the specified dates
                model_date_sk,  _, _, _ = self.ocd.get_dates(rs, offer_cycle)

                self.model_features.transactions.create_features(
                    rs, Utils.sk_to_date(model_date_sk), offer_cycle, overwrite)
                self.model_features.redemptions.create_features(
                    rs, Utils.sk_to_date(model_date_sk), offer_cycle, overwrite)

            # otherwise update from the last input to the most up to date
            else:
                self.logger.info(
                    f"Update features up to most recent")
                self.model_features.redemptions.update(rs, self.ocd, overwrite)
                self.model_features.transactions.update(
                    rs, self.ocd, overwrite)

        except Exception as e:
            self.logger.error("Error updating features")
            self.logger.exception(e)

    def create_training_data(self, rs: Redshift, start_cycle: int, end_cycle: int, overwrite: bool) -> None:
        try:

            self.logger.info("CREATE TRAINING DATA")
            self.logger.info(
                f"Start Cycle: {start_cycle}, End Cycle: {end_cycle}, overwrite: {overwrite}")

            self.model_features.generate_training(
                rs,
                self.ocd,
                start_cycle,
                end_cycle,
                overwrite
            )

        except Exception as e:
            self.logger.error("Error creating training data")
            self.logger.exception(e)

    def create_infer_data(self, rs: Redshift, offer_cycle: int, model_date: date, overwrite: bool) -> None:
        try:
            self.logger.info("CREATE INFER DATA")
            self.logger.info(
                f"Offer Cycle: {offer_cycle}, Model Date: {model_date}, overwrite: {overwrite}")

            self.model_features.generate_infer(
                rs,
                model_date,
                offer_cycle,
                overwrite
            )

        except Exception as e:
            self.logger.error("Error creating infer data")
            self.logger.exception(e)


def build_feature_engineering(config):

    ocd = OfferCycles(
        schema=config['schema'],
        **config['offer_cycle_dates']
    )

    daily_agg = DailyAggregates(
        schema=config['schema'],
        **config['daily_aggregates']
    )

    redemptions = Redemptions(
        schema=config['schema'],
        **config['redemptions']
    )

    transaction_features = Features(
        schema=config['schema'],
        **config['transaction_features']
    )

    customer_stats = Features(
        schema=config['schema'],
        **config['customer_redemption_stats']
    )

    offer_stats = Features(
        schema=config['schema'],
        **config['offer_redemption_stats']
    )

    customer_offer_stats = Features(
        schema=config['schema'],
        **config['customer_offer_redemption_stats']
    )

    redemption_features = RedemptionFeatures(
        customer_stats=customer_stats,
        offer_stats=offer_stats,
        customer_offer_stats=customer_offer_stats,
        schema=config['schema'],
        **config['redemption_features']
    )

    model_features = ModelFeatures(
        redemption_features,
        transaction_features,
        schema=config['schema'],
        **config['model_features']
    )

    return FeatureEngineering(
        ocd,
        daily_agg,
        redemptions,
        model_features,
        config.get('schema', 'annalect')
    )


def build_parser():

    parser = argparse.ArgumentParser(
        prog='Feature Engineering',
        description="Run various processes to create the features for the Offer Allocation Engine"
    )
    subparsers = parser.add_subparsers(dest='command')

    parser.add_argument(
        '-c', '--config', type=str,
        help='The path to the config file', required=True
    )

    parser.add_argument(
        '-r', '--redshift', type=str,
        help="The path to the redshift credentials", required=True
    )

    # all methods have an overwrite argument so add it to the top line
    parser.add_argument(
        '-o', '--overwrite',
        dest='overwrite', action='store_true',
        help="Would you like to overwrite the existing values in redshift?"
    )
    parser.set_defaults(overwrite=False)

    prep = subparsers.add_parser('preprocessing')
    prep.add_argument(
        '-oc', '--offer-cycle',
        dest='offer_cycle', type=int, default=-1,
        help="The offer cycle in question. If not provided it will run all preprocessing since the last update."
    )

    training = subparsers.add_parser('training')
    training.add_argument(
        '-sc', '--start-cycle',
        dest='start_cycle', type=int, required=True,
        help="The start cycle for the training data."
    )
    training.add_argument(
        '-ec', '--end-cycle',
        dest='end_cycle', type=int, required=True,
        help="The end cycle for the training data"
    )

    infer = subparsers.add_parser('infer')
    infer.add_argument(
        '-oc', '--offer-cycle',
        dest='offer_cycle', type=int, required=True,
        help="The offer cycle to run the infer for"
    )

    features = subparsers.add_parser('features')
    features.add_argument(
        '-oc', '--offer-cycle',
        dest='offer_cycle', type=int, default=-1,
        help="The offer cycle to create the features for"
    )

    return parser


if __name__ == "__main__":

    # build the logger
    logging.basicConfig(
        filename=os.path.join(
            pathlib.Path(__file__).parent.absolute(),
            'log_feature_engineering.log'
        ),
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.DEBUG
    )

    # set up the parser
    parser = build_parser()

    # parse the args
    args = parser.parse_args()

    logging.info(f"ARGS: {args}")

    # read in the yaml
    config = Utils.process_yaml(args.config)
    credentials = Utils.process_yaml(args.redshift)

    # set up the redshift connection using the details in the config
    rs = Redshift(
        **credentials
    )

    # Feature engineering
    fe = build_feature_engineering(config)

    if args.command == 'preprocessing':
        fe.update_preprocessing(rs, args.offer_cycle, args.overwrite)

    elif args.command == 'features':
        fe.update_features(rs, args.offer_cycle, args.overwrite)

    elif args.command == 'training':
        fe.create_training_data(rs, args.start_cycle,
                                args.end_cycle, args.overwrite)

    elif args.command == 'infer':
        fe.create_infer_data(rs, args.offer_cycle,
                             datetime.now().date(), args.overwrite)

    logging.info('Complete')
